<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Action\User;

use ApiPlatform\Core\Exception\InvalidArgumentException;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;

class Register
{
//    /**
//     * @var JWTTokenManagerInterface
//     */
//    private $jwtManager;
//
//    /**
//     * Register constructor.
//     *
//     * @param JWTTokenManagerInterface $jwtManager
//     */
//    public function __construct(JWTTokenManagerInterface $jwtManager)
//    {
//        $this->jwtManager = $jwtManager;
//    }

    /**
     * @Route(
     *     name="register",
     *     path="/register",
     *     defaults={"_api_resource_class"=User::class, "_api_collection_operation_name"="register"}
     * )
     * @Method("POST")
     *
     * @param mixed $data
     *
     * @throws \RuntimeException
     * @throws \ApiPlatform\Core\Exception\InvalidArgumentException
     *
     * @return mixed
     */
    public function __invoke($data)
    {
        if (!$data instanceof User) {
            throw new InvalidArgumentException(sprintf('Data must be an instance of %s', User::class));
        }

        // Initialize token to avoid useless call after registration
//        $data->setToken($this->jwtManager->create($data));

        return $data;
    }
}
