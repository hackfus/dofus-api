<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Action\User;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;

class Me
{
    /**
     * @Route(
     *     name="api_users_get_item",
     *     path="/users/{id}",
     *     defaults={"_api_resource_class"=User::class, "_api_item_operation_name"="api_users_get_item"},
     *     requirements={"id"="\d+|me"}
     * )
     * @Method("GET")
     *
     * @param mixed $data
     * @param mixed $id
     *
     * @return null|User
     */
    public function __invoke($data, $id)
    {
        // Return the User object
        return $data;
    }
}
