<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\ResourceEffect;
use App\Entity\ResourceEffectInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method null|ResourceEffectInterface find($id, $lockMode = null, $lockVersion = null)
 * @method null|ResourceEffectInterface findOneBy(array $criteria, array $orderBy = null)
 * @method ResourceEffectInterface[]    findAll()
 * @method ResourceEffectInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResourceEffectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ResourceEffect::class);
    }
}
