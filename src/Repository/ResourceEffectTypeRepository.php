<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\ResourceEffectType;
use App\Entity\ResourceEffectTypeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method null|ResourceEffectTypeInterface find($id, $lockMode = null, $lockVersion = null)
 * @method null|ResourceEffectTypeInterface findOneBy(array $criteria, array $orderBy = null)
 * @method ResourceEffectTypeInterface[]    findAll()
 * @method ResourceEffectTypeInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResourceEffectTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ResourceEffectType::class);
    }
}
