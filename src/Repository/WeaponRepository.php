<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Weapon;
use App\Entity\WeaponInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method null|WeaponInterface find($id, $lockMode = null, $lockVersion = null)
 * @method null|WeaponInterface findOneBy(array $criteria, array $orderBy = null)
 * @method WeaponInterface[]    findAll()
 * @method WeaponInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeaponRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Weapon::class);
    }
}
