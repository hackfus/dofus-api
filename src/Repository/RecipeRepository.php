<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\RecipeItem;
use App\Entity\RecipeItemInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method null|RecipeItemInterface find($id, $lockMode = null, $lockVersion = null)
 * @method null|RecipeItemInterface findOneBy(array $criteria, array $orderBy = null)
 * @method RecipeItemInterface[]    findAll()
 * @method RecipeItemInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RecipeItem::class);
    }
}
