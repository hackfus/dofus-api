<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\MarketCategory;
use App\Entity\MarketCategoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method null|MarketCategoryInterface find($id, $lockMode = null, $lockVersion = null)
 * @method null|MarketCategoryInterface findOneBy(array $criteria, array $orderBy = null)
 * @method MarketCategoryInterface[]    findAll()
 * @method MarketCategoryInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarketCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MarketCategory::class);
    }
}
