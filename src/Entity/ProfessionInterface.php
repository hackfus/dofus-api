<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface ProfessionInterface extends IdentifiableInterface, NameableInterface, TimestampableInterface, ToggleableInterface, CrawlableInterface
{
    /**
     * Set description.
     *
     * @param null|string $description
     *
     * @return $this
     */
    public function setDescription(?string $description = null): self;

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): ?string;

    /**
     * @return Collection|ResourceInterface[]
     */
    public function getHarvests();

    /**
     * @return int
     */
    public function countHarvests(): int;

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function addHarvest(ResourceInterface $resource): self;

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function removeHarvest(ResourceInterface $resource): self;

    /**
     * @param ResourceInterface $resource
     *
     * @return bool
     */
    public function hasHarvest(ResourceInterface $resource): bool;

    /**
     * @return Collection|ResourceInterface[]
     */
    public function getRecipes();

    /**
     * @return int
     */
    public function countRecipes(): int;

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function addRecipe(ResourceInterface $resource): self;

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function removeRecipe(ResourceInterface $resource): self;

    /**
     * @param ResourceInterface $resource
     *
     * @return bool
     */
    public function hasRecipe(ResourceInterface $resource): bool;
}
