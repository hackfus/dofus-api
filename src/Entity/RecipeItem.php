<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity({"resourceCrafted", "subresource"})
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="recipe_item_unique", columns={"resource_crafted_id", "subresource_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\RecipeRepository")
 */
class RecipeItem implements RecipeItemInterface
{
    use TimestampableEntity, ToggleableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var ResourceInterface
     *
     * @Assert\Valid
     * @Assert\NotNull()
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Resource", inversedBy="recipeItems", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $resourceCrafted;

    /**
     * @var ResourceInterface
     *
     * @Assert\Valid
     * @Assert\NotNull()
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Resource", inversedBy="crafts", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $subresource;

    /**
     * @var int
     *
     * @Assert\GreaterThanOrEqual(1)
     *
     * @ORM\Column(type="integer")
     */
    protected $quantity = 1;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setResourceCrafted(?ResourceInterface $resourceCrafted = null): RecipeItemInterface
    {
        $this->resourceCrafted = $resourceCrafted;

        return $this;
    }

    public function getResourceCrafted(): ?ResourceInterface
    {
        return $this->resourceCrafted;
    }

    /**
     * @param ResourceInterface $subresource
     *
     * @return RecipeItem
     */
    public function setSubresource(?ResourceInterface $subresource = null): RecipeItemInterface
    {
        $this->subresource = $subresource;

        return $this;
    }

    /**
     * @return ResourceInterface
     */
    public function getSubresource(): ?ResourceInterface
    {
        return $this->subresource;
    }

    public function setQuantity(int $quantity = 1): RecipeItemInterface
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
