<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity("name")
 *
 * @ORM\Entity(repositoryClass="App\Repository\MarketRepository")
 */
class Market implements MarketInterface
{
    use TimestampableEntity, ToggleableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * Market name. This field has a unique constraint and must be at least 2 characters long.
     *
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var Collection|MarketCategoryInterface[]
     *
     * @Assert\Valid(traverse=true)
     *
     * @ORM\OneToMany(targetEntity="App\Entity\MarketCategory", mappedBy="market", cascade={"persist"})
     */
    protected $categories;

    /**
     * Market constructor.
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(?string $name = null): MarketInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return Collection|MarketCategoryInterface[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return int
     */
    public function countCategories(): int
    {
        return $this->categories->count();
    }

    /**
     * @param MarketCategoryInterface $category
     *
     * @return $this
     */
    public function addCategory(MarketCategoryInterface $category): MarketInterface
    {
        if (!$this->hasCategory($category)) {
            $this->categories->add($category);
            $category->setMarket($this);
        }

        return $this;
    }

    /**
     * @param MarketCategoryInterface $category
     *
     * @return $this
     */
    public function removeCategory(MarketCategoryInterface $category): MarketInterface
    {
        if ($this->hasCategory($category)) {
            $this->categories->removeElement($category);
            $category->setMarket(null);
        }

        return $this;
    }

    /**
     * @param MarketCategoryInterface $category
     *
     * @return bool
     */
    public function hasCategory(MarketCategoryInterface $category): bool
    {
        return $this->categories->contains($category);
    }
}
