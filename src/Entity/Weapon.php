<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WeaponRepository")
 */
class Weapon extends Resource implements WeaponInterface
{
    /**
     * Weapon action points. This value must be in range [1;12]. Default to 1.
     *
     * @var int
     *
     * @Assert\Range(min="1", max="12")
     *
     * @ORM\Column(name="action_points", type="integer")
     */
    protected $actionPoints = 1;

    /**
     * Weapon max uses per turn. This value must be in range [1;3]. Default to 1.
     *
     * @var int
     *
     * @Assert\Range(min="1", max="3")
     *
     * @ORM\Column(name="uses_per_turn", type="integer")
     */
    protected $usesPerTurn = 1;

    /**
     * Weapon minimal range. This value must be in range [1;63]. Default to 1.
     *
     * @var int
     *
     * @Assert\Range(min="1", max="63")
     *
     * @ORM\Column(name="min_range", type="integer")
     */
    protected $minRange = 1;

    /**
     * Weapon maximal range. This value must be in range [1;63]. Default to 1.
     *
     * @var int
     *
     * @Assert\Range(min="1", max="63")
     *
     * @ORM\Column(name="max_range", type="integer")
     */
    protected $maxRange = 1;

    /**
     * Weapon maximal range. This value must be in range [0;100]. Default to 0.
     *
     * @var int
     *
     * @Assert\Range(min="0", max="100")
     *
     * @ORM\Column(name="critical_rate", type="integer")
     */
    protected $criticalRate = 0;

    /**
     * Weapon maximal range. This value must be at least 0. Default to 0.
     *
     * @var int
     *
     * @Assert\Range(min="0")
     *
     * @ORM\Column(name="critical_hit_damages", type="integer")
     */
    protected $criticalHitDamages = 0;

    public function setActionPoints(int $value = 1): WeaponInterface
    {
        $this->actionPoints = $value;

        return $this;
    }

    public function getActionPoints(): int
    {
        return $this->actionPoints;
    }

    public function setUsesPerTurn(int $value = 1): WeaponInterface
    {
        $this->usesPerTurn = $value;

        return $this;
    }

    public function getUsesPerTurn(): int
    {
        return $this->usesPerTurn;
    }

    public function setMinRange(int $value = 1): WeaponInterface
    {
        $this->minRange = $value;

        return $this;
    }

    public function getMinRange(): int
    {
        return $this->minRange;
    }

    public function setMaxRange(int $value = 1): WeaponInterface
    {
        $this->maxRange = $value;

        return $this;
    }

    public function getMaxRange(): int
    {
        return $this->maxRange;
    }

    public function setCriticalRate(int $value = 0): WeaponInterface
    {
        $this->criticalRate = $value;

        return $this;
    }

    public function getCriticalRate(): int
    {
        return $this->criticalRate;
    }

    public function setCriticalHitDamages(int $value = 0): WeaponInterface
    {
        $this->criticalHitDamages = $value;

        return $this;
    }

    public function getCriticalHitDamages(): int
    {
        return $this->criticalHitDamages;
    }
}
