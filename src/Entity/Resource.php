<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Traits\CrawlableTrait;
use App\Entity\Traits\ImageEmbeddableTrait;
use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity("name")
 *
 * @ORM\Entity(repositoryClass="App\Repository\ResourceRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="class", type="string")
 * @ORM\DiscriminatorMap({
 *     "resource" = "Resource",
 *     "equipment" = "Equipment",
 *     "weapon" = "Weapon"
 * })
 */
class Resource implements ResourceInterface
{
    use TimestampableEntity, ToggleableTrait, CrawlableTrait;
    use ImageEmbeddableTrait {
        ImageEmbeddableTrait::__construct as private initializeImageEmbeddableTrait;
    }

    /**
     * Resource identifier.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * Resource name. This field has a unique constraint and must be at least 2 characters long.
     *
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * Resource level. This value must be in range [1;1000]. Default to 1.
     *
     * @var int
     *
     * @Assert\Range(min="1", max="1000")
     *
     * @ORM\Column(name="level", type="integer")
     */
    protected $level = 1;

    /**
     * Resource description. Story about the item.
     *
     * @var string
     *
     * @Assert\Length(min="2")
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * Is the resource tradable ? If so, it could be sold on markets. Default to true.
     *
     * @var bool
     *
     * @ORM\Column(name="tradable", type="boolean")
     */
    protected $tradable = true;

    /**
     * Is the resource droppable ? If so, it could be won by defeating monsters. Default to true.
     *
     * @var bool
     *
     * @ORM\Column(name="droppable", type="boolean")
     */
    protected $droppable = true;

    /**
     * Is the resource secret ? If so, it doesn't appear in recipes book. Default to false.
     *
     * @var bool
     *
     * @ORM\Column(name="secret", type="boolean")
     */
    protected $secret = false;

    /**
     * @var MarketCategoryInterface
     *
     * @Assert\Valid
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MarketCategory", inversedBy="resources", cascade={"persist"})
     */
    protected $marketCategory;

    /**
     * @var Collection|RecipeItemInterface[]
     *
     * @Assert\Count(max="8")
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="App\Entity\RecipeItem", mappedBy="resourceCrafted", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $recipeItems;

    /**
     * @var Collection|RecipeItemInterface[]
     *
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="App\Entity\RecipeItem", mappedBy="subresource", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $crafts;

    /**
     * @var ProfessionInterface
     *
     * @Assert\Valid
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Profession", inversedBy="harvests", cascade={"persist"})
     */
    protected $harvestProfession;

    /**
     * @var ProfessionInterface
     *
     * @Assert\Valid
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Profession", inversedBy="recipes", cascade={"persist"})
     */
    protected $recipeProfession;

    /**
     * @var Collection|ResourceEffectInterface[]
     *
     * @Assert\Valid(traverse=true)
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ResourceEffect", mappedBy="resource", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $effects;

    /**
     * @var ?string
     *
     * @ORM\Column(name="conditions", type="string", nullable=true)
     */
    protected $conditions;

    /**
     * Resource constructor.
     */
    public function __construct()
    {
        $this->initializeImageEmbeddableTrait();
        $this->recipeItems = new ArrayCollection();
        $this->crafts = new ArrayCollection();
        $this->effects = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(?string $name = null): ResourceInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set level.
     *
     * @param int $level
     *
     * @return $this
     */
    public function setLevel(int $level = 1): ResourceInterface
    {
        // Minimum level value is 1
        $this->level = $level;

        return $this;
    }

    /**
     * Get level.
     *
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * Set description.
     *
     * @param null|string $description
     *
     * @return ResourceInterface
     */
    public function setDescription(?string $description = null): ResourceInterface
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set tradable.
     *
     * @param bool $tradable
     *
     * @return ResourceInterface
     */
    public function setTradable(bool $tradable = true): ResourceInterface
    {
        $this->tradable = $tradable;

        return $this;
    }

    /**
     * Is tradable.
     *
     * @return bool
     */
    public function isTradable(): bool
    {
        return $this->tradable;
    }

    /**
     * Set droppable.
     *
     * @param bool $droppable
     *
     * @return ResourceInterface
     */
    public function setDroppable(bool $droppable = true): ResourceInterface
    {
        $this->droppable = $droppable;

        return $this;
    }

    /**
     * Is droppable.
     *
     * @return bool
     */
    public function isDroppable(): bool
    {
        return $this->droppable;
    }

    /**
     * Set secret.
     *
     * @param bool $secret
     *
     * @return ResourceInterface
     */
    public function setSecret(bool $secret = false): ResourceInterface
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Is secret.
     *
     * @return bool
     */
    public function isSecret(): bool
    {
        return $this->secret;
    }

    /**
     * Get market.
     *
     * @return null|MarketInterface
     */
    public function getMarket(): ?MarketInterface
    {
        return null !== $this->marketCategory ? $this->marketCategory->getMarket() : null;
    }

    /**
     * Get market.
     *
     * @return string
     */
    public function getCategoryName(): string
    {
        return null !== $this->marketCategory ? $this->marketCategory->getName() : '';
    }

    /**
     * Set market.
     *
     * @param null|MarketCategoryInterface $category
     *
     * @return $this
     */
    public function setMarketCategory(?MarketCategoryInterface $category = null): ResourceInterface
    {
        $this->marketCategory = $category;

        return $this;
    }

    /**
     * Get market.
     *
     * @return null|MarketCategoryInterface
     */
    public function getMarketCategory(): ?MarketCategoryInterface
    {
        return $this->marketCategory;
    }

    /**
     * Get market.
     *
     * @return RecipeItemInterface[]
     */
    public function getRecipeItems(): array
    {
        return $this->recipeItems->getValues();
    }

    /**
     * @param RecipeItemInterface $item
     *
     * @return $this
     */
    public function addRecipeItem(RecipeItemInterface $item): ResourceInterface
    {
        if (!$this->hasRecipeItem($item)) {
            $this->recipeItems->add($item);
            $item->setResourceCrafted($this);
        }

        return $this;
    }

    /**
     * @param RecipeItemInterface $item
     *
     * @return $this
     */
    public function removeRecipeItem(RecipeItemInterface $item): ResourceInterface
    {
        if ($this->hasRecipeItem($item)) {
            $this->recipeItems->removeElement($item);
            $item->setResourceCrafted();
        }

        return $this;
    }

    /**
     * @param RecipeItemInterface $item
     *
     * @return bool
     */
    public function hasRecipeItem(RecipeItemInterface $item): bool
    {
        return $this->recipeItems->contains($item);
    }

    public function clearRecipeItems()
    {
        $this->recipeItems->clear();
    }

    /**
     * @return RecipeItemInterface[]
     */
    public function getCrafts(): array
    {
        return $this->crafts->getValues();
    }

    /**
     * @param RecipeItemInterface $item
     *
     * @return $this
     */
    public function addCraft(RecipeItemInterface $item): ResourceInterface
    {
        if (!$this->hasCraft($item)) {
            $this->crafts->add($item);
            $item->setSubresource($this);
        }

        return $this;
    }

    /**
     * @param RecipeItemInterface $item
     *
     * @return $this
     */
    public function removeCraft(RecipeItemInterface $item): ResourceInterface
    {
        if ($this->hasCraft($item)) {
            $this->crafts->removeElement($item);
            $item->setSubresource();
        }

        return $this;
    }

    /**
     * @param RecipeItemInterface $item
     *
     * @return bool
     */
    public function hasCraft(RecipeItemInterface $item): bool
    {
        return $this->crafts->contains($item);
    }

    /**
     * @param null|ProfessionInterface $harvestProfession
     *
     * @return ResourceInterface
     */
    public function setHarvestProfession(?ProfessionInterface $harvestProfession = null): ResourceInterface
    {
        $this->harvestProfession = $harvestProfession;

        return $this;
    }

    /**
     * @return null|ProfessionInterface
     */
    public function getHarvestProfession(): ?ProfessionInterface
    {
        return $this->harvestProfession;
    }

    /**
     * @param null|ProfessionInterface $recipeProfession
     *
     * @return ResourceInterface
     */
    public function setRecipeProfession(?ProfessionInterface $recipeProfession = null): ResourceInterface
    {
        $this->recipeProfession = $recipeProfession;

        return $this;
    }

    /**
     * @return null|ProfessionInterface
     */
    public function getRecipeProfession(): ?ProfessionInterface
    {
        return $this->recipeProfession;
    }

    /**
     * {@inheritdoc}
     */
    public function getEffects()
    {
        return $this->effects;
    }

    /**
     * {@inheritdoc}
     */
    public function countEffects(): int
    {
        return $this->effects->count();
    }

    /**
     * {@inheritdoc}
     */
    public function addEffect(ResourceEffectInterface $resourceEffect): ResourceInterface
    {
        if (!$this->hasEffect($resourceEffect)) {
            $this->effects->add($resourceEffect);
            $resourceEffect->setResource($this);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeEffect(ResourceEffectInterface $resourceEffect): ResourceInterface
    {
        if ($this->hasEffect($resourceEffect)) {
            $this->effects->removeElement($resourceEffect);
            $resourceEffect->setResource(null);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasEffect(ResourceEffectInterface $resourceEffect): bool
    {
        return $this->effects->contains($resourceEffect);
    }

    public function setConditions(?string $value = null): ResourceInterface
    {
        $this->conditions = $value;

        return $this;
    }

    public function getConditions(): ?string
    {
        return $this->conditions;
    }
}
