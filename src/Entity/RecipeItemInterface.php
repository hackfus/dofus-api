<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

interface RecipeItemInterface extends IdentifiableInterface, TimestampableInterface, ToggleableInterface
{
    public function setResourceCrafted(?ResourceInterface $resourceCrafted = null): self;

    public function getResourceCrafted(): ?ResourceInterface;

    /**
     * @param ResourceInterface $subresource
     *
     * @return RecipeItem
     */
    public function setSubresource(?ResourceInterface $subresource = null): self;

    /**
     * @return ResourceInterface
     */
    public function getSubresource(): ?ResourceInterface;

    public function setQuantity(int $quantity = 1): self;

    public function getQuantity(): int;
}
