<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface MarketCategoryInterface extends IdentifiableInterface, NameableInterface, TimestampableInterface, ToggleableInterface
{
    /**
     * Set market.
     *
     * @param null|MarketInterface $market
     *
     * @return $this
     */
    public function setMarket(?MarketInterface $market = null): self;

    /**
     * Get market.
     *
     * @return null|MarketInterface
     */
    public function getMarket(): ?MarketInterface;

    /**
     * @return Collection|ResourceInterface[]
     */
    public function getResources();

    /**
     * @return int
     */
    public function countResources(): int;

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function addResource(ResourceInterface $resource): self;

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function removeResource(ResourceInterface $resource): self;

    /**
     * @param ResourceInterface $resource
     *
     * @return bool
     */
    public function hasResource(ResourceInterface $resource): bool;
}
