<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

interface ResourceEffectInterface extends IdentifiableInterface, NameableInterface, TimestampableInterface, ToggleableInterface, CrawlableInterface
{
    // type => regexp
    public const PRIMARY_BONUSES = [
        'critique',
        'agilité',
        'chance',
        'intelligence',
        'invocations',
        'pa',
        'pm',
        'portée',
    ];

    public function setMinValue(int $min = 1): self;

    public function getMinValue(): int;

    public function setMaxValue(int $max = 1): self;

    public function getMaxValue(): int;

    public function setValueIsInPercent(bool $value = false): self;

    public function getValueIsInPercent(): bool;

    public function setType(ResourceEffectTypeInterface $type = null): self;

    public function getType(): ?ResourceEffectTypeInterface;

    public function setPrimaryBonus(bool $value = true): self;

    public function isPrimaryBonus(): bool;

    public function setLiteral(?string $literal): self;

    public function getLiteral(): ?string;

    public function setResource(?ResourceInterface $resource): self;

    public function getResource(): ?ResourceInterface;
}
