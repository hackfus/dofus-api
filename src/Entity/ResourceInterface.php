<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface ResourceInterface extends IdentifiableInterface, NameableInterface, TimestampableInterface, ToggleableInterface, CrawlableInterface, ImageEmbeddable
{
    /**
     * Set level.
     *
     * @param int $level
     *
     * @return $this
     */
    public function setLevel(int $level = 1): self;

    /**
     * Get level.
     *
     * @return int
     */
    public function getLevel(): int;

    /**
     * Set description.
     *
     * @param null|string $description
     *
     * @return $this
     */
    public function setDescription(?string $description = null): self;

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): ?string;

    /**
     * Set tradable.
     *
     * @param bool $tradable
     *
     * @return $this
     */
    public function setTradable(bool $tradable = true): self;

    /**
     * Is tradable.
     *
     * @return bool
     */
    public function isTradable(): bool;

    /**
     * Set droppable.
     *
     * @param bool $droppable
     *
     * @return $this
     */
    public function setDroppable(bool $droppable = true): self;

    /**
     * Is droppable.
     *
     * @return bool
     */
    public function isDroppable(): bool;

    /**
     * Set secret.
     *
     * @param bool $secret
     *
     * @return $this
     */
    public function setSecret(bool $secret = false): self;

    /**
     * Is secret.
     *
     * @return bool
     */
    public function isSecret(): bool;

    /**
     * Get market.
     *
     * @return null|MarketInterface
     */
    public function getMarket(): ?MarketInterface;

    /**
     * Set market.
     *
     * @param null|MarketCategoryInterface $category
     *
     * @return $this
     */
    public function setMarketCategory(?MarketCategoryInterface $category = null): self;

    /**
     * Get market.
     *
     * @return null|MarketCategoryInterface
     */
    public function getMarketCategory(): ?MarketCategoryInterface;

    /**
     * @return RecipeItemInterface[]
     */
    public function getRecipeItems(): array;

    /**
     * @param RecipeItemInterface $item
     *
     * @return $this
     */
    public function addRecipeItem(RecipeItemInterface $item): self;

    /**
     * @param RecipeItemInterface $item
     *
     * @return $this
     */
    public function removeRecipeItem(RecipeItemInterface $item): self;

    /**
     * @param RecipeItemInterface $item
     *
     * @return bool
     */
    public function hasRecipeItem(RecipeItemInterface $item): bool;

    public function clearRecipeItems();

    /**
     * @return RecipeItemInterface[]
     */
    public function getCrafts(): array;

    /**
     * @param RecipeItemInterface $item
     *
     * @return $this
     */
    public function addCraft(RecipeItemInterface $item): self;

    /**
     * @param RecipeItemInterface $item
     *
     * @return $this
     */
    public function removeCraft(RecipeItemInterface $item): self;

    /**
     * @param RecipeItemInterface $item
     *
     * @return bool
     */
    public function hasCraft(RecipeItemInterface $item): bool;

    /**
     * @param null|ProfessionInterface $profession
     *
     * @return ResourceInterface
     */
    public function setHarvestProfession(?ProfessionInterface $profession = null): self;

    /**
     * @return null|ProfessionInterface
     */
    public function getHarvestProfession(): ?ProfessionInterface;

    /**
     * @param null|ProfessionInterface $profession
     *
     * @return ResourceInterface
     */
    public function setRecipeProfession(?ProfessionInterface $profession = null): self;

    /**
     * @return null|ProfessionInterface
     */
    public function getRecipeProfession(): ?ProfessionInterface;

    /**
     * @return Collection|ResourceEffectInterface[]
     */
    public function getEffects();

    /**
     * @return int
     */
    public function countEffects(): int;

    /**
     * @param ResourceEffectInterface $resourceEffect
     *
     * @return $this
     */
    public function addEffect(ResourceEffectInterface $resourceEffect): self;

    /**
     * @param ResourceEffectInterface $resourceEffect
     *
     * @return $this
     */
    public function removeEffect(ResourceEffectInterface $resourceEffect): self;

    /**
     * @param ResourceEffectInterface $resourceEffect
     *
     * @return bool
     */
    public function hasEffect(ResourceEffectInterface $resourceEffect): bool;

    public function setConditions(?string $value = null): self;

    public function getConditions(): ?string;
}
