<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Symfony\Component\Security\Core\User\AdvancedUserInterface as BaseUserInterface;

interface UserInterface extends ToggleableInterface, BaseUserInterface, IdentifiableInterface, NameableInterface, TimestampableInterface, \Serializable
{
    public const ROLE_DEFAULT = 'ROLE_USER';

    public const ROLE_ADMIN = 'ROLE_ADMIN';

    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    public function setUsername(?string $username = null): self;

    public function setPassword(?string $password = null): self;

    public function getPlainPassword(): ?string;

    public function setPlainPassword(string $plainPassword = null): self;

    public function addRole(string $role): self;

    public function removeRole(string $role): self;

    public function hasRole(string $role): bool;
}
