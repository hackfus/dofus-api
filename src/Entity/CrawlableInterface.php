<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

interface CrawlableInterface
{
    /**
     * Set fetched.
     *
     * @param bool $fetched
     */
    public function setFetched(bool $fetched = true): void;

    /**
     * Get fetched.
     *
     * @return bool
     */
    public function isFetched(): bool;

    /**
     * @param string $id
     */
    public function setLastCrawler(?string $id): void;

    /**
     * @return string
     */
    public function getLastCrawler(): ?string;
}
