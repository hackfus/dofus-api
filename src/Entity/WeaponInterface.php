<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

interface WeaponInterface extends ResourceInterface
{
    public function setActionPoints(int $value = 1): self;

    public function getActionPoints(): int;

    public function setUsesPerTurn(int $value = 1): self;

    public function getUsesPerTurn(): int;

    public function setMinRange(int $value = 1): self;

    public function getMinRange(): int;

    public function setMaxRange(int $value = 1): self;

    public function getMaxRange(): int;

    public function setCriticalRate(int $value = 0): self;

    public function getCriticalRate(): int;

    public function setCriticalHitDamages(int $value = 0): self;

    public function getCriticalHitDamages(): int;
}
