<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Traits\CrawlableTrait;
use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity("name")
 * @ORM\Entity(repositoryClass="App\Repository\ProfessionRepository")
 */
class Profession implements ProfessionInterface
{
    use TimestampableEntity, ToggleableTrait, CrawlableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Profession name. This field has a unique constraint and must be at least 2 characters long.
     *
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * Profession description. Story about the item.
     *
     * @var string
     *
     * @Assert\Length(min="2")
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * Defines harvestable resources. Resources' level indicates the profession's level needed to harvest them.
     *
     * @var Collection|ResourceInterface[]
     *
     * @Assert\Valid(traverse=true)
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Resource", mappedBy="harvestProfession", cascade={"persist"})
     */
    protected $harvests;

    /**
     * Defines craftables resources. Resources' level indicates the profession's level needed to craft them.
     *
     * @var Collection|ResourceInterface[]
     *
     * @Assert\Valid(traverse=true)
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Resource", mappedBy="recipeProfession", cascade={"persist"})
     */
    protected $recipes;

    /**
     * Profession constructor.
     */
    public function __construct()
    {
        $this->harvests = new ArrayCollection();
        $this->recipes = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(?string $name = null): ProfessionInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param null|string $description
     *
     * @return ProfessionInterface
     */
    public function setDescription(?string $description = null): ProfessionInterface
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return Collection|ResourceInterface[]
     */
    public function getHarvests()
    {
        return $this->harvests;
    }

    /**
     * @return int
     */
    public function countHarvests(): int
    {
        return $this->harvests->count();
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function addHarvest(ResourceInterface $resource): ProfessionInterface
    {
        if (!$this->hasHarvest($resource)) {
            $this->harvests->add($resource);
            $resource->setHarvestProfession($this);
        }

        return $this;
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function removeHarvest(ResourceInterface $resource): ProfessionInterface
    {
        if ($this->hasHarvest($resource)) {
            $this->harvests->removeElement($resource);
            $resource->setHarvestProfession(null);
        }

        return $this;
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return bool
     */
    public function hasHarvest(ResourceInterface $resource): bool
    {
        return $this->harvests->contains($resource);
    }

    /**
     * @return Collection|ResourceInterface[]
     */
    public function getRecipes()
    {
        return $this->recipes;
    }

    /**
     * @return int
     */
    public function countRecipes(): int
    {
        return $this->recipes->count();
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function addRecipe(ResourceInterface $resource): ProfessionInterface
    {
        if (!$this->hasRecipe($resource)) {
            $this->recipes->add($resource);
            $resource->setRecipeProfession($this);
        }

        return $this;
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function removeRecipe(ResourceInterface $resource): ProfessionInterface
    {
        if ($this->hasRecipe($resource)) {
            $this->recipes->removeElement($resource);
            $resource->setRecipeProfession(null);
        }

        return $this;
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return bool
     */
    public function hasRecipe(ResourceInterface $resource): bool
    {
        return $this->recipes->contains($resource);
    }
}
