<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

interface ResourceEffectTypeInterface extends IdentifiableInterface, TimestampableInterface, ToggleableInterface
{
    public function isPrimaryBonus(): bool;

    public function setPrimaryBonus($value = false);

    public function isCosmeticBonus(): bool;

    public function setCosmeticBonus($value = false);

    public function getLabel(): ?string;

    public function setLabel(?string $label): self;
}
