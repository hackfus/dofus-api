<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Traits\CrawlableTrait;
use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResourceEffectRepository")
 */
class ResourceEffect implements ResourceEffectInterface
{
    use TimestampableEntity, ToggleableTrait, CrawlableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Resource effect min value. Can be positive or negative. Default to 1.
     *
     * @var int
     *
     * @ORM\Column(name="min_value", type="integer")
     */
    protected $minValue = 1;

    /**
     * Resource effect max value. Can be positive or negative. Default to 1.
     *
     * @var int
     *
     * @ORM\Column(name="max_value", type="integer")
     */
    protected $maxValue = 1;

    /**
     * Is this effect expressed in percentage ? If so, you should consider append '%". Default to false.
     *
     * @var bool
     *
     * @ORM\Column(name="value_is_in_percent", type="boolean")
     */
    protected $valueIsInPercent = false;

    /**
     * Resource effect type.
     *
     * @var ResourceEffectTypeInterface
     *
     * @Assert\Valid(traverse=true)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ResourceEffectType", cascade={"persist"})
     */
    protected $type;

    /**
     * Is it a primary bonus.
     *
     * @var bool
     *
     * @ORM\Column(name="primary_bonus", type="boolean")
     */
    protected $primaryBonus = false;

    /**
     * Resource effect literal. This field must be at least 2 characters long. For example : 1 PA.
     *
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     *
     * @ORM\Column(name="literal", type="string", length=255)
     */
    protected $literal;

    /**
     * @var ResourceInterface
     *
     * @Assert\Valid
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Resource", inversedBy="effects", cascade={"persist"})
     */
    protected $resource;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(?string $name = null): ResourceEffectInterface
    {
        $this->setLiteral($name);

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->getLiteral();
    }

    public function setMinValue(int $min = 1): ResourceEffectInterface
    {
        $this->minValue = $min;

        return $this;
    }

    public function getMinValue(): int
    {
        return $this->minValue;
    }

    public function setMaxValue(int $max = 1): ResourceEffectInterface
    {
        $this->maxValue = $max;

        return $this;
    }

    public function getMaxValue(): int
    {
        return $this->maxValue;
    }

    public function setValueIsInPercent(bool $value = false): ResourceEffectInterface
    {
        $this->valueIsInPercent = $value;

        return $this;
    }

    public function getValueIsInPercent(): bool
    {
        return $this->valueIsInPercent;
    }

    public function setType(ResourceEffectTypeInterface $type = null): ResourceEffectInterface
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?ResourceEffectTypeInterface
    {
        return $this->type;
    }

    public function setPrimaryBonus(bool $value = true): ResourceEffectInterface
    {
        $this->primaryBonus = $value;

        return $this;
    }

    public function isPrimaryBonus(): bool
    {
        return $this->primaryBonus;
    }

    public function setLiteral(?string $literal): ResourceEffectInterface
    {
        $this->literal = $literal;

        return $this;
    }

    public function getLiteral(): ?string
    {
        return $this->literal;
    }

    public function setResource(?ResourceInterface $resource): ResourceEffectInterface
    {
        $this->resource = $resource;

        return $this;
    }

    public function getResource(): ?ResourceInterface
    {
        return $this->resource;
    }
}
