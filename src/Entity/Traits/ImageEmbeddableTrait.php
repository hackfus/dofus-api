<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Traits;

use App\Entity\FileInterface;
use Doctrine\ORM\Mapping as ORM;

trait ImageEmbeddableTrait
{
    /**
     * @var FileInterface
     *
     * @ORM\OneToOne(targetEntity="File", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * ImageEmbeddableTrait constructor.
     */
    public function __construct()
    {
        $this->initialiaze();
    }

    public function setImage(FileInterface $image): void
    {
        $this->image = $image;
    }

    public function getImage(): ?FileInterface
    {
        if (!$this->image) {
            $this->initialiaze();
        }

        return $this->image;
    }

    public function initialiaze(): void
    {
//        $this->image = $this->image ?? new File();
    }
}
