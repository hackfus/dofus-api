<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Traits;

trait CrawlableTrait
{
    /**
     * This field is only used for backend purpose in order to know if the resource was manually inserted or not. Default to false.
     *
     * @var bool
     *
     * @ORM\Column(name="fetched", type="boolean")
     */
    protected $fetched = false;

    /**
     * Crawler ID used to avoid duplicate update from crawler command.
     *
     * Not serialized
     *
     * @var string
     *
     * @ORM\Column(name="last_crawler", type="string", length=255, nullable=true)
     */
    protected $lastCrawler;

    /**
     * Set fetched.
     *
     * @param bool $fetched
     */
    public function setFetched(bool $fetched = true): void
    {
        $this->fetched = $fetched;
    }

    /**
     * Get fetched.
     *
     * @return bool
     */
    public function isFetched(): bool
    {
        return $this->fetched;
    }

    /**
     * @param string $id
     */
    public function setLastCrawler(?string $id): void
    {
        $this->lastCrawler = $id;
    }

    /**
     * @return string
     */
    public function getLastCrawler(): ?string
    {
        return $this->lastCrawler;
    }
}
