<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity("name")
 *
 * @ORM\Entity(repositoryClass="App\Repository\MarketCategoryRepository")
 */
class MarketCategory implements MarketCategoryInterface
{
    use TimestampableEntity, ToggleableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Market name. This field has a unique constraint and must be at least 2 characters long.
     *
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var MarketInterface
     *
     * @Assert\Valid
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Market", inversedBy="categories", cascade={"persist"})
     */
    protected $market;

    /**
     * @var Collection|ResourceInterface[]
     *
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Resource", mappedBy="marketCategory", cascade={"persist"})
     */
    protected $resources;

    /**
     * MarketCategory constructor.
     */
    public function __construct()
    {
        $this->resources = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(?string $name = null): MarketCategoryInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set market.
     *
     * @param null|MarketInterface $market
     *
     * @return $this
     */
    public function setMarket(?MarketInterface $market = null): MarketCategoryInterface
    {
        $this->market = $market;

        return $this;
    }

    /**
     * Get market.
     *
     * @return null|MarketInterface
     */
    public function getMarket(): ?MarketInterface
    {
        return $this->market;
    }

    /**
     * @return Collection|ResourceInterface[]
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * @return int
     */
    public function countResources(): int
    {
        return $this->resources->count();
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function addResource(ResourceInterface $resource): MarketCategoryInterface
    {
        if (!$this->hasResource($resource)) {
            $this->resources->add($resource);
            $resource->setMarketCategory($this);
        }

        return $this;
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return $this
     */
    public function removeResource(ResourceInterface $resource): MarketCategoryInterface
    {
        if ($this->hasResource($resource)) {
            $this->resources->removeElement($resource);
            $resource->setMarketCategory(null);
        }

        return $this;
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return bool
     */
    public function hasResource(ResourceInterface $resource): bool
    {
        return $this->resources->contains($resource);
    }
}
