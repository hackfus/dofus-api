<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity("label")
 *
 * @ORM\Entity(repositoryClass="App\Repository\ResourceEffectTypeRepository")
 */
class ResourceEffectType implements ResourceEffectTypeInterface
{
    use TimestampableEntity, ToggleableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="primary_bonus", type="boolean")
     */
    protected $primaryBonus = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="cosmetic_bonus", type="boolean")
     */
    protected $cosmeticBonus = false;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     *
     * @ORM\Column(name="label", type="string", length=255, unique=true)
     */
    protected $label;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isPrimaryBonus(): bool
    {
        return $this->primaryBonus;
    }

    public function setPrimaryBonus($value = false)
    {
        $this->primaryBonus = $value;
    }

    public function isCosmeticBonus(): bool
    {
        return $this->cosmeticBonus;
    }

    public function setCosmeticBonus($value = false)
    {
        $this->cosmeticBonus = $value;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): ResourceEffectTypeInterface
    {
        $this->label = $label;

        return $this;
    }
}
