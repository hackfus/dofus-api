<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

interface ImageEmbeddable
{
    public function setImage(FileInterface $image): void;

    public function getImage(): ?FileInterface;
}
