<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

interface TimestampableInterface
{
    public function getCreatedAt(): ?\DateTime;

    public function setCreatedAt(?\DateTime $dateTime = null): void;

    public function getUpdatedAt(): ?\DateTime;

    public function setUpdatedAt(?\DateTime $dateTime = null): void;
}
