<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Symfony\Component\HttpFoundation\File\File as HttpFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;

interface FileInterface extends NameableInterface, TimestampableInterface, ToggleableInterface, CrawlableInterface
{
    public function getOriginalName(): ?string;

    public function setOriginalName(?string $originalName): self;

    public function getMimeType(): ?string;

    public function setMimeType(?string $mimeType): self;

    public function getSize(): ?int;

    public function setSize(?int $size): self;

    public function getDimensions(): ?array;

    public function setDimensions(?array $dimensions): self;

    public function getImageFile(): ?HttpFile;

    public function setImageFile(?HttpFile $image = null): self;

    public function getImage(): ?EmbeddedFile;

    public function setImage(?EmbeddedFile $image = null): self;

    public function getPath(): ?string;

    public function setPath(?string $path = null): self;
}
