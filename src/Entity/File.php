<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Traits\CrawlableTrait;
use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File as HttpFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 */
class File implements FileInterface
{
    use TimestampableEntity, ToggleableTrait, CrawlableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(
     *     mapping="item",
     *      fileNameProperty="image.name",
     *      size="image.size",
     *      mimeType="image.mimeType",
     *      originalName="image.originalName",
     *      dimensions="image.dimensions"
     * )
     *
     * @var HttpFile
     */
    private $imageFile;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $image;

    /**
     * @var string
     */
    private $path;

    /**
     * File constructor.
     */
    public function __construct()
    {
        $this->image = new EmbeddedFile();
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param HttpFile|UploadedFile $image
     *
     * @return FileInterface
     */
    public function setImageFile(?HttpFile $image = null): FileInterface
    {
        $this->imageFile = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getImageFile(): ?HttpFile
    {
        return $this->imageFile;
    }

    public function setImage(?EmbeddedFile $image = null): FileInterface
    {
        $this->image = $image;

        return $this;
    }

    public function getImage(): ?EmbeddedFile
    {
        return $this->image;
    }

    public function getName(): ?string
    {
        return $this->image->getName();
    }

    public function setName(?string $name = null)
    {
        $this->image->setName($name);

        return $this;
    }

    public function getOriginalName(): ?string
    {
        return $this->image->getOriginalName();
    }

    public function setOriginalName(?string $originalName): FileInterface
    {
        $this->image->setOriginalName($originalName);

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->image->getMimeType();
    }

    public function setMimeType(?string $mimeType): FileInterface
    {
        $this->image->setMimeType($mimeType);

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->image->getSize();
    }

    public function setSize(?int $size): FileInterface
    {
        $this->image->setSize($size);

        return $this;
    }

    public function getDimensions(): ?array
    {
        return $this->image->getDimensions();
    }

    public function setDimensions(?array $dimensions): FileInterface
    {
        $this->image->setDimensions($dimensions);

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path = null): FileInterface
    {
        $this->path = $path;

        return $this;
    }
}
