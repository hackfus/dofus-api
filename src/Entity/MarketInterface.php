<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface MarketInterface extends IdentifiableInterface, NameableInterface, TimestampableInterface, ToggleableInterface
{
    /**
     * @return Collection|MarketCategoryInterface[]
     */
    public function getCategories();

    /**
     * @return int
     */
    public function countCategories(): int;

    /**
     * @param MarketCategoryInterface $category
     *
     * @return $this
     */
    public function addCategory(MarketCategoryInterface $category): self;

    /**
     * @param MarketCategoryInterface $category
     *
     * @return $this
     */
    public function removeCategory(MarketCategoryInterface $category): self;

    /**
     * @param MarketCategoryInterface $category
     *
     * @return bool
     */
    public function hasCategory(MarketCategoryInterface $category): bool;
}
