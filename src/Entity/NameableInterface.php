<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

interface NameableInterface
{
    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(?string  $name = null);

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): ?string;
}
