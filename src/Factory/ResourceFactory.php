<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\ResourceInterface;

class ResourceFactory implements ResourceFactoryInterface
{
    protected const SUPPORTED_TYPES = [
        'aile',
        'alliage',
        'bois',
        'bourgeon',
        'carapace',
        'carte',
        'champignon',
        'clef',
        'coquille',
        'cuir',
        'céréale',
        'emballage',
        'essence de gardien de donjon',
        'étoffe',
        'fantôme de Familier',
        'fantôme de Montilier',
        'farine',
        'fleur',
        'fragment de carte',
        'fruit',
        'galet',
        'gelée',
        'graine',
        'huile',
        'idole',
        'laine',
        'légume',
        'matériel d\'alchimie',
        'metaria',
        'minerai',
        'nowel',
        'oeil',
        'oeuf',
        'orbe de forgemagie',
        'oreille',
        'os',
        'patte',
        'peau',
        'peluche',
        'pierre brute',
        'pierre précieuse',
        'planche',
        'plante',
        'plume',
        'poil',
        'poisson',
        'poisson vidé',
        'potion de forgemagie',
        'poudre',
        'préparation',
        'queue',
        'racine',
        'ressources diverses',
        'rune de forgemagie',
        'souvenir',
        'substrat',
        'sève',
        'teinture',
        'viande',
        'vêtement',
        'écorce',
    ];

    protected $class;

    public function __construct($resourceClass)
    {
        $this->class = $resourceClass;
    }

    public function createNew(): ResourceInterface
    {
        return new $this->class();
    }

    public function supportsType(string $type): bool
    {
        return \in_array(\mb_strtolower($type), static::SUPPORTED_TYPES, true);
    }
}
