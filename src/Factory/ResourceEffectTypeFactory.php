<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\ResourceEffectTypeInterface;

class ResourceEffectTypeFactory implements ResourceEffectTypeFactoryInterface
{
    protected $class;

    public function __construct($resourceEffectTypeClass)
    {
        $this->class = $resourceEffectTypeClass;
    }

    public function createNew(): ResourceEffectTypeInterface
    {
        return new $this->class();
    }

    public function supportsType(string $type): bool
    {
        return false;
    }
}
