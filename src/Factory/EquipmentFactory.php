<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

class EquipmentFactory extends ResourceFactory implements EquipmentFactoryInterface
{
    protected const SUPPORTED_TYPES = [
        'amulette',
        'anneau',
        'bottes',
        'bouclier',
        'cape',
        'ceitnure',
        'chapeau',
        'dofus',
        'sac à dos',
        'trophée',
    ];

    /**
     * Do not drop constructor as it allows parameter binding from symfony autowiring/autoconfigure
     * EquipmentFactory constructor.
     *
     * @param string $equipmentClass
     */
    public function __construct($equipmentClass)
    {
        parent::__construct($equipmentClass);
    }
}
