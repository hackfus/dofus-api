<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\MarketInterface;

class MarketFactory implements MarketFactoryInterface
{
    protected $class;

    public function __construct($marketClass)
    {
        $this->class = $marketClass;
    }

    public function createNew(): MarketInterface
    {
        return new $this->class();
    }

    public function supportsType(string $type): bool
    {
        return false;
    }
}
