<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\RecipeItemInterface;

class RecipeItemFactory implements RecipeItemFactoryInterface
{
    protected $class;

    public function __construct($recipeItemClass)
    {
        $this->class = $recipeItemClass;
    }

    public function createNew(): RecipeItemInterface
    {
        return new $this->class();
    }

    public function supportsType(string $type): bool
    {
        return false;
    }
}
