<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

class FactoryRegistry
{
    /**
     * @var iterable
     */
    private $factories;

    /**
     * FactoryRegistry constructor.
     *
     * @param iterable $factories
     */
    public function __construct(iterable $factories)
    {
        $this->factories = $factories;
    }

    public function addFactory(FactoryInterface $factory): void
    {
        $this->factories[] = $factory;
    }

    /**
     * @return null|\Generator
     */
    public function getFactories(): ?\Generator
    {
        foreach ($this->factories as $factory) {
            yield $factory;
        }
    }

    /**
     * @param string $type
     *
     * @return null|FactoryInterface
     */
    public function getFactoryFromType(string $type): ?FactoryInterface
    {
        foreach ($this->factories as $factory) {
            if ($factory->supportsType($type)) {
                return $factory;
            }
        }

        return null;
    }
}
