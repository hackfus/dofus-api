<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\FileInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileFactory implements FileFactoryInterface
{
    protected $class;

    public function __construct($fileClass)
    {
        $this->class = $fileClass;
    }

    public function createNew(): FileInterface
    {
        return new $this->class();
    }

    public function createWithUploadedFile(UploadedFile $uploadedFile): FileInterface
    {
        $file = $this->createNew();
        $file->setImageFile($uploadedFile);

        return $file;
    }

    public function supportsType(string $type): bool
    {
        return false;
    }
}
