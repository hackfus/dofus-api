<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

class WeaponFactory extends ResourceFactory implements WeaponFactoryInterface
{
    protected const SUPPORTED_TYPES = [
        'arc',
        'baguette',
        'bâton',
        'faux',
        'hache',
        'marteau',
        'pelle',
        'épée',
    ];

    /**
     * Do not drop constructor as it allows parameter binding from symfony autowiring/autoconfigure
     * WeaponFactory constructor.
     *
     * @param string $weaponClass
     */
    public function __construct($weaponClass)
    {
        parent::__construct($weaponClass);
    }
}
