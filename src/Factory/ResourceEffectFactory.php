<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\ResourceEffectInterface;
use App\Entity\ResourceEffectTypeInterface;
use Doctrine\ORM\EntityManagerInterface;

class ResourceEffectFactory implements ResourceEffectFactoryInterface
{
    protected $class;

    /**
     * @var EntityManagerInterface
     */
    protected $em;
    /**
     * @var ResourceEffectTypeFactoryInterface
     */
    private $effectTypeFactory;

    // https://regex101.com/r/r8fMdC/2
    protected $effectExpression = '/^(([\-|\+]?\d+)(\%)? (à (\-?\d+)(\%)?)? ?(.+)|.*)/u';
    protected $literalOffset = 1;
    protected $minValueOffset = 2;
    protected $minValueIsPercentOffset = 3;
    protected $maxValueOffset = 5;
    protected $maxValueIsPercentOffset = 6;
    protected $typeOffset = 7;

    public function __construct($resourceEffectClass, EntityManagerInterface $em, ResourceEffectTypeFactoryInterface $effectTypeFactory)
    {
        $this->class = $resourceEffectClass;
        $this->em = $em;
        $this->effectTypeFactory = $effectTypeFactory;
    }

    public function createNew(): ResourceEffectInterface
    {
        return new $this->class();
    }

    public function createFromLiteral(string $literal): ResourceEffectInterface
    {
        $effect = $this->createNew();

        \preg_match_all($this->effectExpression, $literal, $matches, PREG_SET_ORDER, 0);

        if (isset($matches[0])) {
            $parts = $matches[0];
            $effect->setLiteral($parts[$this->literalOffset]);

            // if parts count equals 2, then it's a full literal and no values have been extracted
            if (\count($parts) > 2) {
                $type = null;
                $isPercent = '%' === $parts[$this->minValueIsPercentOffset] || '%' === $parts[$this->maxValueIsPercentOffset];
                $label = $parts[$this->typeOffset] ?? false;

                $effect->setMinValue($parts[$this->minValueOffset]);
                $effect->setMaxValue(
                    '' !== $parts[$this->maxValueOffset] ? $parts[$this->maxValueOffset] : $effect->getMinValue()
                );
                $effect->setValueIsInPercent($isPercent);

                if ($label) {
                    $label = $isPercent ? sprintf('%s %s', '%', $label) : $label;
                    $type = $this->em->getRepository(ResourceEffectTypeInterface::class)->findOneBy([
                        'label' => $label,
                    ]);

                    if (!$type) {
                        $type = $this->effectTypeFactory->createNew();
                        $type->setLabel($label);
                        $this->em->persist($type);
                        $this->em->flush($type);
                    }

                    $effect->setType($type);
                }
            }
        }

        return $effect;
    }

    public function supportsType(string $type): bool
    {
        return false;
    }
}
