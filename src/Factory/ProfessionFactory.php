<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\ProfessionInterface;

class ProfessionFactory implements ProfessionFactoryInterface
{
    protected $class;

    public function __construct($professionClass)
    {
        $this->class = $professionClass;
    }

    public function createNew(): ProfessionInterface
    {
        return new $this->class();
    }

    public function createNamed(string $name): ProfessionInterface
    {
        $profession = $this->createNew();
        $profession->setName($name);

        return $profession;
    }

    public function supportsType(string $type): bool
    {
        return false;
    }
}
