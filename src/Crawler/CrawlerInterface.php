<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

interface CrawlerInterface
{
    public function crawlList(CrawlerContextInterface $context);

    public function crawlItem(string $url, CrawlerContextInterface $context);

    public static function getCrawlerAlias(): string;
}
