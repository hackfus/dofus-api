<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Resource;

use App\Crawler\Client;
use App\Crawler\CrawlerContextInterface;
use App\Crawler\Dispatcher\CrawlerDispatcher;
use App\Entity\File;
use App\Entity\ResourceInterface;
use App\Factory\FileFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\RouterInterface;

class ResourceImageExtractor implements ResourceExtractorInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var CrawlerDispatcher
     */
    protected $dispatcher;

    /**
     * @var RouterInterface
     */
    protected $apiRouter;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @var string
     */
    protected $varPath;

    /**
     * @var FileFactoryInterface
     */
    private $fileFactory;

    public function __construct(
        EntityManagerInterface $em,
        CrawlerDispatcher $dispatcher,
        RouterInterface $apiRouter,
        Client $client,
        FileFactoryInterface $fileFactory,
        string $varPath
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->apiRouter = $apiRouter;
        $this->client = $client;
        $this->fileSystem = new Filesystem();
        $this->varPath = $varPath;
        $this->fileFactory = $fileFactory;
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ResourceInterface       $resource
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     * @throws \Symfony\Component\Filesystem\Exception\IOException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ResourceInterface $resource): void
    {
        if (($imageNode = $node->filter('.ak-encyclo-detail-illu img')) && $imageNode->count() > 0) {
            $image = $imageNode->first()->attr('src');
            $imageGet = $this->client->get($image);

            $fileInfo = pathinfo($image);
            $tempDir = sprintf('%s/tmp', $this->varPath);
            $this->fileSystem->mkdir($tempDir);

            $originalName = $fileInfo['basename'] ?? \uniqid('static', true);
            $tempFilepath = sprintf('%s/%s', $tempDir, $originalName);
            $this->fileSystem->dumpFile(
                $tempFilepath,
                (string) $imageGet->getBody()
            );

            $uploadedImage = new UploadedFile(
                $tempFilepath,
                $originalName,
                current($imageGet->getHeader('content-type')),
                current($imageGet->getHeader('content-length')),
                null,
                true
            );

            $resourceImage = $resource->getImage();

            if (!$resourceImage) {
                $resourceImage = $this->fileFactory->createWithUploadedFile($uploadedImage);
            } else {
                $resourceImage->setImageFile($uploadedImage);
            }

            $resource->setImage($resourceImage);

            $this->em->persist($resource);
        }
    }
}
