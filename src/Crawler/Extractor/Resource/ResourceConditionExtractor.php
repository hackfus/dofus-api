<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Resource;

use App\Crawler\CrawlerContextInterface;
use App\Entity\ResourceInterface;
use App\Entity\WeaponInterface;
use Symfony\Component\DomCrawler\Crawler;

class ResourceConditionExtractor implements ResourceExtractorInterface
{
    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ResourceInterface       $resource
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ResourceInterface $resource): void
    {
        if (!$resource instanceof WeaponInterface) {
            return;
        }

        $panelTitle = $node->filter('.ak-panel > .ak-panel-title');

        if ($panelTitle->count() > 0 && 'Conditions' === trim($panelTitle->first()->text())) {
            $contentNode = $panelTitle->nextAll();

            if ($contentNode->count() > 0
                && \in_array(
                    'ak-panel-content',
                    $contentNode->first()->extract(['class']),
                    true
                )
            ) {
                $resource->setConditions(trim(str_replace("\n", ' ', $contentNode->filter('.ak-title')->first()->text())));

                foreach ($contentNode->filter('.ak-content')->children() as $child) {
                    $child->parentNode->removeChild($child);
                }
            }
        }
    }
}
