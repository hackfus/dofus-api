<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Resource;

use App\Crawler\CrawlerContextInterface;
use App\Entity\ResourceInterface;
use Symfony\Component\DomCrawler\Crawler;

class ResourceExtractorChain implements ResourceExtractorInterface
{
    /**
     * @var ResourceExtractorInterface[]
     */
    private $extractors;

    /**
     * ResourceExtractorChain constructor.
     */
    public function __construct()
    {
        $this->extractors = [];
    }

    public function addExtractor(ResourceExtractorInterface $extractor): void
    {
        $this->extractors[] = $extractor;
    }

    public function extract(Crawler $node, CrawlerContextInterface $context, ResourceInterface $resource): void
    {
        foreach ($this->extractors as $extractor) {
            $extractor->extract($node, $context, $resource);
        }
    }
}
