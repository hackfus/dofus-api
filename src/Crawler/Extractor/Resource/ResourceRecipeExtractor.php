<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Resource;

use App\Crawler\CrawlerContextInterface;
use App\Crawler\Dispatcher\CrawlerDispatcher;
use App\Crawler\Event\CrawlerEvents;
use App\Crawler\Event\CrawlerItemEvent;
use App\Crawler\Exception\RuntimeException;
use App\Crawler\ResourceCrawler;
use App\Entity\ResourceInterface;
use App\Factory\FactoryRegistry;
use App\Factory\MarketCategoryFactoryInterface;
use App\Factory\ProfessionFactoryInterface;
use App\Factory\RecipeItemFactoryInterface;
use App\Repository\MarketCategoryRepository;
use App\Repository\ProfessionRepository;
use App\Repository\ResourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;

class ResourceRecipeExtractor implements ResourceExtractorInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var CrawlerDispatcher
     */
    protected $dispatcher;

    /**
     * @var ResourceCrawler
     */
    protected $resourceCrawler;

    /**
     * @var ResourceRepository
     */
    protected $resourceRepository;

    /**
     * @var ProfessionRepository
     */
    protected $professionRepository;

    /**
     * @var ProfessionFactoryInterface
     */
    private $professionFactory;

    /**
     * @var RecipeItemFactoryInterface
     */
    private $recipeItemFactory;

    /**
     * @var FactoryRegistry
     */
    private $factoryRegistry;

    /**
     * @var MarketCategoryRepository
     */
    private $marketCategoryRepository;

    /**
     * @var MarketCategoryFactoryInterface
     */
    private $marketCategoryFactory;

    /**
     * ResourceRecipeExtractor constructor.
     *
     * @param EntityManagerInterface         $em
     * @param CrawlerDispatcher              $dispatcher
     * @param ResourceCrawler                $resourceCrawler
     * @param ResourceRepository             $resourceRepository
     * @param ProfessionRepository           $professionRepository
     * @param MarketCategoryRepository       $marketCategoryRepository
     * @param ProfessionFactoryInterface     $professionFactory
     * @param RecipeItemFactoryInterface     $recipeItemFactory
     * @param MarketCategoryFactoryInterface $marketCategoryFactory
     * @param FactoryRegistry                $factoryRegistry
     */
    public function __construct(
        EntityManagerInterface $em,
        CrawlerDispatcher $dispatcher,
        ResourceCrawler $resourceCrawler,
        ResourceRepository $resourceRepository,
        ProfessionRepository $professionRepository,
        MarketCategoryRepository $marketCategoryRepository,
        ProfessionFactoryInterface $professionFactory,
        RecipeItemFactoryInterface $recipeItemFactory,
        MarketCategoryFactoryInterface $marketCategoryFactory,
        FactoryRegistry $factoryRegistry
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->resourceCrawler = $resourceCrawler;
        $this->resourceRepository = $resourceRepository;
        $this->professionRepository = $professionRepository;
        $this->marketCategoryRepository = $marketCategoryRepository;
        $this->professionFactory = $professionFactory;
        $this->recipeItemFactory = $recipeItemFactory;
        $this->marketCategoryFactory = $marketCategoryFactory;
        $this->factoryRegistry = $factoryRegistry;
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ResourceInterface       $resource
     *
     * @throws \App\Crawler\Exception\RuntimeException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ResourceInterface $resource): void
    {
        $panelTitle = $node->filter('.ak-panel-title');

        if ($panelTitle->count() > 0 && 'Recette' === trim($panelTitle->first()->text())) {
            $professionNode = $node->filter('.ak-panel-intro');

            if ($professionNode->count() > 0) {
                $professionName = $professionNode->first()->html();
                $professionName = trim(preg_replace('/\<span>.*\<\/span>/', '', $professionName));

                $profession = $this->professionRepository->findOneBy(['name' => $professionName]);

                $dispatchProfession = CrawlerEvents::ITEM_RESOURCE_UPDATED;

                if (!$profession) {
                    $profession = $this->professionFactory->createNamed($professionName);
                    $profession->setFetched();
                    $this->em->persist($profession);
                    $dispatchProfession = CrawlerEvents::ITEM_RESOURCE_CREATED;
                }

                $profession
                    ->setName($professionName)
                    ->setLastCrawler($context->getCrawlerIdentifier())
                ;

                $this->em->flush($profession);

                $profession->addRecipe($resource);

                $this->dispatcher->dispatchItem($dispatchProfession, $profession, $context);
            }

            // Avoid duplicate resources and ensure that old resources are removed
            $originalRecipeItems = new ArrayCollection($resource->getRecipeItems());
            $recipeItemsUpdated = new ArrayCollection();

            $node->filter('.ak-content-list .ak-list-element')->each(function (Crawler $recipeNode) use ($context, $resource, $recipeItemsUpdated) {
                $mainContentNode = $recipeNode->filter('.ak-main-content');
                $usedInCraftName = $mainContentNode->filter('.ak-title .ak-linker')->text();
                $type = $mainContentNode->filter('.ak-content .ak-text')->text();
                $level = filter_var($mainContentNode->filter('.ak-aside')->text(), FILTER_SANITIZE_NUMBER_INT);

                $quantity = filter_var($recipeNode->filter('.ak-front')->text(), FILTER_SANITIZE_NUMBER_INT);

                $subresource = $this->resourceRepository->findOneBy(['name' => $usedInCraftName]);
                $subresourceNode = $recipeNode->filter('.ak-main-content .ak-title');

                if ($subresource) {
                    $this->manageRecipeItem($resource, $subresource, $quantity, $recipeItemsUpdated);
                } else {
                    // If we only do partial crawling, just create simple item
                    if (!$context->getAutoDiscover()) {
                        $factory = $this->factoryRegistry->getFactoryFromType($type);
                        if ($factory) {
                            $marketCategory = $this->marketCategoryRepository->findOneBy(['name' => $type]);

                            $dispatchMarketCategory = false;
                            if (!$marketCategory) {
                                $marketCategory = $this->marketCategoryFactory->createNew();
                                $marketCategory->setName($type);
                                $dispatchMarketCategory = true;
                            }

                            $subresource = $factory->createNew();
                            $subresource
                                ->setName($usedInCraftName)
                                ->setLevel($level)
                                ->setMarketCategory($marketCategory)
                                ->setLastCrawler($context->getCrawlerIdentifier());

                            $this->manageRecipeItem($resource, $subresource, $quantity, $recipeItemsUpdated);

                            $this->em->persist($subresource);
                            $this->em->flush($subresource);

                            if ($dispatchMarketCategory) {
                                $this->dispatcher->dispatchItemUpdated($marketCategory, $context);
                            }

                            $this->dispatcher->dispatchItemCreated($subresource, $context);

                            return;
                        }
                    }

                    try {
                        // Is it the only way to know which resource was created ?
                        $resourceCallback = function (CrawlerItemEvent $event) use (
                            $resource,
                            $quantity,
                            $recipeItemsUpdated
                        ) {
                            $item = $event->getItem();

                            if ($item instanceof ResourceInterface) {
                                $this->manageRecipeItem($resource, $item, $quantity, $recipeItemsUpdated);
                            }
                        };

                        $this->dispatcher->addListener(
                            CrawlerEvents::ITEM_RESOURCE_CREATED,
                            $resourceCallback
                        );

                        // Auto discover resources
                        $url = $this->resourceCrawler->getRouteForItem($subresourceNode, $context);
                        if ($url) {
                            $this->resourceCrawler->crawlItem($url, $context);
                        }

                        $this->dispatcher->removeListener(
                            CrawlerEvents::ITEM_RESOURCE_CREATED,
                            $resourceCallback
                        );
                    } catch (\Throwable $exception) {
                        throw new RuntimeException('Crawling recipe items list failed', 0, $exception);
                    }
                }
            });

            foreach ($originalRecipeItems as $originalRecipeItem) {
                if (!$recipeItemsUpdated->contains($originalRecipeItem)) {
                    $resource->removeRecipeItem($originalRecipeItem);
                }
            }
        }
    }

    protected function manageRecipeItem(ResourceInterface $resource, ResourceInterface $subresource, int $quantity, ArrayCollection $recipeItemsUpdated): void
    {
        $recipeItem = null;

        foreach ($resource->getRecipeItems() as $item) {
            if ($item->getSubresource() === $subresource) {
                $recipeItem = $item;
                $recipeItemsUpdated->add($recipeItem);
                break;
            }
        }

        if (!$recipeItem) {
            $recipeItem = $this->recipeItemFactory->createNew();
        }

        $recipeItem->setSubresource($subresource);
        $recipeItem->setQuantity($quantity);
        $resource->addRecipeItem($recipeItem);
    }
}
