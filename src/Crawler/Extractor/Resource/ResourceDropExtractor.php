<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Resource;

use App\Crawler\CrawlerContextInterface;
use App\Entity\ResourceInterface;
use Symfony\Component\DomCrawler\Crawler;

class ResourceDropExtractor implements ResourceExtractorInterface
{
    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ResourceInterface       $resource
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ResourceInterface $resource): void
    {
        $panelTitle = $node->filter('.ak-panel-title');

        if ($panelTitle->count() > 0 && 'Peut être obtenu sur' === trim($panelTitle->first()->text())) {
            $resource->setDroppable();
        }
    }
}
