<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Resource;

use App\Crawler\CrawlerContextInterface;
use App\Crawler\Dispatcher\CrawlerDispatcher;
use App\Crawler\Event\CrawlerEvents;
use App\Entity\MarketCategory;
use App\Entity\MarketCategoryInterface;
use App\Entity\ResourceInterface;
use App\Factory\MarketCategoryFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\RouterInterface;

class ResourceMetadataExtractor implements ResourceExtractorInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var CrawlerDispatcher
     */
    protected $dispatcher;

    /**
     * @var RouterInterface
     */
    protected $apiRouter;

    /**
     * @var MarketCategoryFactoryInterface
     */
    protected $marketCategoryFactory;

    /**
     * ResourceMetadataExtractor constructor.
     *
     * @param EntityManagerInterface         $em
     * @param CrawlerDispatcher              $dispatcher
     * @param RouterInterface                $apiRouter
     * @param MarketCategoryFactoryInterface $marketCategoryFactory
     */
    public function __construct(
        EntityManagerInterface $em,
        CrawlerDispatcher $dispatcher,
        RouterInterface $apiRouter,
        MarketCategoryFactoryInterface $marketCategoryFactory
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->apiRouter = $apiRouter;
        $this->marketCategoryFactory = $marketCategoryFactory;
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ResourceInterface       $resource
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ResourceInterface $resource): void
    {
        $level = $node->filter('.ak-encyclo-detail-level');

        if ($level->count() > 0) {
            $resource->setLevel((int) trim(str_replace('Niveau : ', '', $level->text())));
        }

        $description = $node->filter('.ak-encyclo-detail-right .ak-panel-content');

        if ($description->count() > 0) {
            $resource->setDescription(trim($description->text()));
        }

        $marketCategoryNode = $node->filter('.ak-encyclo-detail-type span');

        if ($marketCategoryNode->count() > 0) {
            $marketCategoryName = $marketCategoryNode->first()->text();
            /** @var MarketCategoryInterface $marketCategory */
            $marketCategory = $this->em->getRepository(MarketCategory::class)->findOneBy(['name' => $marketCategoryName]);
            $dispatchCategory = CrawlerEvents::ITEM_RESOURCE_UPDATED;

            if (!$marketCategory) {
                $marketCategory = $this->marketCategoryFactory->createNew();
                $marketCategory->setName($marketCategoryName);
                $this->em->persist($marketCategory);
                $dispatchCategory = CrawlerEvents::ITEM_RESOURCE_CREATED;
            }

            $marketCategory->addResource($resource);

            $this->em->flush($marketCategory);

            $this->dispatcher->dispatchItem($dispatchCategory, $marketCategory, $context);
        }
    }
}
