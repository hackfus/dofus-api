<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Resource;

use App\Crawler\CrawlerContextInterface;
use App\Entity\ResourceInterface;
use App\Entity\WeaponInterface;
use App\Factory\ResourceEffectFactoryInterface;
use Symfony\Component\DomCrawler\Crawler;

class ResourceCharactrtisticExtractor implements ResourceExtractorInterface
{
    protected $characteristicExpression = '/^(.*) : ((\d)((\/| à )(\d+))?)( (\(((\d) .* tour)\)|.*\)))?/ui';
    protected $typeOffset = 1;
    protected $actionPointsOffset = 3;
    protected $usesPerTurnOffset = 10;
    protected $minRangeOffset = 3;
    protected $maxRangeOffset = 6;
    protected $criticalRateOffset = 6;
    protected $criticalHitDamagesOffset = 8;
    /**
     * @var ResourceEffectFactoryInterface
     */
    protected $effectFacotry;

    /**
     * ResourceEffectExtractor constructor.
     *
     * @param ResourceEffectFactoryInterface $effectFacotry
     */
    public function __construct(
        ResourceEffectFactoryInterface $effectFacotry)
    {
        $this->effectFacotry = $effectFacotry;
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ResourceInterface       $resource
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ResourceInterface $resource): void
    {
        if (!$resource instanceof WeaponInterface) {
            return;
        }

        $panelTitle = $node->filter('.ak-panel > .ak-panel-title');

        if ($panelTitle->count() > 0 && 'Caractéristiques' === trim($panelTitle->first()->text())) {
            $contentNode = $panelTitle->nextAll();

            if ($contentNode->count() > 0
                && \in_array(
                    'ak-panel-content',
                    $contentNode->first()->extract(['class']),
                    true
                )
            ) {
                $contentNode->filter('.ak-list-element')->each(function (Crawler $node) use ($resource) {
                    $literal = trim($node->filter('.ak-title')->text());
                    \preg_match_all($this->characteristicExpression, $literal, $matches, PREG_SET_ORDER, 0);

                    if (isset($matches[0])) {
                        $parts = $matches[0];
                        switch ($parts[$this->typeOffset]) {
                            case 'PA':
                                $resource->setActionPoints($parts[$this->actionPointsOffset]);
                                break;
                            case 'Portée':
                                $resource->setMinRange($parts[$this->minRangeOffset]);
                                $resource->setMaxRange($parts[$this->maxRangeOffset] ?? $parts[$this->minRangeOffset]);
                                break;
                            case 'CC':
                                $resource->setCriticalRate($parts[$this->criticalRateOffset]);
                                $resource->setCriticalHitDamages(
                                    array_key_exists($this->criticalHitDamagesOffset, $parts) ? trim(
                                        $parts[$this->criticalHitDamagesOffset],
                                        '()'
                                    ) : 0
                                );
                                break;
                        }
                    }
                });

                foreach ($contentNode->filter('.ak-content-list')->children() as $child) {
                    $child->parentNode->removeChild($child);
                }
            }
        }
    }
}
