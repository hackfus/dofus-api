<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Resource;

use App\Crawler\CrawlerContextInterface;
use App\Entity\ResourceInterface;
use App\Factory\ResourceEffectFactoryInterface;
use Symfony\Component\DomCrawler\Crawler;

class ResourceEffectExtractor implements ResourceExtractorInterface
{
    /**
     * @var ResourceEffectFactoryInterface
     */
    protected $effectFacotry;

    /**
     * ResourceEffectExtractor constructor.
     *
     * @param ResourceEffectFactoryInterface $effectFacotry
     */
    public function __construct(ResourceEffectFactoryInterface $effectFacotry)
    {
        $this->effectFacotry = $effectFacotry;
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ResourceInterface       $resource
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ResourceInterface $resource): void
    {
        $panelTitle = $node->filter('.ak-panel > .ak-panel-title');

        if ($panelTitle->count() > 0 && 'Effets' === trim($panelTitle->first()->text())) {
            $contentNode = $panelTitle->nextAll();

            if ($contentNode->count() > 0
                && \in_array(
                    'ak-panel-content',
                    $contentNode->first()->extract(['class']),
                    true
                )
            ) {
                $contentNode->filter('.ak-list-element')->each(function (Crawler $node) use ($resource) {
                    $effect = $this->effectFacotry->createFromLiteral(trim($node->filter('.ak-title')->text()));
                    $resource->addEffect($effect);
                });

                foreach ($contentNode->filter('.ak-content-list')->children() as $child) {
                    $child->parentNode->removeChild($child);
                }
            }
        }
    }
}
