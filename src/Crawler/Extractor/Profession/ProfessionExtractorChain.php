<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Profession;

use App\Crawler\CrawlerContextInterface;
use App\Entity\ProfessionInterface;
use Symfony\Component\DomCrawler\Crawler;

class ProfessionExtractorChain implements ProfessionExtractorInterface
{
    /**
     * @var ProfessionExtractorInterface[]
     */
    private $extractors;

    /**
     * ProfessionExtractorChain constructor.
     */
    public function __construct()
    {
        $this->extractors = [];
    }

    public function addExtractor(ProfessionExtractorInterface $extractor): void
    {
        $this->extractors[] = $extractor;
    }

    public function extract(Crawler $node, CrawlerContextInterface $context, ProfessionInterface $profession): void
    {
        foreach ($this->extractors as $extractor) {
            $extractor->extract($node, $context, $profession);
        }
    }
}
