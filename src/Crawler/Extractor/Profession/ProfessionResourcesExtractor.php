<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Profession;

use App\Crawler\Client;
use App\Crawler\CrawlerContextInterface;
use App\Crawler\Event\CrawlerEvents;
use App\Crawler\Event\CrawlerItemEvent;
use App\Crawler\Exception\RuntimeException;
use App\Crawler\ResourceCrawler;
use App\Crawler\Routing\Router;
use App\Entity\ProfessionInterface;
use App\Entity\ResourceInterface;
use App\Repository\ResourceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ProfessionResourcesExtractor implements ProfessionExtractorInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var ResourceCrawler
     */
    protected $resourceCrawler;

    /**
     * @var ResourceRepository
     */
    protected $resourceRepository;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Client
     */
    private $client;

    /**
     * ResourceRecipeExtractor constructor.
     *
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     * @param ResourceCrawler          $resourceCrawler
     * @param ResourceRepository       $resourceRepository
     * @param Router                   $router
     * @param Client                   $client
     */
    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $dispatcher,
        ResourceCrawler $resourceCrawler,
        ResourceRepository $resourceRepository,
        Router $router,
        Client $client
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->resourceCrawler = $resourceCrawler;
        $this->resourceRepository = $resourceRepository;
        $this->router = $router;
        $this->client = $client;
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ProfessionInterface     $profession
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ProfessionInterface $profession): void
    {
        $id = $node->extract(['id']);

        if (\count($id) > 0) {
            $isHarvest = 'tab1' === $id[0] && 'profession_recipes_fr' !== $context->getRouteName();
            $isRecipe = 'tab2' === $id[0] || ('tab1' === $id[0] && 'profession_recipes_fr' === $context->getRouteName());

            if ($isHarvest || $isRecipe) {
                $method = $isHarvest ? 'addHarvest' : 'addRecipe';

                $this->crawlResources($node, $context, $profession, $method);
            }
        }
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ProfessionInterface     $profession
     * @param string                  $method
     *
     * @throws \App\Crawler\Exception\RuntimeException
     * @throws \App\Crawler\Exception\AccessDeniedException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    protected function crawlResources(
        Crawler $node,
        CrawlerContextInterface $context,
        ProfessionInterface $profession,
        string $method
    ): void {
        $resourceContext = $context->clone();
        $resourceContext->setRouteParams();

        $node->filter('tbody tr')->each(
            function (Crawler $rowNode) use ($resourceContext, $profession, $method) {
                $name = $rowNode->filter('td')->eq(1);

                if ($name->count() > 0) {
                    $name = $name->first()->text();
                    $resource = $this->resourceRepository->findOneBy(['name' => $name]);

                    if ($resource) {
                        $profession->{$method}($resource);
                    } else {
                        // Ignore autoDiscover == false, as we can't create resource then
                        try {
                            // This is the only way to know which resource was created
                            $resourceCallback = function (CrawlerItemEvent $event) use ($profession, $method) {
                                $item = $event->getItem();

                                if ($item instanceof ResourceInterface) {
                                    $profession->{$method}($item);
                                }
                            };

                            $this->dispatcher->addListener(
                                CrawlerEvents::ITEM_RESOURCE_CREATED,
                                $resourceCallback
                            );

                            // Auto discover resources
                            $this->resourceCrawler->crawlItem(
                                $this->resourceCrawler->getRouteForItem($rowNode, $resourceContext),
                                $resourceContext
                            );

                            $this->dispatcher->removeListener(
                                CrawlerEvents::ITEM_RESOURCE_CREATED,
                                $resourceCallback
                            );
                        } catch (\Throwable $exception) {
                            throw new RuntimeException("Crawling profession's resources failed", 0, $exception);
                        }
                    }

                    $this->em->flush($resource);
                }
            }
        );

        $pagination = $node->filter('.ak-container .pagination .active');
        if ($pagination->count() > 0) {
            $nextPages = $pagination->nextAll();

            if ($nextPages->count() > 0) {
                $page = trim($nextPages->first()->text());

                if ($page) {
                    $resourceContext = $context->clone();
                    $resourceContext->mergeRouteParams(['page' => $page]);
                    $resourceContext->setRouteName('profession_recipes_fr');

                    $url = $this->router->generate($resourceContext->getRouteName(), $resourceContext->getRouteParams());
                    $crawler = $this->client->createCrawler($url);
                    $crawler->filter('.ak-container .ak-tab')->each(function (Crawler $node) use ($resourceContext, $profession) {
                        $this->extract($node, $resourceContext, $profession);
                    });
                }
            }
        }
    }
}
