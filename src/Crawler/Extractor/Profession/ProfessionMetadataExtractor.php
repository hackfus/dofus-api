<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Extractor\Profession;

use App\Crawler\CrawlerContextInterface;
use App\Entity\ProfessionInterface;
use App\Repository\MarketCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;

class ProfessionMetadataExtractor implements ProfessionExtractorInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var RouterInterface
     */
    protected $apiRouter;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository|MarketCategoryRepository
     */
    protected $marketCategoryRepository;

    /**
     * ResourceMetadataExtractor constructor.
     *
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     * @param RouterInterface          $apiRouter
     */
    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $dispatcher,
        RouterInterface $apiRouter
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->apiRouter = $apiRouter;
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     * @param ProfessionInterface     $profession
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    public function extract(Crawler $node, CrawlerContextInterface $context, ProfessionInterface $profession): void
    {
        $panelTitle = $node->filter('.ak-panel > .ak-panel-title');

        if ($panelTitle->count() > 0 && 'Description du métier' === trim($panelTitle->first()->text())) {
            $contentNode = $panelTitle->nextAll();

            if ($contentNode->count() > 0
                && \in_array(
                    'ak-panel-content',
                    $contentNode->first()->extract(['class']),
                    true
                )
            ) {
                $profession->setDescription(trim($contentNode->first()->text()));
            }
        }
    }
}
