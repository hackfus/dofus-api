<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

class CrawlerRegistry
{
    /**
     * @var CrawlerInterface[]
     */
    private $crawlers = [];

    public function addCrawler(CrawlerInterface $crawler, string $alias): self
    {
        $this->crawlers[$alias] = $crawler;

        return $this;
    }

    public function removeCrawler(string $alias): void
    {
        if ($this->hasCrawler($alias)) {
            unset($this->crawlers[$alias]);
        }
    }

    /**
     * @return null|\Generator
     */
    public function getCrawlers(): ?\Generator
    {
        foreach ($this->crawlers as $alias => $crawler) {
            yield $alias => $crawler;
        }
    }

    public function getCrawler(string $alias): ?CrawlerInterface
    {
        return $this->hasCrawler($alias) ? $this->crawlers[$alias] : null;
    }

    public function hasCrawler(string $alias): bool
    {
        return \array_key_exists($alias, $this->crawlers);
    }
}
