<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

class EquipmentCrawler extends ResourceCrawler
{
    public static function getCrawlerAlias(): string
    {
        return 'equipment';
    }

    protected function getListRouteName(): string
    {
        return 'equipments_fr';
    }

    protected function getItemRouteName(): string
    {
        return 'equipment_fr';
    }

    protected function getApiItemRouteName(): ?string
    {
        // Be carefull as this route name is singular
        return 'api_equipment_get_item';
    }
}
