<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

class WeaponCrawler extends ResourceCrawler
{
    public static function getCrawlerAlias(): string
    {
        return 'weapon';
    }

    protected function getListRouteName(): string
    {
        return 'weapons_fr';
    }

    protected function getItemRouteName(): string
    {
        return 'weapon_fr';
    }

    protected function getApiItemRouteName(): ?string
    {
        return 'api_weapons_get_item';
    }
}
