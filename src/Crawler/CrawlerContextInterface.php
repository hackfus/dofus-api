<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

interface CrawlerContextInterface
{
    public function getCrawlerIdentifier(): string;

    public function setRouteName(string $routeName): self;

    public function getRouteName(): string;

    public function setRouteParams(array $routeParams = []): self;

    public function getRouteParams(): array;

    public function mergeRouteParams(array $params): self;

    public function unsetRouteParams(string $name): self;

    public function getParentContext(): self;

    public function clone(): self;

    public function toArray(): array;

    public function getAutoDiscover(): bool;

    public function getPageMax(): ?int;

    public function getCrawlerAlias(): string;

    public function setCrawlerAlias(string $alias): self;

    public function getItemsCount(): int;

    public function setItemsCount(int $count): self;
}
