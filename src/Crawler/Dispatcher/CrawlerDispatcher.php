<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Dispatcher;

use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\Metadata\Resource\Factory\ShortNameResourceMetadataFactory;
use App\Crawler\CrawlerContextInterface;
use App\Crawler\Event\CrawlerEvents;
use App\Crawler\Event\CrawlerItemEvent;
use App\Crawler\Routing\RouteNameGenerator;
use App\Entity\IdentifiableInterface;
use App\Entity\NameableInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;

class CrawlerDispatcher implements EventDispatcherInterface
{
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var RouterInterface
     */
    protected $apiRouter;

    /**
     * @var ShortNameResourceMetadataFactory
     */
    protected $shortNameResourceMetadataFactory;

    /**
     * CrawlerDispatcher constructor.
     *
     * @param EventDispatcherInterface         $dispatcher
     * @param RouterInterface                  $apiRouter
     * @param ShortNameResourceMetadataFactory $shortNameResourceMetadataFactory
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        RouterInterface $apiRouter,
        ShortNameResourceMetadataFactory $shortNameResourceMetadataFactory
    ) {
        $this->dispatcher = $dispatcher;
        $this->apiRouter = $apiRouter;
        $this->shortNameResourceMetadataFactory = $shortNameResourceMetadataFactory;
    }

    public function dispatchItem(string $eventName, NameableInterface $item, CrawlerContextInterface $context): Event
    {
        $route = $this->createRoute($item);

        return $this->dispatch(
            $eventName,
            new CrawlerItemEvent($item, $route, $context)
        );
    }

    public function dispatchItemCreated(NameableInterface $item, CrawlerContextInterface $context): Event
    {
        $route = $this->createRoute($item);

        return $this->dispatch(
            CrawlerEvents::ITEM_RESOURCE_CREATED,
            new CrawlerItemEvent($item, $route, $context)
        );
    }

    public function dispatchItemUpdated(NameableInterface $item, CrawlerContextInterface $context): Event
    {
        $route = $this->createRoute($item);

        return $this->dispatch(
            CrawlerEvents::ITEM_RESOURCE_UPDATED,
            new CrawlerItemEvent($item, $route, $context)
        );
    }

    public function dispatchItemIgnored(NameableInterface $item, CrawlerContextInterface $context): Event
    {
        $route = $this->createRoute($item);

        return $this->dispatch(
            CrawlerEvents::ITEM_RESOURCE_IGNORED,
            new CrawlerItemEvent($item, $route, $context)
        );
    }

    /**
     * Dispatches an event to all registered listeners.
     *
     * @param string $eventName The name of the event to dispatch. The name of
     *                          the event is the name of the method that is
     *                          invoked on listeners.
     * @param Event  $event     The event to pass to the event handlers/listeners
     *                          If not supplied, an empty Event instance is created
     *
     * @return Event
     */
    public function dispatch($eventName, Event $event = null): Event
    {
        return $this->dispatcher->dispatch($eventName, $event);
    }

    /**
     * Adds an event listener that listens on the specified events.
     *
     * @param string   $eventName The event to listen on
     * @param callable $listener  The listener
     * @param int      $priority  The higher this value, the earlier an event
     *                            listener will be triggered in the chain (defaults to 0)
     */
    public function addListener($eventName, $listener, $priority = 0): void
    {
        $this->dispatcher->addListener($eventName, $listener, $priority);
    }

    /**
     * Adds an event subscriber.
     * The subscriber is asked for all the events he is
     * interested in and added as a listener for these events.
     *
     * @param EventSubscriberInterface $subscriber
     */
    public function addSubscriber(EventSubscriberInterface $subscriber): void
    {
        $this->dispatcher->addSubscriber($subscriber);
    }

    /**
     * Removes an event listener from the specified events.
     *
     * @param string   $eventName The event to remove a listener from
     * @param callable $listener  The listener to remove
     */
    public function removeListener($eventName, $listener): void
    {
        $this->dispatcher->removeListener($eventName, $listener);
    }

    public function removeSubscriber(EventSubscriberInterface $subscriber): void
    {
        $this->dispatcher->removeSubscriber($subscriber);
    }

    /**
     * Gets the listeners of a specific event or all listeners sorted by descending priority.
     *
     * @param string $eventName The name of the event
     *
     * @return array The event listeners for the specified event, or all event listeners by event name
     */
    public function getListeners($eventName = null): array
    {
        return $this->dispatcher->getListeners($eventName);
    }

    /**
     * Gets the listener priority for a specific event.
     * Returns null if the event or the listener does not exist.
     *
     * @param string   $eventName The name of the event
     * @param callable $listener  The listener
     *
     * @return null|int The event listener priority
     */
    public function getListenerPriority($eventName, $listener): ?int
    {
        return $this->dispatcher->getListenerPriority($eventName, $listener);
    }

    /**
     * Checks whether an event has any registered listeners.
     *
     * @param string $eventName The name of the event
     *
     * @return bool true if the specified event has any listeners, false otherwise
     */
    public function hasListeners($eventName = null): bool
    {
        return $this->dispatcher->hasListeners($eventName);
    }

    /**
     * @param IdentifiableInterface|NameableInterface $item
     *
     * @return string
     */
    protected function createRoute($item): string
    {
        $class = \get_class($item);
        $class = ltrim($class, 'Proxies\\__CG__\\');
        $resourceMetadata = $this->shortNameResourceMetadataFactory->create($class);

        $route = $this->apiRouter->generate(
            RouteNameGenerator::generate(
                'get',
                $resourceMetadata->getShortName(),
                OperationType::ITEM
            ),
            ['id' => $item->getId()]
        );

        return $route;
    }
}
