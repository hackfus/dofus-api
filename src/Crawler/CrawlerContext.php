<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

use Symfony\Component\OptionsResolver\OptionsResolver;

class CrawlerContext implements CrawlerContextInterface
{
    /**
     * @var array
     */
    private $options;

    /**
     * @var OptionsResolver
     */
    private $resolver;

    /**
     * CrawlerContext constructor.
     *
     * @param string $id
     * @param array  $options
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function __construct(?string $id = null, array $options = [])
    {
        $this->resolver = new OptionsResolver();
        $this->configureOptions($this->resolver);

        $this->options = $this->resolver->resolve(\array_merge(['id' => $id], $options));
    }

    public function getCrawlerIdentifier(): string
    {
        return $this->options['id'];
    }

    public function setRouteName(string $routeName): CrawlerContextInterface
    {
        $this->options = $this->resolver->resolve(array_merge($this->options, ['route_name' => $routeName]));

        return $this;
    }

    public function getRouteName(): string
    {
        return $this->options['route_name'];
    }

    public function setRouteParams(array $routeParams = []): CrawlerContextInterface
    {
        $this->options = $this->resolver->resolve(array_replace($this->options, ['route_params' => $routeParams]));

        return $this;
    }

    public function getRouteParams(): array
    {
        return $this->options['route_params'];
    }

    public function mergeRouteParams(array $params): CrawlerContextInterface
    {
        $this->options = $this->resolver->resolve(
            array_merge(
                $this->options,
                ['route_params' => array_merge($this->getRouteParams(), $params)]
            )
        );

        return $this;
    }

    public function unsetRouteParams(string $name): CrawlerContextInterface
    {
        $params = $this->getRouteParams();
        if (\array_key_exists($name, $params)) {
            unset($this->$params[$name]);
        }

        $this->options = $this->resolver->resolve(array_replace($this->options, ['route_params' => $params]));

        return $this;
    }

    public function getParentContext(): CrawlerContextInterface
    {
        return $this->options['parent'];
    }

    public function getAutoDiscover(): bool
    {
        return $this->options['auto_discover'];
    }

    public function getPageMax(): ?int
    {
        return $this->options['page_max'];
    }

    public function getCrawlerAlias(): string
    {
        return $this->options['crawler_alias'];
    }

    public function setCrawlerAlias(string $alias): CrawlerContextInterface
    {
        $this->options = $this->resolver->resolve(array_merge($this->options, ['crawler_alias' => $alias]));

        return $this;
    }

    public function getItemsCount(): int
    {
        return $this->options['items_count'];
    }

    public function setItemsCount(int $count): CrawlerContextInterface
    {
        $this->options = $this->resolver->resolve(array_merge($this->options, ['items_count' => $count]));

        return $this;
    }

    public function clone(): CrawlerContextInterface
    {
        $clone = clone $this;

        $clone->options = $this->resolver->resolve(array_replace($clone->options, ['parent' => $this]));

        return $clone;
    }

    public function toArray(): array
    {
        return \array_merge(
            $this->options,
            ['parent' => $this->options['parent'] ? $this->options['parent']->toArray() : null]
        );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'auto_discover' => true,
            'crawler_alias' => null,
            'id' => date('YmdHis'),
            'items_count' => 0,
            'page_max' => null,
            'parent' => null,
            'route_name' => null,
            'route_params' => [],
        ]);

        $resolver->setAllowedTypes('auto_discover', ['bool']);
        $resolver->setAllowedTypes('crawler_alias', ['null', 'string']);
        $resolver->setAllowedTypes('id', ['null', 'string']);
        $resolver->setAllowedTypes('items_count', ['int']);
        $resolver->setAllowedTypes('parent', ['null', CrawlerContextInterface::class]);
        $resolver->setAllowedTypes('page_max', ['null', 'int']);
        $resolver->setAllowedTypes('route_name', ['null', 'string']);
        $resolver->setAllowedTypes('route_params', ['array', 'string[]']);
    }
}
