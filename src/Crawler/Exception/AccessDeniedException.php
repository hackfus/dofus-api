<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Exception;

class AccessDeniedException extends \RuntimeException
{
    public function __construct(string $message = 'Access Denied.', \Exception $previous = null)
    {
        parent::__construct($message, 403, $previous);
    }
}
