<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

use App\Crawler\Dispatcher\CrawlerDispatcher;
use App\Crawler\Event\CrawlerEvents;
use App\Crawler\Event\CrawlerListEvent;
use App\Crawler\Routing\Router;
use App\Factory\FactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\RouterInterface;

abstract class AbstractCrawler implements CrawlerInterface
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var RouterInterface
     */
    protected $apiRouter;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var CrawlerDispatcher
     */
    protected $dispatcher;

    /**
     * @var FactoryInterface
     */
    protected $factory;

    /**
     * @var Client
     */
    protected $client;

    /**
     * ResourceCrawler constructor.
     *
     * @param EntityManagerInterface $em
     * @param CrawlerDispatcher      $dispatcher
     * @param RouterInterface        $apiRouter
     * @param Router                 $router
     * @param FactoryInterface       $factory
     * @param Client                 $client
     */
    public function __construct(
        EntityManagerInterface $em,
        CrawlerDispatcher $dispatcher,
        RouterInterface $apiRouter,
        Router $router,
        FactoryInterface $factory,
        Client $client
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->apiRouter = $apiRouter;
        $this->router = $router;
        $this->factory = $factory;
        $this->client = $client;
    }

    /**
     * @param CrawlerContextInterface $context
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \App\Crawler\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     */
    public function crawlList(CrawlerContextInterface $context): void
    {
        $context->setRouteName($this->getListRouteName());
        $route = $this->generateURL($context, Router::ABSOLUTE_URL);

        $crawler = $this->client->createCrawler($route);

        $itemsCount = (int) $crawler->filter('.ak-container .ak-list-options .ak-list-info strong')->text();
        $context->setItemsCount($itemsCount);

        $eventListBefore = new CrawlerListEvent($context, $route);
        $this->dispatcher->dispatch(CrawlerEvents::CRAWLER_LIST_BEFORE, $eventListBefore);

        if ($eventListBefore->isPropagationStopped()) {
            return;
        }

        $this->crawlerFromList($context->clone(), $crawler);

        $eventListAfter = new CrawlerListEvent($context, $route);
        $this->dispatcher->dispatch(CrawlerEvents::CRAWLER_LIST_AFTER, $eventListAfter);
        if ($eventListAfter->isPropagationStopped()) {
            return;
        }

        $this->handleNextPage($crawler, $context);
    }

    /**
     * @param string                  $url
     * @param CrawlerContextInterface $context
     *
     * @throws \App\Crawler\Exception\AccessDeniedException
     */
    public function crawlItem(string $url, CrawlerContextInterface $context): void
    {
        $eventItemBefore = new CrawlerListEvent($context, $url);
        $this->dispatcher->dispatch(CrawlerEvents::CRAWLER_ITEM_BEFORE, $eventItemBefore);
        if ($eventItemBefore->isPropagationStopped()) {
            return;
        }

        $crawler = $this->client->createCrawler($url);

        $this->createItem($crawler, $context->clone());

        $eventItemAfter = new CrawlerListEvent($context, $url);
        $this->dispatcher->dispatch(CrawlerEvents::CRAWLER_ITEM_AFTER, $eventItemAfter);
        if ($eventItemAfter->isPropagationStopped()) {
            return;
        }
    }

    /**
     * @param CrawlerContextInterface $context
     * @param int                     $referenceType
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     *
     * @return string
     */
    protected function generateURL(CrawlerContextInterface $context, $referenceType = Router::ABSOLUTE_PATH): string
    {
        return $this->router->generate($context->getRouteName(), $context->getRouteParams(), $referenceType);
    }

    /**
     * @param Crawler                 $node
     * @param CrawlerContextInterface $context
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     *
     * @return null|string
     */
    public function getRouteForItem(Crawler $node, CrawlerContextInterface $context): ?string
    {
        $link = $node->filter('.ak-linker')->first()->filter('a')->extract(['href']);
        $link = $link[0] ?? null;

        if (!$link) {
            $link = $node->filter('.ak-title')->first()->filter('a')->extract(['href']);
            $link = $link[0] ?? null;
        }

        if (!$link) {
            $link = $node->filter('td a')->first()->extract(['href']);
            $link = $link[0] ?? null;
        }

        if (!$link) {
            $link = $node->filter('.ak-mosaic-item-illu a')->first()->extract(['href']);
            $link = $link[0] ?? null;
        }

        if ($link) {
            // Get last part of the URL as it's the identifier
            preg_match('/\d+[^\?]*/', $link, $matches, PREG_OFFSET_CAPTURE);

            if (isset($matches[0][0])) {
                $context->setRouteName($this->getItemRouteName());
                // Override route params as we don't need other extras
                $context->setRouteParams(['id' => $matches[0][0]]);

                return $this->generateURL($context, Router::ABSOLUTE_URL);
            }
        }

        return null;
    }

    /**
     * @param Crawler                 $crawler
     * @param CrawlerContextInterface $context
     *
     * @throws \App\Crawler\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     */
    public function handleNextPage(Crawler $crawler, CrawlerContextInterface $context): void
    {
        $context->getPageMax();
        $pagination = $crawler->filter('.ak-container .pagination .active');
        if ($pagination->count() > 0 && (int) $pagination->text() < $context->getPageMax()) {
            $nextPages = $pagination->nextAll();

            if ($nextPages->count() > 0) {
                $page = (int) $nextPages->first()->text();

                if ($page) {
                    $context->mergeRouteParams(['page' => $page]);

                    $this->crawlList($context);
                }
            }
        }
    }

    abstract protected function getListRouteName(): string;

    abstract protected function getItemRouteName(): string;

    abstract protected function crawlerFromList(CrawlerContextInterface $context, Crawler $crawler): void;

    abstract protected function createItem(Crawler $crawler, CrawlerContextInterface $context): void;
}
