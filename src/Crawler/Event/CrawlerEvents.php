<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Event;

class CrawlerEvents
{
    public const CRAWLER_LIST_BEFORE = 'crawler.list.before';
    public const CRAWLER_LIST_AFTER = 'crawler.list.after';
    public const CRAWLER_ITEM_BEFORE = 'crawler.item.before';
    public const CRAWLER_ITEM_AFTER = 'crawler.item.after';

    public const ITEM_RESOURCE_CREATED = 'crawler.item.resource.created';
    public const ITEM_RESOURCE_UPDATED = 'crawler.item.resource.updated';
    public const ITEM_RESOURCE_IGNORED = 'crawler.item.resource.ignored';
}
