<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Event;

use App\Crawler\CrawlerContextInterface;
use App\Entity\NameableInterface;
use Symfony\Component\EventDispatcher\Event;

class CrawlerItemEvent extends Event
{
    /**
     * @var NameableInterface
     */
    protected $item;
    /**
     * @var string
     */
    private $url;
    /**
     * @var CrawlerContextInterface
     */
    private $context;

    /**
     * CrawlerItemCreatedEvent constructor.
     *
     * @param NameableInterface       $item
     * @param string                  $url
     * @param CrawlerContextInterface $context
     */
    public function __construct(NameableInterface $item, string $url, CrawlerContextInterface $context)
    {
        $this->item = $item;
        $this->url = $url;
        $this->context = $context;
    }

    /**
     * @return string
     */
    public function getCrawlerAlias(): string
    {
        return $this->context->getCrawlerAlias();
    }

    /**
     * @return NameableInterface
     */
    public function getItem(): NameableInterface
    {
        return $this->item;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return CrawlerContextInterface
     */
    public function getContext(): CrawlerContextInterface
    {
        return $this->context;
    }
}
