<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Event;

use App\Crawler\CrawlerContextInterface;
use Symfony\Component\EventDispatcher\Event;

class CrawlerListEvent extends Event
{
    private $context;

    private $url;

    /**
     * CrawlerListEvent constructor.
     *
     * @param CrawlerContextInterface $context
     * @param string                  $url
     */
    public function __construct(CrawlerContextInterface $context, string $url)
    {
        $this->context = $context;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getCrawlerAlias(): string
    {
        return $this->context->getCrawlerAlias();
    }

    /**
     * @return int
     */
    public function getItemsCount(): int
    {
        return $this->context->getItemsCount();
    }

    /**
     * @return CrawlerContextInterface
     */
    public function getContext(): CrawlerContextInterface
    {
        return $this->context;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
