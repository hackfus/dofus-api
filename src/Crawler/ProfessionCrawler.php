<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

use App\Crawler\Event\CrawlerEvents;
use App\Crawler\Exception\RuntimeException;
use App\Crawler\Extractor\Profession\ProfessionExtractorChain;
use App\Crawler\Extractor\Profession\ProfessionExtractorInterface;
use App\Entity\Profession;
use App\Entity\ProfessionInterface;
use Symfony\Component\DomCrawler\Crawler;

class ProfessionCrawler extends AbstractCrawler
{
    protected $professionRepository;

    /**
     * @var ProfessionExtractorInterface
     */
    protected $professionExtractor;

    /**
     * @required Mark method as required for autowiring call
     *
     * @param ProfessionExtractorChain $professionExtractor
     */
    public function setProfessionExtractor(ProfessionExtractorChain $professionExtractor): void
    {
        $this->professionExtractor = $professionExtractor;
    }

    public static function getCrawlerAlias(): string
    {
        return 'profession';
    }

    protected function getListRouteName(): string
    {
        return 'professions_fr';
    }

    protected function getItemRouteName(): string
    {
        return 'profession_fr';
    }

    /**
     * @param CrawlerContextInterface $context
     * @param Crawler                 $crawler
     *
     * @throws \App\Crawler\Exception\RuntimeException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     * @throws \RuntimeException
     */
    protected function crawlerFromList(CrawlerContextInterface $context, Crawler $crawler): void
    {
        $crawler->filter('.ak-main-content .ak-mosaic-item')->each(
            function (Crawler $node) use ($context) {
                try {
                    $professionNode = $node->filter('.ak-mosaic-item-name a');

                    if ($professionNode->count() > 0) {
                        $text = trim($professionNode->text());

                        if (!$text) {
                            return;
                        }

                        /** @var ProfessionInterface $profession */
                        $profession = $this->getProfessionRepository()->findOneBy(['name' => $text]);

                        // Check if this profession is not already up to date
                        if ($profession && $profession->getLastCrawler() === $context->getCrawlerIdentifier()) {
                            return;
                        }
                    }

                    $itemContext = $context->clone();
                    $itemContext->setRouteParams();

                    $this->crawlItem($this->getRouteForItem($node, $itemContext), $itemContext);
                } catch (\Throwable $exception) {
                    throw new RuntimeException('Crawling professions from list failed', 0, $exception);
                }
            }
        );
    }

    /**
     * @param Crawler                 $crawler
     * @param CrawlerContextInterface $context
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    protected function createItem(Crawler $crawler, CrawlerContextInterface $context): void
    {
        $name = trim($crawler->filter('h1')->text());

        /** @var ProfessionInterface $profession */
        $profession = $this->getProfessionRepository()->findOneBy(['name' => $name]);

        $dispatchProfession = CrawlerEvents::ITEM_RESOURCE_UPDATED;

        if (!$profession) {
            $profession = new Profession();
            $profession->setFetched();
            $this->em->persist($profession);
            $dispatchProfession = CrawlerEvents::ITEM_RESOURCE_CREATED;
        }

        $profession
            ->setName($name)
            ->setLastCrawler($context->getCrawlerIdentifier())
        ;

        $crawler->filter('.ak-container .ak-panel, .ak-container .ak-tab')->each(
                function (Crawler $node) use ($context, $profession) {
                    $this->professionExtractor->extract($node, $context, $profession);
                }
            );

        $this->em->flush();

        $this->dispatcher->dispatchItem($dispatchProfession, $profession, $context);
    }

    /**
     * @return \App\Repository\ProfessionRepository|\Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
     */
    protected function getProfessionRepository()
    {
        if (!$this->professionRepository) {
            $this->professionRepository = $this->em->getRepository(Profession::class);
        }

        return $this->professionRepository;
    }
}
