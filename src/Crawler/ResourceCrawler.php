<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

use App\Crawler\Event\CrawlerEvents;
use App\Crawler\Event\CrawlerItemEvent;
use App\Crawler\Exception\RuntimeException;
use App\Crawler\Extractor\Resource\ResourceExtractorChain;
use App\Crawler\Extractor\Resource\ResourceExtractorInterface;
use App\Entity\MarketCategory;
use App\Entity\Resource;
use App\Entity\ResourceInterface;
use Symfony\Component\DomCrawler\Crawler;

class ResourceCrawler extends AbstractCrawler
{
    protected $resourceRepository;

    protected $marketCategoryRepository;

    /**
     * @var ResourceExtractorInterface
     */
    protected $resourceExtractor;

    /**
     * @required Mark method as required for autowiring call
     *
     * @param ResourceExtractorChain $resourceExtractor
     */
    public function setResourceExtractor(ResourceExtractorChain $resourceExtractor): void
    {
        $this->resourceExtractor = $resourceExtractor;
    }

    public static function getCrawlerAlias(): string
    {
        return 'resource';
    }

    protected function getListRouteName(): string
    {
        return 'resources_fr';
    }

    protected function getItemRouteName(): string
    {
        return 'resource_fr';
    }

    protected function getApiItemRouteName(): ?string
    {
        return 'api_resources_get_item';
    }

    /**
     * @param CrawlerContextInterface $context
     * @param Crawler                 $crawler
     *
     * @throws \App\Crawler\Exception\RuntimeException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     * @throws \RuntimeException
     */
    protected function crawlerFromList(CrawlerContextInterface $context, Crawler $crawler): void
    {
        $crawler->filter('.ak-container tr')->each(
            function (Crawler $node) use ($context) {
                try {
                    $resourceNode = $node->filter('.ak-linker')->eq(1);

                    if ($resourceNode->count() > 0) {
                        /** @var ResourceInterface $resource */
                        $resource = $this->getResourceRepository()->findOneBy(['name' => $resourceNode->text()]);

                        // Check if this resource is not already up to date
                        if ($resource && $resource->getLastCrawler() === $context->getCrawlerIdentifier()) {
                            $this->dispatcher->dispatchItemIgnored($resource, $context);

                            return;
                        }
                    }
                    $itemUrl = $this->getRouteForItem($node, $context);
                    if ($itemUrl) {
                        $this->crawlItem($itemUrl, $context);
                    }
                } catch (\Throwable $exception) {
                    throw new RuntimeException('Crawling resources from list failed', 0, $exception);
                }
            }
        );
    }

    /**
     * @param Crawler                 $crawler
     * @param CrawlerContextInterface $context
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\ExceptionInterface
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    protected function createItem(Crawler $crawler, CrawlerContextInterface $context): void
    {
        $name = trim($crawler->filter('h1')->text());

        $resource = $this->getResourceRepository()->findOneBy(['name' => $name]);

        $dispatchResource = CrawlerEvents::ITEM_RESOURCE_UPDATED;

        if (!$resource) {
            $resource = $this->factory->createNew();
            $resource->setFetched();
            $resource->setDroppable(false);
            $this->em->persist($resource);
            $dispatchResource = CrawlerEvents::ITEM_RESOURCE_CREATED;
        }

        $resource
            ->setName($name)
            ->setLastCrawler($context->getCrawlerIdentifier())
        ;

        if ($resource->isFetched()) {
            // Reset original effects to stay up-to-date with remote data
            $resource->getEffects()->clear();

            $crawler->filter('.ak-main-container .ak-container.ak-panel')->each(
                function (Crawler $node) use ($context, $resource) {
                    $this->resourceExtractor->extract($node, $context, $resource);
                }
            );
        }

        $this->em->flush();

        if ($dispatchResource) {
            $resourceRoute = $this->apiRouter->generate($this->getApiItemRouteName(), ['id' => $resource->getId()]);
            $this->dispatcher->dispatch(
                $dispatchResource,
                new CrawlerItemEvent($resource, $resourceRoute, $context)
            );
        }
    }

    /**
     * @return \App\Repository\ResourceRepository|\Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
     */
    protected function getResourceRepository()
    {
        if (!$this->resourceRepository) {
            $this->resourceRepository = $this->em->getRepository(Resource::class);
        }

        return $this->resourceRepository;
    }

    /**
     * @return \App\Repository\MarketCategoryRepository|\Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
     */
    protected function getMarketCategoryRepository()
    {
        if (!$this->marketCategoryRepository) {
            $this->marketCategoryRepository = $this->em->getRepository(MarketCategory::class);
        }

        return $this->marketCategoryRepository;
    }
}
