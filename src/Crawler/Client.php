<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler;

use App\Crawler\Exception\AccessDeniedException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DomCrawler\Crawler;

class Client extends \GuzzleHttp\Client
{
    /**
     * {@inheritdoc}
     */
    public function __construct($baseUri, array $config = [])
    {
        $defaultConfig = [
            'base_uri' => $baseUri,
            RequestOptions::DELAY => 1,
        ];

        parent::__construct(\array_merge($defaultConfig, $config));
    }

    /**
     * @param string $route
     *
     * @throws \App\Crawler\Exception\AccessDeniedException
     *
     * @return Crawler
     */
    public function createCrawler(string $route): Crawler
    {
        $response = $this->get($route);
        $statusCode = $response->getStatusCode();
        $html = (string) $response->getBody();

        if (!($statusCode >= 200 && $statusCode < 300)) {
            throw new AccessDeniedException(sprintf('Could not crawl due to status code %s', $statusCode));
        }

        return new Crawler($html);
    }
}
