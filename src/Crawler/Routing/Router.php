<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Crawler\Routing;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;

class Router implements RouterInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var array
     */
    private $crawlerConf;

    public function __construct(RouterInterface $router, array $crawlerConf)
    {
        $this->router = $router;
        $this->crawlerConf = $crawlerConf;
    }

    /**
     * {@inheritdoc}
     */
    public function setContext(RequestContext $context): void
    {
        $this->router->setContext($context);
    }

    /**
     * {@inheritdoc}
     */
    public function getContext(): RequestContext
    {
        return $this->router->getContext();
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteCollection(): RouteCollection
    {
        return $this->router->getRouteCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH): string
    {
        $routerContext = $this->router->getContext();
        $routerOriginalContext = clone $routerContext;

        if (self::ABSOLUTE_URL === $referenceType) {
            // Alter router context to generate full URL for crawler calls
            if (\array_key_exists('path_info', $this->crawlerConf)) {
                $routerContext->setPathInfo($this->crawlerConf['path_info']);
            }
            $routerContext->setScheme($this->crawlerConf['scheme']);
            $routerContext->setHost($this->crawlerConf['host']);
        }

        $route = $this->router->generate($name, $parameters, $referenceType);

        if (self::ABSOLUTE_URL === $referenceType) {
            $this->router->setContext($routerOriginalContext);
        }

        return $route;
    }

    /**
     * {@inheritdoc}
     */
    public function match($pathinfo): array
    {
        return $this->router->match($pathinfo);
    }
}
