<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CrawlerProxyController extends Controller
{
    /**
     * > Resources.
     *
     * @Route("/fr/mmorpg/encyclopedie/ressources", name="resources_fr")
     * @Route("/en/mmorpg/encyclopedie/resources", name="resources_en")
     *
     * @Route("/fr/mmorpg/encyclopedie/ressources/{id}", name="resource_fr")
     * @Route("/en/mmorpg/encyclopedie/resources/{id}", name="resource_en")
     * < Resources
     *
     * > Weapons
     * @Route("/fr/mmorpg/encyclopedie/armes", name="weapons_fr")
     * @Route("/en/mmorpg/encyclopedie/weapons", name="weapons_en")
     *
     * @Route("/fr/mmorpg/encyclopedie/armes/{id}", name="weapon_fr")
     * @Route("/en/mmorpg/encyclopedie/weapons/{id}", name="weapons_en")
     * < Weapons
     *
     * > Equipments
     * @Route("/fr/mmorpg/encyclopedie/equipements", name="equipments_fr")
     * @Route("/en/mmorpg/encyclopedie/equipments", name="equipments_en")
     *
     * @Route("/fr/mmorpg/encyclopedie/equipements/{id}", name="equipment_fr")
     * @Route("/en/mmorpg/encyclopedie/equipments/{id}", name="equipments_en")
     * < Equipments
     *
     * > Professions
     * @Route("/fr/mmorpg/encyclopedie/metiers", name="professions_fr")
     * @Route("/en/mmorpg/encyclopedie/professions", name="professions_en")
     *
     * @Route("/fr/mmorpg/encyclopedie/metiers/{id}", name="profession_fr")
     * @Route("/en/mmorpg/encyclopedie/professions/{id}", name="profession_en")
     *
     * @Route("/fr/mmorpg/encyclopedie/metiers/{id}/recettes", name="profession_recipes_fr")
     * @Route("/en/mmorpg/encyclopedie/professions/{id}/recettes", name="profession_recipes_en")
     * < Professions
     *
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function index(): void
    {
        throw $this->createAccessDeniedException();
    }
}
