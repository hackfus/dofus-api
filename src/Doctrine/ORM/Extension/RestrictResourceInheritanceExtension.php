<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Doctrine\ORM\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\EquipmentInterface;
use App\Entity\RecipeItemInterface;
use App\Entity\ResourceInterface;
use App\Entity\WeaponInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class RestrictResourceInheritanceExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    protected static $resourcesClasses = [
        ResourceInterface::class,
        EquipmentInterface::class,
        WeaponInterface::class,
    ];

    protected static $operationsNames = [
        'api_resources_recipe_items_get_subresource' => ResourceInterface::class,
        'api_equipments_recipe_items_get_subresource' => EquipmentInterface::class,
        'api_weapons_recipe_items_get_subresource' => WeaponInterface::class,
    ];

    /**
     * @var Request
     */
    protected $request;

    /**
     * RestrictResourceInheritanceExtension constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getMasterRequest();
    }

    /**
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $resourceClass
     * @param null|string                 $operationName
     */
    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ): void {
        $this->addWhere($queryBuilder, $resourceClass, $operationName);
    }

    /**
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $resourceClass
     * @param array                       $identifiers
     * @param null|string                 $operationName
     * @param array                       $context
     *
     * @throws \InvalidArgumentException
     */
    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ): void {
        $this->addWhere($queryBuilder, $resourceClass, $operationName);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $resourceClass
     * @param null|string  $operationName
     */
    protected function addWhere(QueryBuilder $queryBuilder, string $resourceClass, string $operationName = null): void
    {
        // Don't be strict on autocomplete
        if ($this->request && \in_array('autocomplete', $this->request->query->get('groups', []), false)) {
            return;
        }

        $expr = $queryBuilder->expr();
        $rootAlias = $queryBuilder->getRootAliases()[0];

        if (is_subclass_of($resourceClass, ResourceInterface::class)) {
            $andX = $expr->andX($expr->isInstanceOf($rootAlias, $resourceClass));
            foreach (self::$resourcesClasses as $class) {
                if ($class !== $resourceClass && !is_subclass_of($resourceClass, $class)) {
                    $andX->add($expr->not($expr->isInstanceOf($rootAlias, $class)));
                }
            }

            $queryBuilder->andWhere($andX);
        } elseif (is_subclass_of($resourceClass, RecipeItemInterface::class)) {
            $resourceInstanceClass = self::$operationsNames[$operationName] ?? null;

            if (null !== $resourceInstanceClass) {
                foreach ($queryBuilder->getAllAliases() as $alias) {
                    if (false !== mb_strpos($alias, 'resourceCrafted')) {
                        $andX = $expr->andX($expr->isInstanceOf($alias, $resourceInstanceClass));
                        foreach (self::$resourcesClasses as $class) {
                            if ($class !== $resourceInstanceClass && !is_subclass_of($resourceInstanceClass, $class)) {
                                $andX->add($expr->not($expr->isInstanceOf($alias, $class)));
                            }
                        }

                        $queryBuilder->andWhere($andX);
                        break;
                    }
                }
            }
        }
    }
}
