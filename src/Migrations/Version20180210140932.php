<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180210140932 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE profession_recipe');
        $this->addSql('ALTER TABLE resource DROP FOREIGN KEY FK_BC91F416FDEF8996');
        $this->addSql('DROP INDEX IDX_BC91F416FDEF8996 ON resource');
        $this->addSql('ALTER TABLE resource ADD recipe_profession_id INT DEFAULT NULL, CHANGE profession_id harvest_profession_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F416F813535C FOREIGN KEY (harvest_profession_id) REFERENCES profession (id)');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F4166D1A9F50 FOREIGN KEY (recipe_profession_id) REFERENCES profession (id)');
        $this->addSql('CREATE INDEX IDX_BC91F416F813535C ON resource (harvest_profession_id)');
        $this->addSql('CREATE INDEX IDX_BC91F4166D1A9F50 ON resource (recipe_profession_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE profession_recipe (profession_id INT NOT NULL, resource_id INT NOT NULL, UNIQUE INDEX UNIQ_6562BF6289329D25 (resource_id), INDEX IDX_6562BF62FDEF8996 (profession_id), PRIMARY KEY(profession_id, resource_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE profession_recipe ADD CONSTRAINT FK_6562BF6289329D25 FOREIGN KEY (resource_id) REFERENCES resource (id)');
        $this->addSql('ALTER TABLE profession_recipe ADD CONSTRAINT FK_6562BF62FDEF8996 FOREIGN KEY (profession_id) REFERENCES profession (id)');
        $this->addSql('ALTER TABLE resource DROP FOREIGN KEY FK_BC91F416F813535C');
        $this->addSql('ALTER TABLE resource DROP FOREIGN KEY FK_BC91F4166D1A9F50');
        $this->addSql('DROP INDEX IDX_BC91F416F813535C ON resource');
        $this->addSql('DROP INDEX IDX_BC91F4166D1A9F50 ON resource');
        $this->addSql('ALTER TABLE resource ADD profession_id INT DEFAULT NULL, DROP harvest_profession_id, DROP recipe_profession_id');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F416FDEF8996 FOREIGN KEY (profession_id) REFERENCES profession (id)');
        $this->addSql('CREATE INDEX IDX_BC91F416FDEF8996 ON resource (profession_id)');
    }
}
