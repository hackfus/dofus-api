<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180128142128 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE market_category (id INT AUTO_INCREMENT NOT NULL, market_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_EBFD0C095E237E06 (name), INDEX IDX_EBFD0C09622F3F37 (market_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE market_category ADD CONSTRAINT FK_EBFD0C09622F3F37 FOREIGN KEY (market_id) REFERENCES market (id)');
        $this->addSql('ALTER TABLE resource DROP FOREIGN KEY FK_BC91F416622F3F37');
        $this->addSql('DROP INDEX IDX_BC91F416622F3F37 ON resource');
        $this->addSql('ALTER TABLE resource CHANGE market_id market_category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F4166293FF9 FOREIGN KEY (market_category_id) REFERENCES market_category (id)');
        $this->addSql('CREATE INDEX IDX_BC91F4166293FF9 ON resource (market_category_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE resource DROP FOREIGN KEY FK_BC91F4166293FF9');
        $this->addSql('DROP TABLE market_category');
        $this->addSql('DROP INDEX IDX_BC91F4166293FF9 ON resource');
        $this->addSql('ALTER TABLE resource CHANGE market_category_id market_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F416622F3F37 FOREIGN KEY (market_id) REFERENCES market (id)');
        $this->addSql('CREATE INDEX IDX_BC91F416622F3F37 ON resource (market_id)');
    }
}
