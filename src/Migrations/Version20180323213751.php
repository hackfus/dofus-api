<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180323213751 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE UNIQUE INDEX recipe_item_unique ON recipe_item (resource_crafted_id, subresource_id)');
        $this->addSql('CREATE TABLE resource_effect_type (id INT AUTO_INCREMENT NOT NULL, primary_bonus TINYINT(1) NOT NULL, cosmetic_bonus TINYINT(1) NOT NULL, label VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_18005E82EA750E8 (label), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE resource_effect ADD type_id INT DEFAULT NULL, DROP type');
        $this->addSql('ALTER TABLE resource_effect ADD CONSTRAINT FK_7E9C7B08C54C8C93 FOREIGN KEY (type_id) REFERENCES resource_effect_type (id)');
        $this->addSql('CREATE INDEX IDX_7E9C7B08C54C8C93 ON resource_effect (type_id)');
        $this->addSql('ALTER TABLE resource_effect ADD value_is_in_percent TINYINT(1) NOT NULL, DROP min_value_is_percent, DROP max_value_is_percent');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE resource_effect ADD max_value_is_percent TINYINT(1) NOT NULL, CHANGE value_is_in_percent min_value_is_percent TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE resource_effect DROP FOREIGN KEY FK_7E9C7B08C54C8C93');
        $this->addSql('DROP TABLE resource_effect_type');
        $this->addSql('DROP INDEX IDX_7E9C7B08C54C8C93 ON resource_effect');
        $this->addSql('ALTER TABLE resource_effect ADD type VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP type_id');
        $this->addSql('DROP INDEX recipe_item_unique ON recipe_item');
    }
}
