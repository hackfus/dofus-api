<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180212234426 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE resource_effect (id INT AUTO_INCREMENT NOT NULL, resource_id INT DEFAULT NULL, min_value INT NOT NULL, min_value_is_percent TINYINT(1) NOT NULL, max_value INT NOT NULL, max_value_is_percent TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, primary_bonus TINYINT(1) NOT NULL, literal VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, fetched TINYINT(1) NOT NULL, last_crawler VARCHAR(255) DEFAULT NULL, INDEX IDX_7E9C7B0889329D25 (resource_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE resource_effect ADD CONSTRAINT FK_7E9C7B0889329D25 FOREIGN KEY (resource_id) REFERENCES resource (id)');
        $this->addSql('ALTER TABLE resource CHANGE class class VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE resource_effect');
        $this->addSql('ALTER TABLE resource CHANGE class class VARCHAR(255) DEFAULT \'resource\' NOT NULL COLLATE utf8_unicode_ci');
    }
}
