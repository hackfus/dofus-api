<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Swagger;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SwaggerDecorator implements NormalizerInterface
{
    // Unwanted routes
    protected const REMOVE_PATHS = [
        '/recipe-items',
        '/recipe-items/{id}',
        '/resource-effects',
        '/resource-effects/{id}',
        '/login',
        '/register',
        '/users',
        '/users/{id}',
    ];

    // Unwanted models definitions
    protected const REMOVE_DEFINITIONS = [
        'User',
        'User-user_read',
        'User-user_read_collection',
        'User-user_write',
        'User-user_register',
    ];

    private $decorated;

    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    /**
     * @param object $object
     * @param null   $format
     * @param array  $context
     *
     * @throws \Symfony\Component\Serializer\Exception\LogicException
     * @throws \Symfony\Component\Serializer\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Serializer\Exception\CircularReferenceException
     *
     * @return array|bool|float|int|string
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        foreach (self::REMOVE_PATHS as $PATH) {
            if (isset($docs['paths'][$PATH])) {
                unset($docs['paths'][$PATH]);
            }
        }

        foreach (self::REMOVE_DEFINITIONS as $DEFINITION) {
            if (isset($docs['definitions'][$DEFINITION])) {
                unset($docs['definitions'][$DEFINITION]);
            }
        }

        return $docs;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }
}
