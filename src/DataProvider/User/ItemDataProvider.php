<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\DataProvider\User;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ItemDataProvider implements ItemDataProviderInterface
{
    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserItemDataProvider constructor.
     *
     * @param ManagerRegistry       $managerRegistry
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ManagerRegistry $managerRegistry, TokenStorageInterface $tokenStorage)
    {
        $this->managerRegistry = $managerRegistry;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Retrieves an item.
     *
     * @param string      $resourceClass
     * @param int|string  $id
     * @param null|string $operationName
     * @param array       $context
     *
     * @throws ResourceClassNotSupportedException
     *
     * @return null|User
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?User
    {
        if (User::class !== $resourceClass) {
            throw new ResourceClassNotSupportedException();
        }

        $repository = $this->managerRegistry->getRepository(User::class);

        // retrieves User from the security when hitting the route 'api_users_me' (no id needed)
        if ('me' === $id && 'get' === $operationName && null !== ($token = $this->tokenStorage->getToken()) && null !== ($user = $token->getUser())) {
            return $user;
        }

        return $repository->find($id); // Retrieves User normally for other actions
    }
}
