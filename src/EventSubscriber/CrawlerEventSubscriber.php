<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\EventSubscriber;

use App\Crawler\Event\CrawlerEvents;
use App\Crawler\Event\CrawlerItemEvent;
use App\Crawler\Event\CrawlerListEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CrawlerEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CrawlerEventSubscriber constructor.
     *
     * @param LoggerInterface $crawlerLogger
     */
    public function __construct(LoggerInterface $crawlerLogger)
    {
        $this->logger = $crawlerLogger;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * The array keys are event names and the value can be:
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     * For instance:
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))).
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            CrawlerEvents::CRAWLER_LIST_BEFORE => 'logList',
            CrawlerEvents::CRAWLER_ITEM_BEFORE => 'logList',
            CrawlerEvents::ITEM_RESOURCE_CREATED => 'logItemCreated',
        ];
    }

    public function logList(CrawlerListEvent $event): void
    {
        $this->logger->info(sprintf('Crawling %s', $event->getUrl()), $this->crawlerListEventToContext($event));
    }

    public function logItemCreated(CrawlerItemEvent $event): void
    {
        $item = $event->getItem();
        $route = $event->getUrl();
        $this->logger->info(sprintf('%s created (%s)', $item->getName(), $route), $this->crawlerItemEventToContext($event));
    }

    /**
     * @param CrawlerListEvent $event
     *
     * @return array
     */
    protected function crawlerListEventToContext(CrawlerListEvent $event): array
    {
        return ['url' => $event->getUrl(), 'context' => $event->getContext()->toArray()];
    }

    /**
     * @param CrawlerItemEvent $event
     *
     * @return array
     */
    protected function crawlerItemEventToContext(CrawlerItemEvent $event): array
    {
        return ['url' => $event->getUrl(), 'context' => $event->getContext()->toArray()];
    }
}
