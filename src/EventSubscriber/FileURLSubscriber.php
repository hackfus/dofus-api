<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\EventSubscriber;

use App\Entity\FileInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Asset\Packages;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class FileURLSubscriber implements EventSubscriber
{
    /**
     * @var UploaderHelper
     */
    protected $uploaderHelper;
    /**
     * @var Packages
     */
    private $urlPackage;

    /**
     * FileURLSubscriber constructor.
     *
     * @param UploaderHelper $uploaderHelper
     * @param Packages       $urlPackage
     */
    public function __construct(UploaderHelper $uploaderHelper, Packages $urlPackage)
    {
        $this->uploaderHelper = $uploaderHelper;
        $this->urlPackage = $urlPackage;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::postLoad,
        ];
    }

    /**
     * @param LifecycleEventArgs $event The event
     */
    public function postLoad(LifecycleEventArgs $event): void
    {
        $object = $event->getObject();

        if (!$object instanceof FileInterface) {
            return;
        }

        $path = $this->uploaderHelper->asset($object, 'imageFile');

        if ($path) {
            $object->setPath($this->urlPackage->getUrl($path));
        }
    }
}
