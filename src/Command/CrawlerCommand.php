<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Crawler\CrawlerContext;
use App\Crawler\CrawlerInterface;
use App\Crawler\CrawlerRegistry;
use App\Crawler\Event\CrawlerEvents;
use App\Crawler\Event\CrawlerItemEvent;
use App\Crawler\Event\CrawlerListEvent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Terminal;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CrawlerCommand extends Command
{
    protected static $defaultName = 'app:crawler';

    /**
     * @var CrawlerRegistry
     */
    private $registry;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $progressBars = [];

    private $cursors = [];

    /**
     * CrawlerCommand constructor.
     *
     * @param CrawlerRegistry          $registry
     * @param EventDispatcherInterface $dispatcher
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(
        CrawlerRegistry $registry,
        EventDispatcherInterface $dispatcher
    ) {
        $this->dispatcher = $dispatcher;

        parent::__construct();
        $this->registry = $registry;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Crawl DOFUS website to fill in database')
            ->addOption(
                'disable-auto-discover',
                null,
                InputOption::VALUE_NONE,
                'If set to true, each link encountered will not be explored to add data'
            )
            ->addOption(
                'list-size',
                's',
                InputOption::VALUE_OPTIONAL,
                'Number of items per page',
                48
            )
            ->addOption(
                'page-max',
                'm',
                InputOption::VALUE_OPTIONAL,
                'Page max to crawl'
            )
            ->addOption(
                'page',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Page number to start',
                1
            )
            ->addOption(
                'crawlers',
                null,
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                <<<EOT
Specify which crawler must be called. All by default. Available values are :
- equipment
- weapon
- resource
- profession
EOT
                ,
                []
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \App\Crawler\Exception\RuntimeException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     *
     * @return null|int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);

        $disableAutoDiscover = $input->getOption('disable-auto-discover');
        $listSize = (int) filter_var($input->getOption('list-size'), FILTER_SANITIZE_NUMBER_INT);
        $page = (int) filter_var($input->getOption('page'), FILTER_SANITIZE_NUMBER_INT);
        $pageMax = (int) filter_var($input->getOption('page-max'), FILTER_SANITIZE_NUMBER_INT);
        /** @var string[] $crawlers */
        $crawlers = $input->getOption('crawlers');

        $counter = [
            'crawled_pages' => 0,
            'objects_created' => [],
            'objects_updated' => [],
        ];

        $this->attachListeners($counter);

        $crawlerId = date('YmdHis');

        $this->io->comment(sprintf('Crawler identifier : %s', $crawlerId));

        try {
            if (0 === \count($crawlers)) {
                $crawlers = $this->registry->getCrawlers();
                /** @var CrawlerInterface $crawl */
                foreach ($crawlers as $crawl) {
                    $context = $this->createContext($crawlerId, $disableAutoDiscover, $listSize, $page, $pageMax);

                    $this->io->comment(sprintf('%s - Start crawling with %s crawler', date('Y-m-d H:i:s'), $crawl::getCrawlerAlias()));
                    $crawl->crawlList($context);
                }
            } else {
                /** @var string $alias */
                foreach ($crawlers as $alias) {
                    $crawl = $this->registry->getCrawler($alias);

                    if (!$crawl) {
                        $this->io->warning(sprintf('Unknown crawler %s - skipping', $alias));
                        continue;
                    }

                    $context = $this->createContext($crawlerId, $disableAutoDiscover, $listSize, $page, $pageMax);

                    $context->setCrawlerAlias($crawl::getCrawlerAlias());

                    $this->io->comment(sprintf('%s - Start crawling with %s crawler', date('Y-m-d H:i:s'), $crawl::getCrawlerAlias()));
                    $crawl->crawlList($context);

                    $this->io->section(str_repeat('-', (new Terminal())->getWidth()));
                }
            }
        } catch (\Throwable $exception) {
            $this->io->error('Crawling failed');
            $this->io->error($exception->getMessage());

            $this->io->section(str_repeat('-', (new Terminal())->getWidth()));
        }

        $this->writeStats($counter);
    }

    private function attachListeners(array &$counter): void
    {
        $this->dispatcher->addListener(CrawlerEvents::CRAWLER_LIST_BEFORE, function (CrawlerListEvent $event) {
            if (!\array_key_exists($event->getCrawlerAlias(), $this->progressBars)) {
                $formatName = sprintf('format_%s', $event->getCrawlerAlias());

                ProgressBar::setFormatDefinition($formatName,
                    ' %current%/%max% : %message% %elapsed:6s%/%estimated:-6s%'
                );

                $progressBar = new ProgressBar($this->io, $event->getItemsCount());
                $progressBar->setFormat($formatName);
                $progressBar->setMessage(sprintf('Crawling %s', $event->getUrl()));
                $progressBar->start();

                $this->progressBars[$event->getCrawlerAlias()] = $progressBar;
                $this->cursors[$event->getCrawlerAlias()] = 0;
            }
        });

        $this->dispatcher->addListener(CrawlerEvents::CRAWLER_LIST_AFTER, function () use (&$counter) {
            ++$counter['crawled_pages'];
        });

        $this->dispatcher->addListener(CrawlerEvents::CRAWLER_ITEM_BEFORE, function (CrawlerListEvent $event) {
            if (\array_key_exists($event->getCrawlerAlias(), $this->progressBars)) {
                $progressBar = $this->progressBars[$event->getCrawlerAlias()];
                ++$this->cursors[$event->getCrawlerAlias()];

                $progressBar->setMessage($event->getUrl());
                $progressBar->advance();
            }
        });

        $this->dispatcher->addListener(CrawlerEvents::CRAWLER_ITEM_AFTER, function () use (&$counter) {
            ++$counter['crawled_pages'];
        });

        $this->dispatcher->addListener(CrawlerEvents::ITEM_RESOURCE_CREATED, function (CrawlerItemEvent $event) use (&$counter) {
            $item = $event->getItem();
            if ($this->io->isVerbose()) {
                $route = $event->getUrl();
                $this->io->comment(sprintf('<info>%s</info> created (<info>%s</info>)', $item->getName(), $route));
            }

            $class = \get_class($item);
            if (!\array_key_exists($class, $counter['objects_created'])) {
                $counter['objects_created'][$class] = 1;
            } else {
                ++$counter['objects_created'][$class];
            }
        });

        $this->dispatcher->addListener(CrawlerEvents::ITEM_RESOURCE_UPDATED, function (CrawlerItemEvent $event) use (&$counter) {
            $item = $event->getItem();

            if ($this->io->isVerbose()) {
                $route = $event->getUrl();
                $this->io->comment(sprintf('<info>%s</info> updated (<info>%s</info>)', $item->getName(), $route));
            }

            $class = \get_class($item);
            $class = ltrim($class, 'Proxies\\__CG__\\');
            if (!\array_key_exists($class, $counter['objects_updated'])) {
                $counter['objects_updated'][$class] = 1;
            } else {
                ++$counter['objects_updated'][$class];
            }
        });
    }

    /**
     * @param array|string[][] $counter
     */
    private function writeStats(array $counter): void
    {
        $message = [sprintf('Crawled pages : %s', $counter['crawled_pages'])];

        foreach ($counter['objects_created'] as $class => $count) {
            $message[] = sprintf('Objects %s created : %s', $class, $count);
        }

        foreach ($counter['objects_updated'] as $class => $count) {
            $message[] = sprintf('Objects %s updated : %s', $class, $count);
        }

        $this->io->success($message);
    }

    /**
     * @param string $crawlerId
     * @param bool   $disableAutoDiscover
     * @param int    $listSize
     * @param int    $page
     * @param int    $pageMax
     *
     * @return CrawlerContext
     */
    protected function createContext($crawlerId, $disableAutoDiscover, $listSize, $page, $pageMax): CrawlerContext
    {
        $context = new CrawlerContext(
            $crawlerId,
            [
                'auto_discover' => !$disableAutoDiscover,
                'route_params' => ['size' => $listSize, 'page' => $page],
                'page_max' => $pageMax,
            ]
        );

        return $context;
    }
}
