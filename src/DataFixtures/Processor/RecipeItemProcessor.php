<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\DataFixtures\Processor;

use App\Entity\RecipeItemInterface;
use App\Entity\ResourceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Fidry\AliceDataFixtures\ProcessorInterface;

class RecipeItemProcessor implements ProcessorInterface
{
    /**
     * @var ArrayCollection
     */
    protected $resources;

    /**
     * @var ArrayCollection
     */
    protected $recipeItems;

    /**
     * RecipeItemProcessor constructor.
     */
    public function __construct()
    {
        $this->resources = new ArrayCollection();
        $this->recipeItems = new ArrayCollection();
    }

    public function preProcess(string $id, $object): void
    {
        if ($object instanceof ResourceInterface) {
            $this->resources->add($object);

            return;
        }

        if (!$object instanceof RecipeItemInterface) {
            return;
        }

        $resourcesCount = $this->resources->count();
        $try = 0;

        // Ensure unique constraint on recipe items (subresource, resourceCrafted)
        do {
            $invalidUniqueConstraint = $this->recipeItems->exists(function ($key, RecipeItemInterface $item) use ($object) {
                return $item->getSubresource() === $object->getSubresource()
                    && $item->getResourceCrafted() === $object->getResourceCrafted();
            });

            if ($invalidUniqueConstraint) {
                do {
                    $subresource = $this->resources->get(\random_int(0, $resourcesCount - 1));
                } while (!$subresource);

                $object->setSubresource($subresource);
                ++$try;
            }
        } while ($invalidUniqueConstraint && $try < $resourcesCount);

        $try = 0;

        // Ensure that subresource is not same as resource crafted
        do {
            $invalidSameResourceConstraint = $object->getSubresource() === $object->getResourceCrafted();

            if ($invalidSameResourceConstraint) {
                do {
                    $resourceCrafted = $this->resources->get(\random_int(0, $resourcesCount - 1));
                } while (!$resourceCrafted);

                $object->setResourceCrafted($resourceCrafted);
                ++$try;
            }
        } while ($invalidSameResourceConstraint && $try < $resourcesCount);

        $this->recipeItems->add($object);
    }

    /**
     * Processes an object after it is persisted to DB.
     *
     * @param string $id     Fixture ID
     * @param object $object
     */
    public function postProcess(string $id, $object): void
    {
    }
}
