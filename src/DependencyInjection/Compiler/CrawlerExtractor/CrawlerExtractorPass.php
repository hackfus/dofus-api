<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\DependencyInjection\Compiler\CrawlerExtractor;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

abstract class CrawlerExtractorPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Exception
     */
    public function process(ContainerBuilder $container): void
    {
        $extractorChainName = $this->getChainExtractorName();
        if (!$container->has($extractorChainName)) {
            return;
        }

        $tagName = sprintf('app.extractor.%s', $this->getTagSuffix());
        $definition = $container->findDefinition($extractorChainName);

        $definition->clearTag($tagName);

        $taggedServices = $container->findTaggedServiceIds($tagName);

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addExtractor', [new Reference($id)]);
        }
    }

    abstract protected function getChainExtractorName(): string;

    abstract protected function getTagSuffix(): string;
}
