<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\DependencyInjection\Compiler\CrawlerExtractor;

use App\Crawler\EquipmentCrawler;
use App\Crawler\Extractor\Resource\ResourceExtractorChain;

class EquipmentExtractorPass extends CrawlerExtractorPass
{
    protected function getChainExtractorName(): string
    {
        return ResourceExtractorChain::class;
    }

    protected function getTagSuffix(): string
    {
        return EquipmentCrawler::getCrawlerAlias();
    }
}
