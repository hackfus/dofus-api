<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\DependencyInjection\Compiler\CrawlerExtractor;

use App\Crawler\Extractor\Resource\ResourceExtractorChain;
use App\Crawler\WeaponCrawler;

class WeaponExtractorPass extends CrawlerExtractorPass
{
    protected function getChainExtractorName(): string
    {
        return ResourceExtractorChain::class;
    }

    protected function getTagSuffix(): string
    {
        return WeaponCrawler::getCrawlerAlias();
    }
}
