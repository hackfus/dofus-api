<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\DependencyInjection\Compiler;

use App\Crawler\CrawlerRegistry;
use App\Crawler\EquipmentCrawler;
use App\Crawler\ProfessionCrawler;
use App\Crawler\ResourceCrawler;
use App\Crawler\WeaponCrawler;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CrawlerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(CrawlerRegistry::class)) {
            return;
        }

        $crawlerRegistry = $container->findDefinition(CrawlerRegistry::class);
        $crawlerRegistry->addMethodCall('addCrawler', [new Reference(EquipmentCrawler::class), EquipmentCrawler::getCrawlerAlias()]);
        $crawlerRegistry->addMethodCall('addCrawler', [new Reference(WeaponCrawler::class), WeaponCrawler::getCrawlerAlias()]);
        $crawlerRegistry->addMethodCall('addCrawler', [new Reference(ResourceCrawler::class), ResourceCrawler::getCrawlerAlias()]);
        $crawlerRegistry->addMethodCall('addCrawler', [new Reference(ProfessionCrawler::class), ProfessionCrawler::getCrawlerAlias()]);
    }
}
