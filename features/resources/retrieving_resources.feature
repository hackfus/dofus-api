@createSchema
@jsonld
Feature: Retrieving resources
  In order to retrieve resources
  As an API consumer
  I want to be able to retrieve them

  Background:
    Given there is a resource named "Hormone de Toukancre"
    And there is a resource named "Hormone de Chacha Tigré"
    And the market "FooShop" has a category named "Fooz" and a resource named "Pierre d'Air"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Retrieving resources
    When I send a "GET" request to "/resources"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/list.json"
    And the JSON node "hydra:member" should have 3 elements
    And the JSON node "hydra:totalItems" should be equal to the number 3

  Scenario: Retrieving resources filtered by name
    When I send a "GET" request to "/resources?name=Pierre"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/list.json"
    And the JSON node "hydra:member" should have 1 element
    And the JSON node "hydra:totalItems" should be equal to the number 1

  Scenario: Retrieving resources ordered by name ascending
    When I send a "GET" request to "/resources?order[name]=asc"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/list.json"
    And the JSON node "hydra:member" should have 3 element
    And the JSON node "hydra:totalItems" should be equal to the number 3
    And the JSON node "hydra:member[0].name" should be equal to the string "Hormone de Chacha Tigré"

  Scenario: Retrieving resources ordered by name descending
    When I send a "GET" request to "/resources?order[name]=desc"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/list.json"
    And the JSON node "hydra:member" should have 3 element
    And the JSON node "hydra:totalItems" should be equal to the number 3
    And the JSON node "hydra:member[0].name" should be equal to the string "Pierre d'Air"
