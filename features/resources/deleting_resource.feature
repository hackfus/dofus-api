@createSchema
@jsonld
@admin
@login
@logout
Feature: Deleting resource
  In order to remove resources
  As an Administrator
  I want to be able to delete a resource

  Background:
    Given there is a resource named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Deleting resource
    When I send a "DELETE" request to "/resources/1"
    Then the response status code should be 204
    And the response should be empty
