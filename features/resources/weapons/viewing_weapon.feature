@createSchema
@jsonld
Feature: Viewing a weapon details
  In order to see weapon detailed information
  As an API consumer
  I want to be able to view a single weapon

  Background:
    Given there is a weapon named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Viewing a detailed weapon
    When I send a "GET" request to "/weapons/1"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/weapons/read.json"
    And the JSON node "name" should be equal to the string "Foo"
