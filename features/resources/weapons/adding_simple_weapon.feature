@createSchema
@jsonld
@admin
@login
@logout
Feature: Adding weapons
  In order to create weapons
  As an Administrator
  I want to be able to create them

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple weapon with name
    When I send a "POST" request to "/weapons" with body:
    """
    {
      "name": "Hormone de Chacha Tigré"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/weapons/create.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"

  Scenario: Creating a simple weapon with name, level and description
    When I send a "POST" request to "/weapons" with body:
    """
    {
      "name": "Hormone de Chacha Tigré",
      "level": 10,
      "description": "Cette hormone de croissance déshydratée peut être utilisée dans la confection d'une puissante potion pour améliorer un familier.",
      "actionPoints": 5,
      "minRange": 2,
      "maxRange": 3,
      "criticalRate": 25,
      "criticalHitDamages": 8
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/weapons/create.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"
    And the JSON node "level" should be equal to the number 10
    And the JSON node "description" should be equal to the string "Cette hormone de croissance déshydratée peut être utilisée dans la confection d'une puissante potion pour améliorer un familier."
    And the JSON node "actionPoints" should be equal to the number 5
    And the JSON node "minRange" should be equal to the number 2
    And the JSON node "maxRange" should be equal to the number 3
    And the JSON node "criticalRate" should be equal to the number 25
    And the JSON node "criticalHitDamages" should be equal to the number 8
