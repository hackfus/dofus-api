@createSchema
@jsonld
@admin
@login
@logout
Feature: Deleting weapon
  In order to remove weapons
  As an Administrator
  I want to be able to delete a weapon

  Background:
    Given there is a weapon named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Deleting weapon
    When I send a "DELETE" request to "/weapons/1"
    Then the response status code should be 204
    And the response should be empty
