@createSchema
@jsonld
@admin
@login
@logout
Feature: Editing weapons
  In order to update a weapon
  As an Administrator
  I want to be able to edite its content

  Background:
    Given there is a weapon named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Renaming a simple weapon
    When I send a "PUT" request to "/weapons/1" with body:
    """
    {
      "name": "Hormone de Chacha Tigré"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/weapons/read.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"

  Scenario: Changing a simple weapon fields
    When I send a "PUT" request to "/weapons/1" with body:
    """
    {
      "name": "Hormone de Chacha Tigré",
      "level": 65,
      "description": "Dummy description",
      "tradable": false,
      "droppable": false,
      "secret": true,
      "fetched": true,
      "enabled": false,
      "actionPoints": 5,
      "minRange": 2,
      "maxRange": 3,
      "criticalRate": 25,
      "criticalHitDamages": 8
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/weapons/read.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"
    And the JSON node "level" should be equal to the number 65
    And the JSON node "description" should be equal to the string "Dummy description"
    And the JSON node "tradable" should be false
    And the JSON node "droppable" should be false
    And the JSON node "secret" should be true
    And the JSON node "fetched" should be true
    And the JSON node "enabled" should be false
    And the JSON node "actionPoints" should be equal to the number 5
    And the JSON node "minRange" should be equal to the number 2
    And the JSON node "maxRange" should be equal to the number 3
    And the JSON node "criticalRate" should be equal to the number 25
    And the JSON node "criticalHitDamages" should be equal to the number 8

  Scenario: Adding a market category to a weapon
    Given the market "FooShop" has a category named "Fooz"
    When I send a "PUT" request to "/weapons/1" with body:
    """
    {
      "marketCategory": "/market-categories/1"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/weapons/read.json"
    And the JSON node "marketCategory" should be equal to the string "/market-categories/1"
    And the JSON node "categoryName" should be equal to the string "Fooz"
