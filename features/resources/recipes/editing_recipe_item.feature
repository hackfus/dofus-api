@createSchema
@jsonld
@admin
@login
@logout
Feature: Editing recipe item
  In order to update a recipe item
  As an Administrator
  I want to be able to edite its content

  Background:
    Given there is a resource named "Foo" with 1 recipe items
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Update recipe item quantity
    When I send a "PUT" request to "/recipe-items/1" with body:
    """
    {
      "quantity": 80
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/read.json"
    And the JSON node "quantity" should be equal to the number 80
    And the JSON node "resourceCrafted.@id" should be equal to the string "/resources/1"
    And the JSON node "subresource.@id" should be equal to the string "/resources/2"

  Scenario: Update recipe item subresource
    When I send a "PUT" request to "/recipe-items/1" with body:
    """
    {
      "subresource": {
        "name": "Bar"
      }
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/read.json"
    And the JSON node "resourceCrafted.@id" should be equal to the string "/resources/1"
    And the JSON node "subresource.@id" should be equal to the string "/resources/3"

  Scenario: Update recipe item resourceCrafted
    When I send a "PUT" request to "/recipe-items/1" with body:
    """
    {
      "resourceCrafted": {
        "name": "Bar"
      }
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/read.json"
    And the JSON node "resourceCrafted.@id" should be equal to the string "/resources/3"
    And the JSON node "subresource.@id" should be equal to the string "/resources/2"
