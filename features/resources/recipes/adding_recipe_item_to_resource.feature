@createSchema
@jsonld
@admin
@login
@logout
Feature: Adding recipe item to resource
  In order to add recipe item to resource
  As an Administrator
  I want to be able to fill a resource with recipe items

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple recipe with an unknwon resource
    And I send a "POST" request to "/recipe-items" with body:
    """
    {
      "quantity": 15,
      "resourceCrafted": {
        "name": "Foo"
      },
      "subresource": {
        "name": "Bar"
      }
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/recipes/create.json"
    And the JSON node "quantity" should be equal to the number 15
    And the JSON node "resourceCrafted.name" should be equal to the string "Foo"
    And the JSON node "subresource.name" should be equal to the string "Bar"

  Scenario: Adding a recipe item to an existing resource and an unknown subresource
    Given there is a resource named "Foo"
    And I send a "POST" request to "/recipe-items" with body:
    """
    {
      "quantity": 15,
      "resourceCrafted": "/resources/1",
      "subresource": {
        "name": "Bar"
      }
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/recipes/create.json"
    And the JSON node "quantity" should be equal to the number 15
    And the JSON node "resourceCrafted.name" should be equal to the string "Foo"
    And the JSON node "subresource.name" should be equal to the string "Bar"

  Scenario: Adding a recipe item to existing resources
    Given there is a resource named "Foo"
    And there is a resource named "Bar"
    And I send a "POST" request to "/recipe-items" with body:
    """
    {
      "quantity": 15,
      "resourceCrafted": "/resources/1",
      "subresource": "/resources/2"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/recipes/create.json"
    And the JSON node "quantity" should be equal to the number 15
    And the JSON node "resourceCrafted.name" should be equal to the string "Foo"
    And the JSON node "subresource.name" should be equal to the string "Bar"
