@createSchema
@jsonld
@admin
@login
@logout
Feature: Recipe item validation
  In order to avoid making mistakes when managing a recipe items
  As an Administrator
  I want to be prevented from adding it without required fields

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Adding a new recipe with empty body
    When I send a "POST" request to "/recipe-items" with body:
    """
    {}
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "resourceCrafted"
    And the JSON node "violations[0].message" should be equal to the string "This value should not be null."
    And the JSON node "violations[1].propertyPath" should be equal to the string "subresource"
    And the JSON node "violations[1].message" should be equal to the string "This value should not be null."

  Scenario: Adding a new recipe without negative quantity
    Given there is a resource named "Foo"
    Given there is a resource named "Bar"
    When I send a "POST" request to "/recipe-items" with body:
    """
    {
      "quantity": -15,
      "resourceCrafted": "/resources/1",
      "subresource": "/resources/2"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "quantity"
    And the JSON node "violations[0].message" should be equal to the string "This value should be greater than or equal to 1."

  Scenario: Adding a new recipe with a duplicate resource name
    Given there is a resource named "Foo"
    When I send a "POST" request to "/recipe-items" with body:
    """
    {
      "quantity": 15,
      "resourceCrafted": "/resources/1",
      "subresource": {
        "name": "Foo"
      }
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "subresource.name"
    And the JSON node "violations[0].message" should be equal to the string "This value is already used."
