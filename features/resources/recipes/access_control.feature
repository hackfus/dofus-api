@createSchema
@jsonld
@accessControl
Feature: Perform write operations on weapons
  In order to manage weapons
  As an API consumer
  I'm not able to manage them

  Background:
    Given there is a resource named "Foo" with 1 recipe items
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple recipe with an unknwon resource
    And I send a "POST" request to "/recipe-items" with body:
    """
    {
      "quantity": 15,
      "resourceCrafted": {
        "name": "Foo"
      },
      "subresource": {
        "name": "Bar"
      }
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  Scenario: Deleting recipe item
    When I send a "DELETE" request to "/recipe-items/1"
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  Scenario: Update recipe item quantity
    When I send a "PUT" request to "/recipe-items/1" with body:
    """
    {
      "quantity": 80
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  @loginDemo
  @logout
  Scenario: Creating a simple recipe with an unknwon resource
    And I send a "POST" request to "/recipe-items" with body:
    """
    {
      "quantity": 15,
      "resourceCrafted": {
        "name": "Foo"
      },
      "subresource": {
        "name": "Bar"
      }
    }
    """
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."

  @loginDemo
  @logout
  Scenario: Deleting recipe item
    When I send a "DELETE" request to "/recipe-items/1"
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."

  @loginDemo
  @logout
  Scenario: Update recipe item quantity
    When I send a "PUT" request to "/recipe-items/1" with body:
    """
    {
      "quantity": 80
    }
    """
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."
