@createSchema
@jsonld
@admin
@login
@logout
Feature: Deleting recipe item
  In order to remove recipe item
  As an Administrator
  I want to be able to delete a recipe item

  Background:
    Given there is a resource named "Foo" with 1 recipe items
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Deleting recipe item
    When I send a "DELETE" request to "/recipe-items/1"
    Then the response status code should be 204
    And the response should be empty
