@createSchema
@jsonld
@admin
@login
@logout
Feature: Adding complexe resources
  In order to create complexe resources
  As an Administrator
  I want to be able to create them

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a complexe resource with recipe
    Given there is a resource named "Bar"
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "Hormone de Chacha Tigré",
      "recipeItems": [
        {
          "quantity": 15,
          "subresource": "/resources/1"
        }
      ]
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/create.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"
    And the JSON node "recipeItems[0]" should be equal to the string "/recipe-items/1"
