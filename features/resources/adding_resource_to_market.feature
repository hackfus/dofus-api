@createSchema
@jsonld
@admin
@login
@logout
Feature: Adding resources to market
  In order to add resources to market
  As an Administrator
  I want to be able to fill the market with resources

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a resource with name and affected to a market category
    Given the market "FooShop" has a category named "Fooz"
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "Hormone de Chacha Tigré",
      "marketCategory": "/market-categories/1"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/create.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"
    And the JSON node "marketCategory" should be equal to the string "/market-categories/1"
    And the JSON node "categoryName" should be equal to the string "Fooz"
