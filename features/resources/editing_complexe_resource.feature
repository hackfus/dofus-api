@createSchema
@jsonld
@admin
@login
@logout
Feature: Editing resources
  In order to update a resource
  As an Administrator
  I want to be able to edite its content

  Background:
    Given there is a resource named "Foo" with 3 recipe items
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Editing a complexe resource with recipe
    When I send a "PUT" request to "/resources/1" with body:
    """
    {
      "name": "Hormone de Chacha Tigré",
      "recipeItems": [
        {
          "quantity": 15,
          "subresource": {
            "name": "Bar"
          }
        }
      ]
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/create.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"
    # And the JSON node "recipeItems[0].subresource.name" should be equal to the string "Bar"
    And the JSON node "recipeItems" should have 1 element

  Scenario: Editing recipe item quantity from resource
    When I send a "PUT" request to "/resources/1" with body:
    """
    {
      "name": "Hormone de Chacha Tigré",
      "recipeItems": [
        {
          "@id": "recipe-items/1",
          "quantity": 40,
          "subresource": "/resources/2"
        },
        {
          "@id": "recipe-items/2",
          "quantity": 50,
          "subresource": "/resources/3"
        },
        {
          "@id": "recipe-items/3",
          "quantity": 60,
          "subresource": "/resources/4"
        }
      ]
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/create.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"
    And the JSON node "recipeItems" should have 3 elements
