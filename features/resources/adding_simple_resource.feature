@createSchema
@jsonld
@admin
@login
@logout
Feature: Adding resources
  In order to create resources
  As an Administrator
  I want to be able to create them

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple resource with name
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "Hormone de Chacha Tigré"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/create.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"

  Scenario: Creating a simple resource with name, level and description
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "Hormone de Chacha Tigré",
      "level": 10,
      "description": "Cette hormone de croissance déshydratée peut être utilisée dans la confection d'une puissante potion pour améliorer un familier."
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/create.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"
    And the JSON node "level" should be equal to the number 10
    And the JSON node "description" should be equal to the string "Cette hormone de croissance déshydratée peut être utilisée dans la confection d'une puissante potion pour améliorer un familier."
