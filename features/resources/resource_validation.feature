@createSchema
@jsonld
@admin
@login
@logout
Feature: Resources validation
  In order to avoid making mistakes when managing a resource
  As an Administrator
  I want to be prevented from adding it without required fields

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Adding a new simple resource without specifying its name
    When I send a "POST" request to "/resources" with body:
    """
    {}
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "name"
    And the JSON node "violations[0].message" should be equal to the string "This value should not be blank."

  Scenario: Adding a new simple resource with a short name
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "x"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "name"
    And the JSON node "violations[0].message" should be equal to the string "This value is too short. It should have 2 characters or more."

  Scenario: Adding a new simple resource with duplicated name
    Given there is a resource named "Foo"
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "Foo"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "name"
    And the JSON node "violations[0].message" should be equal to the string "This value is already used."

  Scenario: Adding a new simple resource with a 0 level value
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "Foo",
      "level": 0
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "level"
    And the JSON node "violations[0].message" should be equal to the string 'This value should be "1" or more.'

  Scenario: Adding a new simple resource with a negative level value
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "Foo",
      "level": -50
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "level"
    And the JSON node "violations[0].message" should be equal to the string 'This value should be "1" or more.'

  Scenario: Adding a new simple resource with a big level value
    When I send a "POST" request to "/resources" with body:
    """
    {
      "name": "Foo",
      "level": 100000
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "level"
    And the JSON node "violations[0].message" should be equal to the string 'This value should be "1000" or less.'
