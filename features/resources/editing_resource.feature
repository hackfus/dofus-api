@createSchema
@jsonld
@admin
@login
@logout
Feature: Editing resources
  In order to update a resource
  As an Administrator
  I want to be able to edite its content

  Background:
    Given there is a resource named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Renaming a simple resource
    When I send a "PUT" request to "/resources/1" with body:
    """
    {
      "name": "Hormone de Chacha Tigré"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/read.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"

  Scenario: Changing a simple resource fields
    When I send a "PUT" request to "/resources/1" with body:
    """
    {
      "name": "Hormone de Chacha Tigré",
      "level": 65,
      "description": "Dummy description",
      "tradable": false,
      "droppable": false,
      "secret": true,
      "fetched": true,
      "enabled": false
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/read.json"
    And the JSON node "name" should be equal to the string "Hormone de Chacha Tigré"
    And the JSON node "level" should be equal to the number 65
    And the JSON node "description" should be equal to the string "Dummy description"
    And the JSON node "tradable" should be false
    And the JSON node "droppable" should be false
    And the JSON node "secret" should be true
    And the JSON node "fetched" should be true
    And the JSON node "enabled" should be false

  Scenario: Adding a market category to a resource
    Given the market "FooShop" has a category named "Fooz"
    When I send a "PUT" request to "/resources/1" with body:
    """
    {
      "marketCategory": "/market-categories/1"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/read.json"
    And the JSON node "marketCategory" should be equal to the string "/market-categories/1"
    And the JSON node "categoryName" should be equal to the string "Fooz"
