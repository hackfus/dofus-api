@createSchema
@jsonld
@admin
@login
@logout
Feature: Deleting equipment
  In order to remove equipments
  As an Administrator
  I want to be able to delete a equipment

  Background:
    Given there is a equipment named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Deleting equipment
    When I send a "DELETE" request to "/equipments/1"
    Then the response status code should be 204
    And the response should be empty
