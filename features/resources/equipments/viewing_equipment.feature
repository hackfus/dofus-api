@createSchema
@jsonld
Feature: Viewing a equipment details
  In order to see equipment detailed information
  As an API consumer
  I want to be able to view a single equipment

  Background:
    Given there is a equipment named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Viewing a detailed equipment
    When I send a "GET" request to "/equipments/1"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/equipments/read.json"
    And the JSON node "name" should be equal to the string "Foo"
