@createSchema
@jsonld
@admin
@login
@logout
Feature: Adding professions
  In order to create professions
  As an Administrator
  I want to be able to create them

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple profession with name
    When I send a "POST" request to "/professions" with body:
    """
    {
      "name": "Bûcheron"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/create.json"
    And the JSON node "name" should be equal to the string "Bûcheron"

  Scenario: Creating a simple profession with name and description
    When I send a "POST" request to "/professions" with body:
    """
    {
      "name": "Bûcheron",
      "description": "Le bûcheron coupe les arbres. Il peut également fabriquer des planches et des substrats qui sont utilisés pour confectionner d'autres objets."
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/create.json"
    And the JSON node "name" should be equal to the string "Bûcheron"
    And the JSON node "description" should be equal to the string "Le bûcheron coupe les arbres. Il peut également fabriquer des planches et des substrats qui sont utilisés pour confectionner d'autres objets."
