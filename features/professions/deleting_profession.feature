@createSchema
@jsonld
@admin
@login
@logout
Feature: Deleting profession
  In order to remove professions
  As an Administrator
  I want to be able to delete a profession

  Background:
    Given there is a profession named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Deleting profession
    When I send a "DELETE" request to "/professions/1"
    Then the response status code should be 204
    And the response should be empty
