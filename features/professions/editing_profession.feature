@createSchema
@jsonld
@admin
@login
@logout
Feature: Editing professions
  In order to update a profession
  As an Administrator
  I want to be able to edite its content

  Background:
    Given there is a profession named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Renaming a simple profession
    When I send a "PUT" request to "/professions/1" with body:
    """
    {
      "name": "Alchimiste"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/read.json"
    And the JSON node "name" should be equal to the string "Alchimiste"

  Scenario: Changing a simple profession fields
    When I send a "PUT" request to "/professions/1" with body:
    """
    {
      "name": "Alchimiste",
      "description": "Dummy description",
      "enabled": false
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/read.json"
    And the JSON node "name" should be equal to the string "Alchimiste"
    And the JSON node "description" should be equal to the string "Dummy description"
    And the JSON node "enabled" should be false
