@createSchema
@jsonld
Feature: Viewing a profession details
  In order to see profession detailed information
  As an API consumer
  I want to be able to view a single profession

  Background:
    Given there is a profession named "Foo"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Viewing a detailed profession
    When I send a "GET" request to "/professions/1"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/read.json"
    And the JSON node "name" should be equal to the string "Foo"
