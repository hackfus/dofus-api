@createSchema
@jsonld
Feature: Retrieving professions
  In order to retrieve professions
  As an API consumer
  I want to be able to retrieve them

  Background:
    Given there is 15 professions
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Retrieving professions
    When I send a "GET" request to "/professions"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/list.json"
    And the JSON node "hydra:member" should have 10 elements
    And the JSON node "hydra:totalItems" should be equal to the number 15

  Scenario: Retrieving professions filtered by name
    When I send a "GET" request to "/professions?name=Dummy-5"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/list.json"
    And the JSON node "hydra:member" should have 1 element
    And the JSON node "hydra:totalItems" should be equal to the number 1

  Scenario: Retrieving professions ordered by name ascending
    When I send a "GET" request to "/professions?order[name]=asc"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/list.json"
    And the JSON node "hydra:member" should have 10 element
    And the JSON node "hydra:totalItems" should be equal to the number 15
    And the JSON node "hydra:member[0].name" should be equal to the string "Dummy-0"

  Scenario: Retrieving professions ordered by name descending
    When I send a "GET" request to "/professions?order[name]=desc"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/professions/list.json"
    And the JSON node "hydra:member" should have 10 element
    And the JSON node "hydra:totalItems" should be equal to the number 15
    And the JSON node "hydra:member[0].name" should be equal to the string "Dummy-9"
