@createSchema
@jsonld
@accessControl
Feature: Perform write operations on professions
  In order to manage professions
  As an API consumer
  I'm not able to manage them

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple profession with name
    When I send a "POST" request to "/professions" with body:
    """
    {
      "name": "Bûcheron"
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  Scenario: Creating a simple profession with name and description
    When I send a "POST" request to "/professions" with body:
    """
    {
      "name": "Bûcheron",
      "description": "Le bûcheron coupe les arbres. Il peut également fabriquer des planches et des substrats qui sont utilisés pour confectionner d'autres objets."
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  @loginDemo
  @logout
  Scenario: Creating a simple profession with name
    When I send a "POST" request to "/professions" with body:
    """
    {
      "name": "Bûcheron"
    }
    """
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."

  @loginDemo
  @logout
  Scenario: Creating a simple profession with name and description
    When I send a "POST" request to "/professions" with body:
    """
    {
      "name": "Bûcheron",
      "description": "Le bûcheron coupe les arbres. Il peut également fabriquer des planches et des substrats qui sont utilisés pour confectionner d'autres objets."
    }
    """
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."
