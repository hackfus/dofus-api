@createSchema
@jsonld
Feature: Retrieving markets
  In order to retrieve markets
  As an API consumer
  I want to be able to retrieve them

  Background:
    Given there is a market named "Hôtel de vente des pêcheurs"
    And there is a market named "Hôtel de vente des mineurs"
    And there is a market named "Hôtel de vente des alchimistes"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Retrieving markets
    When I send a "GET" request to "/markets"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/markets/list.json"
    And the JSON node "hydra:member" should have 3 elements
    And the JSON node "hydra:totalItems" should be equal to the number 3

  Scenario: Retrieving markets filtered by name
    When I send a "GET" request to "/markets?name=pêcheurs"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/markets/list.json"
    And the JSON node "hydra:member" should have 1 element
    And the JSON node "hydra:totalItems" should be equal to the number 1

  Scenario: Retrieving markets ordered by name ascending
    When I send a "GET" request to "/markets?order[name]=asc"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/markets/list.json"
    And the JSON node "hydra:member" should have 3 element
    And the JSON node "hydra:totalItems" should be equal to the number 3
    And the JSON node "hydra:member[0].name" should be equal to the string "Hôtel de vente des alchimistes"

  Scenario: Retrieving markets ordered by name descending
    When I send a "GET" request to "/markets?order[name]=desc"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/markets/list.json"
    And the JSON node "hydra:member" should have 3 element
    And the JSON node "hydra:totalItems" should be equal to the number 3
    And the JSON node "hydra:member[0].name" should be equal to the string "Hôtel de vente des pêcheurs"
