@createSchema
@jsonld
@admin
@login
@logout
Feature: Editing market
  In order to update a market
  As an Administrator
  I want to be able to edite its content

  Background:
    Given there is a market named "FooShop"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Renaming a simple market
    When I send a "PUT" request to "/markets/1" with body:
    """
    {
      "name": "BarShop"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/resources/read.json"
    And the JSON node "name" should be equal to the string "BarShop"
