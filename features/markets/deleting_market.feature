@createSchema
@jsonld
@admin
@login
@logout
Feature: Deleting market
  In order to remove markets
  As an Administrator
  I want to be able to delete a market

  Background:
    Given there is a market named "FooShop"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Deleting market
    When I send a "DELETE" request to "/markets/1"
    Then the response status code should be 204
    And the response should be empty
