@createSchema
@jsonld
@admin
@login
@logout
Feature: Adding market category
  In order to create market category
  As an Administrator
  I want to be able to create them

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple market category with name
    When I send a "POST" request to "/market-categories" with body:
    """
    {
      "name": "Fooz"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/markets/create.json"
    And the JSON node "name" should be equal to the string "Fooz"

  Scenario: Creating a simple market category associated to a market
    Given there is a market named "FooShop"
    When I send a "POST" request to "/market-categories" with body:
    """
    {
      "name": "Fooz",
      "market": "/markets/1"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/markets/create.json"
    And the JSON node "name" should be equal to the string "Fooz"
    And the JSON node "market" should be equal to the string "/markets/1"

  Scenario: Creating a market category associated to a market and a resource
    Given there is a market named "FooShop"
    And there is a resource named "Foo"
    When I send a "POST" request to "/market-categories" with body:
    """
    {
      "name": "Fooz",
      "market": "/markets/1",
      "resources": [
        "/resources/1"
      ]
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/markets/create.json"
    And the JSON node "name" should be equal to the string "Fooz"
    And the JSON node "market" should be equal to the string "/markets/1"
    And the JSON node "resources[0]" should be equal to the string "/resources/1"
