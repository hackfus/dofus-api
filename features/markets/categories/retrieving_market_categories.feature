@createSchema
@jsonld
Feature: Retrieving market categories
  In order to retrieve market categories
  As an API consumer
  I want to be able to retrieve them

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Retrieving market categories
    Given the market "FooShop" has a category named "Fooz"
    When I send a "GET" request to "/market-categories/1"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/market_categories/list.json"
    And the JSON node "name" should be equal to the string "Fooz"
    And the JSON node "market.name" should be equal to the string "FooShop"

  Scenario: Retrieving market categories from market subresource
    Given the market "FooShop" has a category named "Fooz"
    When I send a "GET" request to "/markets/1/categories"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/market_categories/list.json"
    And the JSON node "hydra:member[0].name" should be equal to the string "Fooz"
    And the JSON node "hydra:member[0].market" should be equal to the string "/markets/1"
