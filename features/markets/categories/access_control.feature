@createSchema
@jsonld
@accessControl
Feature: Perform write operations on market categories
  In order to manage market categories
  As an API consumer
  I'm not able to manage them

  Background:
    Given the market "FooShop" has a category named "Fooz"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple market category with name
    When I send a "POST" request to "/market-categories" with body:
    """
    {
      "name": "Fool"
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  Scenario: Deleting market
    When I send a "DELETE" request to "/market-categories/1"
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  Scenario: Renaming a simple market
    When I send a "PUT" request to "/market-categories/1" with body:
    """
    {
      "name": "Baz"
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  @loginDemo
  @logout
  Scenario: Creating a simple market category with name
    When I send a "POST" request to "/market-categories" with body:
    """
    {
      "name": "Fool"
    }
    """
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."

  @loginDemo
  @logout
  Scenario: Deleting market
    When I send a "DELETE" request to "/market-categories/1"
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."

  @loginDemo
  @logout
  Scenario: Renaming a simple market
    When I send a "PUT" request to "/market-categories/1" with body:
    """
    {
      "name": "Baz"
    }
    """
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."
