@createSchema
@jsonld
Feature: Viewing a market category details
  In order to see market categories detailed information
  As an API consumer
  I want to be able to view a single market category

  Background:
    Given the market "FooShop" has a category named "Fooz"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Viewing a detailed market category
    When I send a "GET" request to "/market-categories/1"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/market_categories/read.json"
    And the JSON node "name" should be equal to the string "Fooz"
    And the JSON node "market.name" should be equal to the string "FooShop"
