@createSchema
@jsonld
@admin
@login
@logout
Feature: Editing market category
  In order to update a market category
  As an Administrator
  I want to be able to edite its content

  Background:
    Given the market "FooShop" has a category named "Fooz"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Renaming a simple market
    When I send a "PUT" request to "/market-categories/1" with body:
    """
    {
      "name": "Baz"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/market_categories/read.json"
    And the JSON node "name" should be equal to the string "Baz"

  Scenario: Adding a resource to a market category
    Given the market "FooShop" has a category named "Fooz"
    Given there is a resource named "Foo"
    When I send a "PUT" request to "/market-categories/1" with body:
    """
    {
      "resources": [
        "/resources/1"
      ]
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/market_categories/read.json"
    And the JSON node "resources[0].@id" should be equal to the string "/resources/1"
