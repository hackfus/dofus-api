@createSchema
@jsonld
@admin
@login
@logout
Feature: Market categories validation
  In order to avoid making mistakes when managing a market category
  As an Administrator
  I want to be prevented from adding it without required fields

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Adding a new market category without specifying its name
    When I send a "POST" request to "/market-categories" with body:
    """
    {}
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "name"
    And the JSON node "violations[0].message" should be equal to the string "This value should not be blank."

  Scenario: Adding a new market category with a short name
    When I send a "POST" request to "/market-categories" with body:
    """
    {
      "name": "x"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "name"
    And the JSON node "violations[0].message" should be equal to the string "This value is too short. It should have 2 characters or more."

  Scenario: Adding a new market with duplicated name
    Given the market "FooShop" has a category named "Fooz"
    When I send a "POST" request to "/market-categories" with body:
    """
    {
      "name": "Fooz"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/errors/constraint_violation.json"
    And the JSON node "violations[0].propertyPath" should be equal to the string "name"
    And the JSON node "violations[0].message" should be equal to the string "This value is already used."
