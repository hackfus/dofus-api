@createSchema
@jsonld
@admin
@login
@logout
Feature: Deleting market category
  In order to remove market categories
  As an Administrator
  I want to be able to delete a market category

  Background:
    Given the market "FooShop" has a category named "Fooz"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Deleting market
    When I send a "DELETE" request to "/market-categories/1"
    Then the response status code should be 204
    And the response should be empty
