@createSchema
@jsonld
@accessControl
Feature: Perform write operations on markets
  In order to manage markets
  As an API consumer
  I'm not able to manage them

  Background:
    Given there is a market named "FooShop"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple market with name
    When I send a "POST" request to "/markets" with body:
    """
    {
      "name": "Hôtel de vente des pêcheurs"
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  Scenario: Deleting market
    When I send a "DELETE" request to "/markets/1"
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  Scenario: Renaming a simple market
    When I send a "PUT" request to "/markets/1" with body:
    """
    {
      "name": "BarShop"
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"
    And the JSON node "message" should be equal to the string "JWT Token not found"

  @loginDemo
  @logout
  Scenario: Creating a simple market with name
    When I send a "POST" request to "/markets" with body:
    """
    {
      "name": "Hôtel de vente des pêcheurs"
    }
    """
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."

  @loginDemo
  @logout
  Scenario: Deleting market
    When I send a "DELETE" request to "/markets/1"
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."

  @loginDemo
  @logout
  Scenario: Renaming a simple market
    When I send a "PUT" request to "/markets/1" with body:
    """
    {
      "name": "BarShop"
    }
    """
    Then the response status code should be 403
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/problem+json; charset=utf-8"
    And the JSON node "detail" should be equal to the string "Access Denied."
