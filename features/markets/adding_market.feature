@createSchema
@jsonld
@admin
@login
@logout
Feature: Adding markets
  In order to create markets
  As an Administrator
  I want to be able to create them

  Background:
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"

  Scenario: Creating a simple market with name
    When I send a "POST" request to "/markets" with body:
    """
    {
      "name": "Hôtel de vente des pêcheurs"
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/ld+json; charset=utf-8"
    # And the JSON should be valid according to the schema "features/schema/jsonld/markets/create.json"
    And the JSON node "name" should be equal to the string "Hôtel de vente des pêcheurs"
