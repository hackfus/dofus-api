<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Behat\Context;

use App\Entity\Market;
use App\Entity\MarketCategory;
use App\Entity\Resource;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class MarketContext implements Context, SnippetAcceptingContext
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * Initializes context.
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->manager = $doctrine->getManager();
    }

    /**
     * @Given there is a market named :name
     *
     * @param string $name
     */
    public function thereIsAMarketNamed(string $name)
    {
        $market = new Market();
        $market->setName($name);

        try {
            $this->manager->persist($market);
            $this->manager->flush();
        } catch (UniqueConstraintViolationException $e) {
        }
    }

    /**
     * @Given the market :marketName has a category named :categoryName
     *
     * @param string $marketName
     * @param string $categoryName
     */
    public function thereIsAMarketCategoryNamed(string $marketName, string $categoryName)
    {
        $market = $this->createMarket($marketName);

        $category = $this->createMarketCategory($categoryName);

        $market->addCategory($category);

        try {
            $this->manager->persist($market);
            $this->manager->flush();
        } catch (UniqueConstraintViolationException $e) {
        }
    }

    /**
     * @Given the market :marketName has a category named :categoryName and a resource named :resourceName
     *
     * @param string $marketName
     * @param string $categoryName
     * @param string $resourceName
     */
    public function thereIsAMarketCategoryNamedAndAResourceName(string $marketName, string $categoryName, string $resourceName)
    {
        $market = $this->createMarket($marketName);

        $category = $this->createMarketCategory($categoryName);

        $resource = $this->createResource($resourceName);

        $category->addResource($resource);

        $market->addCategory($category);

        try {
            $this->manager->persist($market);
            $this->manager->flush();
        } catch (UniqueConstraintViolationException $e) {
        }
    }

    /**
     * @param string $name
     *
     * @return Market
     */
    protected function createMarket(string $name): Market
    {
        $market = $this->manager->getRepository(Market::class)->findOneBy(['name' => $name]);

        if (!$market) {
            $market = new Market();
            $market->setName($name);
        }

        return $market;
    }

    /**
     * @param string $name
     *
     * @return MarketCategory
     */
    protected function createMarketCategory(string $name): MarketCategory
    {
        $category = $this->manager->getRepository(MarketCategory::class)->findOneBy(['name' => $name]);

        if (!$category) {
            $category = new MarketCategory();
            $category->setName($name);
        }

        return $category;
    }

    /**
     * @param string $name
     *
     * @return \App\Entity\Resource
     */
    protected function createResource(string $name): \App\Entity\Resource
    {
        $resource = $this->manager->getRepository(Resource::class)->findOneBy(['name' => $name]);

        if (!$resource) {
            $resource = new Resource();
            $resource->setName($name);
        }

        return $resource;
    }
}
