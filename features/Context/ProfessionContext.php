<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Behat\Context;

use App\Entity\Profession;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class ProfessionContext implements Context, SnippetAcceptingContext
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * Initializes context.
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->manager = $doctrine->getManager();
    }

    /**
     * @Given there is a profession named :name
     *
     * @param string $name
     */
    public function thereIsAProfessionNamed(string $name)
    {
        $profession = new Profession();
        $profession->setName($name);

        $this->manager->persist($profession);
        $this->manager->flush();
    }

    /**
     * @Given there is :count professions
     *
     * @param int $count
     *
     * @throws \Exception
     */
    public function thereIsAResourceNamedWithRecipeItems(int $count)
    {
        for ($i = 0; $i < $count; ++$i) {
            $profession = new Profession();
            $profession->setName(sprintf('Dummy-%s', $i));
            $this->manager->persist($profession);
        }

        $this->manager->flush();
    }
}
