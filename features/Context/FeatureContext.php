<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Behat\Context;

use App\Entity\User;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behatch\Context\RestContext;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\SchemaTool;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $manager;

    /**
     * @var SchemaTool
     */
    private $schemaTool;

    /**
     * @var array
     */
    private $classes;

    /**
     * @var RestContext
     */
    private $restContext;

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    /**
     * Initializes context.
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     * @param ManagerRegistry          $doctrine
     * @param JWTTokenManagerInterface $jwtManager
     */
    public function __construct(ManagerRegistry $doctrine, JWTTokenManagerInterface $jwtManager)
    {
        $this->manager = $manager = $doctrine->getManager();
        $this->schemaTool = new SchemaTool($manager);
        $this->classes = $manager->getMetadataFactory()->getAllMetadata();
        $this->jwtManager = $jwtManager;
    }

    /**
     * @BeforeScenario @createSchema
     *
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function createDatabase()
    {
        $this->dropDatabase();
        $this->schemaTool->createSchema($this->classes);
    }

    /**
     * @AfterScenario @dropSchema
     */
    public function dropDatabase()
    {
        $this->schemaTool->dropSchema($this->classes);
    }

    /**
     * @BeforeScenario @jsonld
     *
     * @param BeforeScenarioScope $scope
     */
    public function addJsonLdHeader(BeforeScenarioScope $scope)
    {
        $this->restContext = $scope->getEnvironment()->getContext(RestContext::class);
        $this->restContext->iAddHeaderEqualTo('Content-Type', 'application/ld+json');
        $this->restContext->iAddHeaderEqualTo('Accept', 'application/ld+json');
    }

    /**
     * @BeforeScenario @json
     *
     * @param BeforeScenarioScope $scope
     */
    public function addJsonHeader(BeforeScenarioScope $scope)
    {
        $this->restContext = $scope->getEnvironment()->getContext(RestContext::class);
        $this->restContext->iAddHeaderEqualTo('Content-Type', 'application/json');
        $this->restContext->iAddHeaderEqualTo('Accept', 'application/json');
    }

    /**
     * @BeforeScenario @login
     *
     * @see https://symfony.com/doc/current/security/entity_provider.html#creating-your-first-user
     */
    public function login(BeforeScenarioScope $scope)
    {
        $this->createUser($scope, 'admin', User::ROLE_ADMIN);
    }

    /**
     * @BeforeScenario @loginDemo
     *
     * @see https://symfony.com/doc/current/security/entity_provider.html#creating-your-first-user
     */
    public function loginDemo(BeforeScenarioScope $scope)
    {
        $this->createUser($scope, 'demo');
    }

    /**
     * @AfterScenario @logout
     */
    public function logout()
    {
        $this->restContext->iAddHeaderEqualTo('Authorization', '');
    }

    protected function createUser(BeforeScenarioScope $scope, $username, $role = User::ROLE_DEFAULT): void
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($username);
        $user->addRole($role);

        $this->manager->persist($user);
        $this->manager->flush();

        $token = $this->jwtManager->create($user);

        $this->restContext = $scope->getEnvironment()->getContext(RestContext::class);
        $this->restContext->iAddHeaderEqualTo('Authorization', "Bearer $token");
    }
}
