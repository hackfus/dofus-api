<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Behat\Context;

use App\Entity\Equipment;
use App\Entity\RecipeItem;
use App\Entity\Resource;
use App\Entity\Weapon;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class ResourceContext implements Context, SnippetAcceptingContext
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * Initializes context.
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->manager = $doctrine->getManager();
    }

    /**
     * @Given there is a resource named :name
     *
     * @param string $name
     */
    public function thereIsAResourceNamed(string $name)
    {
        $resource = new Resource();
        $resource->setName($name);

        $this->manager->persist($resource);
        $this->manager->flush();
    }

    /**
     * @Given there is a weapon named :name
     *
     * @param string $name
     */
    public function thereIsAWeaponNamed(string $name)
    {
        $resource = new Weapon();
        $resource->setName($name);

        $this->manager->persist($resource);
        $this->manager->flush();
    }

    /**
     * @Given there is a(n) equipment named :name
     *
     * @param string $name
     */
    public function thereIsAnEquipmentNamed(string $name)
    {
        $resource = new Equipment();
        $resource->setName($name);

        $this->manager->persist($resource);
        $this->manager->flush();
    }

    /**
     * @Given there is a resource named :name with :items recipe items
     *
     * @param string $name
     * @param int    $items
     *
     * @throws \Exception
     */
    public function thereIsAResourceNamedWithRecipeItems(string $name, int $items = 0)
    {
        $resource = new Resource();
        $resource->setName($name);

        $this->manager->persist($resource);
        // Ensure that this resource has id = 1
        $this->manager->flush();

        if ($items > 0) {
            for ($i = 0; $i < $items; ++$i) {
                $recipeItem = new RecipeItem();
                $recipeItem->setQuantity(random_int(1, 15));

                $subresource = new Resource();
                $subresource->setName(sprintf('Dummy-%s', $i));
                $this->manager->persist($subresource);

                $recipeItem->setSubresource($subresource);
                $recipeItem->setResourceCrafted($resource);

                $this->manager->persist($recipeItem);

                $resource->addRecipeItem($recipeItem);
            }
        }

        $this->manager->flush();
    }
}
