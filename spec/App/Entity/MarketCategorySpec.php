<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use PhpSpec\ObjectBehavior;

class MarketCategorySpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(MarketCategory::class);
    }

    public function it_implements_a_market_category_interface(): void
    {
        $this->shouldImplement(MarketCategoryInterface::class);
    }

    public function it_implements_a_nameable_interface(): void
    {
        $this->shouldImplement(NameableInterface::class);
    }

    public function it_implements_a_timestampable_interface(): void
    {
        $this->shouldImplement(TimestampableInterface::class);
    }

    public function it_implements_a_toggleable_interface(): void
    {
        $this->shouldImplement(ToggleableInterface::class);
    }

    public function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    public function it_has_no_created_at_by_default(): void
    {
        $this->getCreatedAt()->shouldReturn(null);
    }

    public function its_creation_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setCreatedAt($creationDate);
        $this->getCreatedAt()->shouldReturn($creationDate);
    }

    public function it_has_no_updated_at_by_default(): void
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    public function its_update_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setUpdatedAt($creationDate);
        $this->getUpdatedAt()->shouldReturn($creationDate);
    }

    public function it_is_enabled_by_default(): void
    {
        $this->shouldBeEnabled();
    }

    public function it_is_toggleable(): void
    {
        $this->disable();
        $this->shouldNotBeEnabled();
        $this->enable();
        $this->shouldBeEnabled();
    }

    public function it_has_no_name_by_default(): void
    {
        $this->getName()->shouldReturn(null);
    }

    public function its_name_is_mutable(): void
    {
        $this->setName('Foo');
        $this->getName()->shouldReturn('Foo');

        $this->setName(null);
        $this->getName()->shouldReturn(null);
    }

    public function it_has_no_market_by_default(): void
    {
        $this->getMarket()->shouldReturn(null);
    }

    public function its_market_field_is_mutable(MarketInterface $market): void
    {
        $this->setMarket($market);
        $this->getMarket()->shouldReturn($market);

        $this->setMarket(null);
        $this->getMarket()->shouldReturn(null);
    }

    public function it_creates_resources_collection_by_default(): void
    {
        $this->getResources()->shouldHaveType(Collection::class);
    }

    public function it_adds_resources_properly(ResourceInterface $item): void
    {
        $item->setMarketCategory($this)->shouldBeCalled();
        $this->hasResource($item)->shouldReturn(false);

        $this->addResource($item);
        $this->hasResource($item)->shouldReturn(true);
    }

    public function it_removes_resources_properly(ResourceInterface $item): void
    {
        $this->hasResource($item)->shouldReturn(false);

        $item->setMarketCategory($this)->shouldBeCalled();
        $this->addResource($item);
        $this->hasResource($item)->shouldReturn(true);

        $item->setMarketCategory(null)->shouldBeCalled();
        $this->removeResource($item);
        $this->hasResource($item)->shouldReturn(false);
    }
}
