<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use PhpSpec\ObjectBehavior;

class ProfessionSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Profession::class);
    }

    public function it_implements_a_profession_interface(): void
    {
        $this->shouldImplement(ProfessionInterface::class);
    }

    public function it_implements_a_nameable_interface(): void
    {
        $this->shouldImplement(NameableInterface::class);
    }

    public function it_implements_a_timestampable_interface(): void
    {
        $this->shouldImplement(TimestampableInterface::class);
    }

    public function it_implements_a_toggleable_interface(): void
    {
        $this->shouldImplement(ToggleableInterface::class);
    }

    public function it_implements_a_crawlable_interface(): void
    {
        $this->shouldImplement(CrawlableInterface::class);
    }

    public function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    public function it_has_no_created_at_by_default(): void
    {
        $this->getCreatedAt()->shouldReturn(null);
    }

    public function its_creation_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setCreatedAt($creationDate);
        $this->getCreatedAt()->shouldReturn($creationDate);
    }

    public function it_has_no_updated_at_by_default(): void
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    public function its_update_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setUpdatedAt($creationDate);
        $this->getUpdatedAt()->shouldReturn($creationDate);
    }

    public function it_is_enabled_by_default(): void
    {
        $this->shouldBeEnabled();
    }

    public function it_is_toggleable(): void
    {
        $this->disable();
        $this->shouldNotBeEnabled();
        $this->enable();
        $this->shouldBeEnabled();
    }

    public function it_has_no_name_by_default(): void
    {
        $this->getName()->shouldReturn(null);
    }

    public function its_name_is_mutable(): void
    {
        $this->setName('Foo');
        $this->getName()->shouldReturn('Foo');

        $this->setName(null);
        $this->getName()->shouldReturn(null);
    }

    public function it_has_no_description_by_default(): void
    {
        $this->getDescription()->shouldReturn(null);
    }

    public function its_description_is_mutable(): void
    {
        $this->setDescription('Dummy description');
        $this->getDescription()->shouldReturn('Dummy description');

        $this->setDescription(null);
        $this->getDescription()->shouldReturn(null);
    }

    public function it_creates_harvests_collection_by_default(): void
    {
        $this->getHarvests()->shouldHaveType(Collection::class);
    }

    public function it_adds_harvests_properly(ResourceInterface $item): void
    {
        $item->setHarvestProfession($this)->shouldBeCalled();
        $this->hasHarvest($item)->shouldReturn(false);

        $this->addHarvest($item);
        $this->hasHarvest($item)->shouldReturn(true);
    }

    public function it_removes_harvests_properly(ResourceInterface $item): void
    {
        $this->hasHarvest($item)->shouldReturn(false);

        $item->setHarvestProfession($this)->shouldBeCalled();
        $this->addHarvest($item);
        $this->hasHarvest($item)->shouldReturn(true);

        $item->setHarvestProfession(null)->shouldBeCalled();
        $this->removeHarvest($item);
        $this->hasHarvest($item)->shouldReturn(false);
    }

    public function it_creates_recipes_collection_by_default(): void
    {
        $this->getRecipes()->shouldHaveType(Collection::class);
    }

    public function it_adds_recipes_properly(ResourceInterface $item): void
    {
        $item->setRecipeProfession($this)->shouldBeCalled();
        $this->hasRecipe($item)->shouldReturn(false);

        $this->addRecipe($item);
        $this->hasRecipe($item)->shouldReturn(true);
    }

    public function it_removes_recipes_properly(ResourceInterface $item): void
    {
        $this->hasRecipe($item)->shouldReturn(false);

        $item->setRecipeProfession($this)->shouldBeCalled();
        $this->addRecipe($item);
        $this->hasRecipe($item)->shouldReturn(true);

        $item->setRecipeProfession(null)->shouldBeCalled();
        $this->removeRecipe($item);
        $this->hasRecipe($item)->shouldReturn(false);
    }
}
