<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use PhpSpec\ObjectBehavior;

class MarketSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Market::class);
    }

    public function it_implements_a_nameable_interface(): void
    {
        $this->shouldImplement(NameableInterface::class);
    }

    public function it_implements_a_market_interface(): void
    {
        $this->shouldImplement(MarketInterface::class);
    }

    public function it_implements_a_timestampable_interface(): void
    {
        $this->shouldImplement(TimestampableInterface::class);
    }

    public function it_implements_a_toggleable_interface(): void
    {
        $this->shouldImplement(ToggleableInterface::class);
    }

    public function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    public function it_has_no_created_at_by_default(): void
    {
        $this->getCreatedAt()->shouldReturn(null);
    }

    public function its_creation_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setCreatedAt($creationDate);
        $this->getCreatedAt()->shouldReturn($creationDate);
    }

    public function it_has_no_updated_at_by_default(): void
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    public function its_update_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setUpdatedAt($creationDate);
        $this->getUpdatedAt()->shouldReturn($creationDate);
    }

    public function it_is_enabled_by_default(): void
    {
        $this->shouldBeEnabled();
    }

    public function it_is_toggleable(): void
    {
        $this->disable();
        $this->shouldNotBeEnabled();
        $this->enable();
        $this->shouldBeEnabled();
    }

    public function it_has_no_name_by_default(): void
    {
        $this->getName()->shouldReturn(null);
    }

    public function its_name_is_mutable(): void
    {
        $this->setName('Foo');
        $this->getName()->shouldReturn('Foo');

        $this->setName(null);
        $this->getName()->shouldReturn(null);
    }

    public function it_creates_empty_categories_by_default(): void
    {
        $this->getCategories()->shouldHaveType(Collection::class);
        $this->getCategories()->shouldHaveCount(0);
    }

    public function it_adds_categories_properly(MarketCategoryInterface $item): void
    {
        $item->setMarket($this)->shouldBeCalled();
        $this->hasCategory($item)->shouldReturn(false);

        $this->addCategory($item);
        $this->hasCategory($item)->shouldReturn(true);
    }

    public function it_removes_categories_properly(MarketCategoryInterface $item): void
    {
        $this->hasCategory($item)->shouldReturn(false);

        $item->setMarket($this)->shouldBeCalled();
        $this->addCategory($item);
        $this->hasCategory($item)->shouldReturn(true);

        $item->setMarket(null)->shouldBeCalled();
        $this->removeCategory($item);
        $this->hasCategory($item)->shouldReturn(false);
    }
}
