<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\File\File as HttpFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;

class FileSpec extends ObjectBehavior
{
    public function let($file, $uploadedFile): void
    {
        $file->beADoubleOf(HttpFile::class);
        $file->beConstructedWith(['/path', false]);

        $uploadedFile->beADoubleOf(UploadedFile::class);
        $uploadedFile->beConstructedWith(['/path', 'foo', null, null, 1, true]);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(File::class);
    }

    public function it_implements_a_nameable_interface(): void
    {
        $this->shouldImplement(NameableInterface::class);
    }

    public function it_implements_a_file_interface(): void
    {
        $this->shouldImplement(FileInterface::class);
    }

    public function it_implements_a_timestampable_interface(): void
    {
        $this->shouldImplement(TimestampableInterface::class);
    }

    public function it_implements_a_toggleable_interface(): void
    {
        $this->shouldImplement(ToggleableInterface::class);
    }

    public function it_implements_a_crawlable_interface(): void
    {
        $this->shouldImplement(CrawlableInterface::class);
    }

    public function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    public function it_has_no_created_at_by_default(): void
    {
        $this->getCreatedAt()->shouldReturn(null);
    }

    public function its_creation_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setCreatedAt($creationDate);
        $this->getCreatedAt()->shouldReturn($creationDate);
    }

    public function it_has_no_updated_at_by_default(): void
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    public function its_update_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setUpdatedAt($creationDate);
        $this->getUpdatedAt()->shouldReturn($creationDate);
    }

    public function it_is_enabled_by_default(): void
    {
        $this->shouldBeEnabled();
    }

    public function it_is_toggleable(): void
    {
        $this->disable();
        $this->shouldNotBeEnabled();
        $this->enable();
        $this->shouldBeEnabled();
    }

    public function it_has_no_name_by_default(): void
    {
        $this->getName()->shouldReturn(null);
    }

    public function its_name_is_mutable(): void
    {
        $this->setName('Foo');
        $this->getName()->shouldReturn('Foo');

        $this->setName(null);
        $this->getName()->shouldReturn(null);
    }

    public function it_has_no_orignal_name_by_default(): void
    {
        $this->getOriginalName()->shouldReturn(null);
    }

    public function its_orignal_name_is_mutable(): void
    {
        $this->setOriginalName('Foo');
        $this->getOriginalName()->shouldReturn('Foo');

        $this->setOriginalName(null);
        $this->getOriginalName()->shouldReturn(null);
    }

    public function it_has_no_mime_type_by_default(): void
    {
        $this->getMimeType()->shouldReturn(null);
    }

    public function its_mime_type_is_mutable(): void
    {
        $this->setMimeType('Foo');
        $this->getMimeType()->shouldReturn('Foo');

        $this->setMimeType(null);
        $this->getMimeType()->shouldReturn(null);
    }

    public function it_has_no_size_by_default(): void
    {
        $this->getSize()->shouldReturn(null);
    }

    public function its_size_is_mutable(): void
    {
        $this->setSize(10);
        $this->getSize()->shouldReturn(10);

        $this->setSize(null);
        $this->getSize()->shouldReturn(null);
    }

    public function it_has_no_dimensions_by_default(): void
    {
        $this->getDimensions()->shouldReturn(null);
    }

    public function its_dimensions_is_mutable(): void
    {
        $this->setDimensions([100, 100]);
        $this->getDimensions()->shouldReturn([100, 100]);

        $this->setDimensions(null);
        $this->getDimensions()->shouldReturn(null);
    }

    public function it_has_no_image_file_by_default(): void
    {
        $this->getImageFile()->shouldReturn(null);
    }

    public function its_image_file_is_mutable($file): void
    {
        $this->setImageFile($file);
        $this->getImageFile()->shouldReturn($file);

        $this->setImageFile(null);
        $this->getImageFile()->shouldReturn(null);
    }

    public function its_image_file_should_accept_uploaded_file($uploadedFile): void
    {
        $this->setImageFile($uploadedFile);
        $this->getImageFile()->shouldReturn($uploadedFile);

        $this->setImageFile(null);
        $this->getImageFile()->shouldReturn(null);
    }

    public function it_has_no_image_by_default(): void
    {
        $this->getImageFile()->shouldReturn(null);
    }

    public function its_image_is_mutable(EmbeddedFile $file): void
    {
        $this->setImageFile($file);
        $this->getImageFile()->shouldReturn($file);

        $this->setImageFile(null);
        $this->getImageFile()->shouldReturn(null);
    }
}
