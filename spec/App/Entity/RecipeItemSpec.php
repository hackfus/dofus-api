<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use PhpSpec\ObjectBehavior;

class RecipeItemSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RecipeItem::class);
    }

    public function it_implements_a_recipe_interface(): void
    {
        $this->shouldImplement(RecipeItemInterface::class);
    }

    public function it_implements_a_timestampable_interface(): void
    {
        $this->shouldImplement(TimestampableInterface::class);
    }

    public function it_implements_a_toggleable_interface(): void
    {
        $this->shouldImplement(ToggleableInterface::class);
    }

    public function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    public function it_has_no_created_at_by_default(): void
    {
        $this->getCreatedAt()->shouldReturn(null);
    }

    public function its_creation_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setCreatedAt($creationDate);
        $this->getCreatedAt()->shouldReturn($creationDate);
    }

    public function it_has_no_updated_at_by_default(): void
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    public function its_update_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setUpdatedAt($creationDate);
        $this->getUpdatedAt()->shouldReturn($creationDate);
    }

    public function it_is_enabled_by_default(): void
    {
        $this->shouldBeEnabled();
    }

    public function it_is_toggleable(): void
    {
        $this->disable();
        $this->shouldNotBeEnabled();
        $this->enable();
        $this->shouldBeEnabled();
    }

    public function it_has_no_resource_crafted_by_default(): void
    {
        $this->getResourceCrafted()->shouldReturn(null);
    }

    public function its_resource_crafted_field_is_mutable(ResourceInterface $resource): void
    {
        $this->setResourceCrafted($resource);
        $this->getResourceCrafted()->shouldReturn($resource);

        $this->setResourceCrafted(null);
        $this->getResourceCrafted()->shouldReturn(null);
    }

    public function it_has_no_subresource_by_default(): void
    {
        $this->getSubresource()->shouldReturn(null);
    }

    public function its_subresource_field_is_mutable(ResourceInterface $resource): void
    {
        $this->setSubresource($resource);
        $this->getSubresource()->shouldReturn($resource);

        $this->setSubresource(null);
        $this->getSubresource()->shouldReturn(null);
    }

    public function it_has_quantity_equals_to_1_by_default(): void
    {
        $this->getQuantity()->shouldReturn(1);
    }

    public function its_quantity_is_mutable(): void
    {
        $this->setQuantity(50);
        $this->getQuantity()->shouldReturn(50);
    }
}
