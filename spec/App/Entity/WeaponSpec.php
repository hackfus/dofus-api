<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use PhpSpec\ObjectBehavior;

class WeaponSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Weapon::class);
    }

    public function it_implements_a_nameable_interface(): void
    {
        $this->shouldImplement(NameableInterface::class);
    }

    public function it_implements_a_resource_interface(): void
    {
        $this->shouldImplement(ResourceInterface::class);
    }

    public function it_implements_a_weapon_interface(): void
    {
        $this->shouldImplement(WeaponInterface::class);
    }

    public function it_implements_a_timestampable_interface(): void
    {
        $this->shouldImplement(TimestampableInterface::class);
    }

    public function it_implements_a_toggleable_interface(): void
    {
        $this->shouldImplement(ToggleableInterface::class);
    }

    public function it_implements_a_crawlable_interface(): void
    {
        $this->shouldImplement(CrawlableInterface::class);
    }

    public function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    public function it_has_no_created_at_by_default(): void
    {
        $this->getCreatedAt()->shouldReturn(null);
    }

    public function its_creation_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setCreatedAt($creationDate);
        $this->getCreatedAt()->shouldReturn($creationDate);
    }

    public function it_has_no_updated_at_by_default(): void
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    public function its_update_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setUpdatedAt($creationDate);
        $this->getUpdatedAt()->shouldReturn($creationDate);
    }

    public function it_is_enabled_by_default(): void
    {
        $this->shouldBeEnabled();
    }

    public function it_is_toggleable(): void
    {
        $this->disable();
        $this->shouldNotBeEnabled();
        $this->enable();
        $this->shouldBeEnabled();
    }

    public function it_has_no_name_by_default(): void
    {
        $this->getName()->shouldReturn(null);
    }

    public function its_name_is_mutable(): void
    {
        $this->setName('Foo');
        $this->getName()->shouldReturn('Foo');

        $this->setName(null);
        $this->getName()->shouldReturn(null);
    }

    public function it_has_level_equals_to_1_by_default(): void
    {
        $this->getLevel()->shouldReturn(1);
    }

    public function its_level_is_mutable(): void
    {
        $this->setLevel(50);
        $this->getLevel()->shouldReturn(50);
    }

    public function it_has_no_description_by_default(): void
    {
        $this->getDescription()->shouldReturn(null);
    }

    public function its_description_is_mutable(): void
    {
        $this->setDescription('Dummy description');
        $this->getDescription()->shouldReturn('Dummy description');

        $this->setDescription(null);
        $this->getDescription()->shouldReturn(null);
    }

    public function it_is_tradable_by_default(): void
    {
        $this->isTradable()->shouldReturn(true);
    }

    public function its_tradable_field_is_mutable(): void
    {
        $this->setTradable(false);
        $this->isTradable()->shouldReturn(false);

        $this->setTradable(true);
        $this->isTradable()->shouldReturn(true);
    }

    public function it_is_droppable_by_default(): void
    {
        $this->isTradable()->shouldReturn(true);
    }

    public function its_droppable_field_is_mutable(): void
    {
        $this->setDroppable(false);
        $this->isDroppable()->shouldReturn(false);

        $this->setDroppable(true);
        $this->isDroppable()->shouldReturn(true);
    }

    public function it_is_not_secret_by_default(): void
    {
        $this->isSecret()->shouldReturn(false);
    }

    public function its_secret_field_is_mutable(): void
    {
        $this->setSecret(true);
        $this->isSecret()->shouldReturn(true);

        $this->setSecret(false);
        $this->isSecret()->shouldReturn(false);
    }

    public function it_is_not_fetched_by_default(): void
    {
        $this->isFetched()->shouldReturn(false);
    }

    public function its_fetched_field_is_mutable(): void
    {
        $this->setFetched(true);
        $this->isFetched()->shouldReturn(true);

        $this->setFetched(false);
        $this->isFetched()->shouldReturn(false);
    }

    public function it_has_no_market_by_default(): void
    {
        $this->getMarket()->shouldReturn(null);
    }

    public function it_has_no_market_category_by_default(): void
    {
        $this->getMarketCategory()->shouldReturn(null);
    }

    public function its_market_category_field_is_mutable(MarketCategoryInterface $category): void
    {
        $this->setMarketCategory($category);
        $this->getMarketCategory()->shouldReturn($category);

        $this->setMarketCategory(null);
        $this->getMarketCategory()->shouldReturn(null);
    }

    public function its_market_is_the_same_as_market_category_value(MarketInterface $market, MarketCategoryInterface $category): void
    {
        $category->getMarket()->willReturn($market);

        $this->setMarketCategory($category);
        $this->getMarketCategory()->shouldReturn($category);
        $this->getMarket()->shouldReturn($market);
    }

    public function it_creates_recipe_items_collection_by_default(): void
    {
        $this->getRecipeItems()->shouldBeArray();
    }

    public function it_adds_recipe_items_properly(RecipeItemInterface $item): void
    {
        $item->setResourceCrafted($this)->shouldBeCalled();
        $this->hasRecipeItem($item)->shouldReturn(false);

        $this->addRecipeItem($item);
        $this->hasRecipeItem($item)->shouldReturn(true);
    }

    public function it_removes_recipe_items_properly(RecipeItemInterface $item): void
    {
        $this->hasRecipeItem($item)->shouldReturn(false);

        $item->setResourceCrafted($this)->shouldBeCalled();
        $this->addRecipeItem($item);
        $this->hasRecipeItem($item)->shouldReturn(true);

        $item->setResourceCrafted(null)->shouldBeCalled();
        $this->removeRecipeItem($item);
        $this->hasRecipeItem($item)->shouldReturn(false);
    }

    public function it_creates_crafts_collection_by_default(): void
    {
        $this->getCrafts()->shouldBeArray();
    }

    public function it_adds_crafts_properly(RecipeItemInterface $item): void
    {
        $item->setSubresource($this)->shouldBeCalled();
        $this->hasCraft($item)->shouldReturn(false);

        $this->addCraft($item);
        $this->hasCraft($item)->shouldReturn(true);
    }

    public function it_removes_crafts_properly(RecipeItemInterface $item): void
    {
        $this->hasCraft($item)->shouldReturn(false);

        $item->setSubresource($this)->shouldBeCalled();
        $this->addCraft($item);
        $this->hasCraft($item)->shouldReturn(true);

        $item->setSubresource(null)->shouldBeCalled();
        $this->removeCraft($item);
        $this->hasCraft($item)->shouldReturn(false);
    }

    public function it_has_no_harvest_profession_by_default(): void
    {
        $this->getHarvestProfession()->shouldReturn(null);
    }

    public function its_harvest_profession_field_is_mutable(ProfessionInterface $profession): void
    {
        $this->setHarvestProfession($profession);
        $this->getHarvestProfession()->shouldReturn($profession);

        $this->setHarvestProfession(null);
        $this->getHarvestProfession()->shouldReturn(null);
    }

    public function it_has_no_recipe_profession_by_default(): void
    {
        $this->getRecipeProfession()->shouldReturn(null);
    }

    public function its_recipe_profession_field_is_mutable(ProfessionInterface $profession): void
    {
        $this->setRecipeProfession($profession);
        $this->getRecipeProfession()->shouldReturn($profession);

        $this->setRecipeProfession(null);
        $this->getRecipeProfession()->shouldReturn(null);
    }

    public function it_has_no_crawler_id_by_default(): void
    {
        $this->getLastCrawler()->shouldReturn(null);
    }

    public function its_crawler_id_is_mutable(): void
    {
        $this->setLastCrawler('Foo');
        $this->getLastCrawler()->shouldReturn('Foo');

        $this->setLastCrawler(null);
        $this->getLastCrawler()->shouldReturn(null);
    }

    public function it_has_action_points_equals_to_1_by_default(): void
    {
        $this->getActionPoints()->shouldReturn(1);
    }

    public function its_action_points_is_mutable(): void
    {
        $this->setActionPoints(50);
        $this->getActionPoints()->shouldReturn(50);
    }

    public function it_has_uses_per_turn_equals_to_1_by_default(): void
    {
        $this->getUsesPerTurn()->shouldReturn(1);
    }

    public function its_uses_per_turn_is_mutable(): void
    {
        $this->setUsesPerTurn(50);
        $this->getUsesPerTurn()->shouldReturn(50);
    }

    public function it_has_min_range_equals_to_1_by_default(): void
    {
        $this->getMinRange()->shouldReturn(1);
    }

    public function its_min_range_is_mutable(): void
    {
        $this->setMinRange(50);
        $this->getMinRange()->shouldReturn(50);
    }

    public function it_has_max_range_equals_to_1_by_default(): void
    {
        $this->getMaxRange()->shouldReturn(1);
    }

    public function its_max_range_is_mutable(): void
    {
        $this->setMaxRange(50);
        $this->getMaxRange()->shouldReturn(50);
    }

    public function it_has_critical_rate_equals_to_0_by_default(): void
    {
        $this->getCriticalRate()->shouldReturn(0);
    }

    public function its_critical_rate_is_mutable(): void
    {
        $this->setCriticalRate(50);
        $this->getCriticalRate()->shouldReturn(50);
    }

    public function it_has_critical_hit_damages_equals_to_0_by_default(): void
    {
        $this->getCriticalHitDamages()->shouldReturn(0);
    }

    public function its_critical_hit_damages_is_mutable(): void
    {
        $this->setCriticalHitDamages(50);
        $this->getCriticalHitDamages()->shouldReturn(50);
    }
}
