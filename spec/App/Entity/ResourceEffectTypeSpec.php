<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use PhpSpec\ObjectBehavior;

class ResourceEffectTypeSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(ResourceEffectType::class);
    }

    public function it_implements_a_resource_effect_type_interface(): void
    {
        $this->shouldImplement(ResourceEffectTypeInterface::class);
    }

    public function it_implements_a_timestampable_interface(): void
    {
        $this->shouldImplement(TimestampableInterface::class);
    }

    public function it_implements_a_toggleable_interface(): void
    {
        $this->shouldImplement(ToggleableInterface::class);
    }

    public function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    public function it_has_no_created_at_by_default(): void
    {
        $this->getCreatedAt()->shouldReturn(null);
    }

    public function its_creation_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setCreatedAt($creationDate);
        $this->getCreatedAt()->shouldReturn($creationDate);
    }

    public function it_has_no_updated_at_by_default(): void
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    public function its_update_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setUpdatedAt($creationDate);
        $this->getUpdatedAt()->shouldReturn($creationDate);
    }

    public function it_is_enabled_by_default(): void
    {
        $this->shouldBeEnabled();
    }

    public function it_is_toggleable(): void
    {
        $this->disable();
        $this->shouldNotBeEnabled();
        $this->enable();
        $this->shouldBeEnabled();
    }

    public function it_is_not_primary_bonus_by_default(): void
    {
        $this->isPrimaryBonus()->shouldReturn(false);
    }

    public function its_primary_bonus_is_toggleable(): void
    {
        $this->isPrimaryBonus()->shouldReturn(false);
        $this->setPrimaryBonus(true);
        $this->isPrimaryBonus()->shouldReturn(true);
        $this->setPrimaryBonus(false);
        $this->isPrimaryBonus()->shouldReturn(false);
    }

    public function it_is_not_cosmetic_bonus_by_default(): void
    {
        $this->isCosmeticBonus()->shouldReturn(false);
    }

    public function its_cosmetic_bonus_is_toggleable(): void
    {
        $this->isCosmeticBonus()->shouldReturn(false);
        $this->setCosmeticBonus(true);
        $this->isCosmeticBonus()->shouldReturn(true);
        $this->setCosmeticBonus(false);
        $this->isCosmeticBonus()->shouldReturn(false);
    }

    public function it_has_no_label_by_default(): void
    {
        $this->getLabel()->shouldReturn(null);
    }

    public function its_label_is_mutable(): void
    {
        $this->setLabel('Foo');
        $this->getLabel()->shouldReturn('Foo');

        $this->setLabel(null);
        $this->getLabel()->shouldReturn(null);
    }
}
