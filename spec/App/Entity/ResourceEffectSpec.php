<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use PhpSpec\ObjectBehavior;

class ResourceEffectSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(ResourceEffect::class);
    }

    public function it_implements_a_nameable_interface(): void
    {
        $this->shouldImplement(NameableInterface::class);
    }

    public function it_implements_a_resource_interface(): void
    {
        $this->shouldImplement(ResourceEffectInterface::class);
    }

    public function it_implements_a_timestampable_interface(): void
    {
        $this->shouldImplement(TimestampableInterface::class);
    }

    public function it_implements_a_toggleable_interface(): void
    {
        $this->shouldImplement(ToggleableInterface::class);
    }

    public function it_implements_a_crawlable_interface(): void
    {
        $this->shouldImplement(CrawlableInterface::class);
    }

    public function it_has_no_id_by_default(): void
    {
        $this->getId()->shouldReturn(null);
    }

    public function it_has_no_created_at_by_default(): void
    {
        $this->getCreatedAt()->shouldReturn(null);
    }

    public function its_creation_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setCreatedAt($creationDate);
        $this->getCreatedAt()->shouldReturn($creationDate);
    }

    public function it_has_no_updated_at_by_default(): void
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    public function its_update_date_is_mutable(\DateTime $creationDate): void
    {
        $this->setUpdatedAt($creationDate);
        $this->getUpdatedAt()->shouldReturn($creationDate);
    }

    public function it_is_enabled_by_default(): void
    {
        $this->shouldBeEnabled();
    }

    public function it_is_toggleable(): void
    {
        $this->disable();
        $this->shouldNotBeEnabled();
        $this->enable();
        $this->shouldBeEnabled();
    }

    public function it_has_no_name_by_default(): void
    {
        $this->getName()->shouldReturn(null);
    }

    public function its_name_is_mutable(): void
    {
        $this->setName('Foo');
        $this->getName()->shouldReturn('Foo');

        $this->setName(null);
        $this->getName()->shouldReturn(null);
    }

    public function it_has_min_value_equals_to_1_by_default(): void
    {
        $this->getMinValue()->shouldReturn(1);
    }

    public function its_min_value_is_mutable(): void
    {
        $this->setMinValue(50);
        $this->getMinValue()->shouldReturn(50);
    }

    public function it_has_max_value_equals_to_1_by_default(): void
    {
        $this->getMaxValue()->shouldReturn(1);
    }

    public function its_max_value_is_mutable(): void
    {
        $this->setMaxValue(50);
        $this->getMaxValue()->shouldReturn(50);
    }

    public function its_value_is_not_in_percent_by_default(): void
    {
        $this->getValueIsInPercent()->shouldReturn(false);
    }

    public function its_value_is_percent_field_is_mutable(): void
    {
        $this->setValueIsInPercent(true);
        $this->getValueIsInPercent()->shouldReturn(true);

        $this->setValueIsInPercent(false);
        $this->getValueIsInPercent()->shouldReturn(false);
    }

    public function it_has_no_type_by_default(): void
    {
        $this->getType()->shouldReturn(null);
    }

    public function its_type_is_mutable(ResourceEffectTypeInterface $effectType): void
    {
        $this->setType($effectType);
        $this->getType()->shouldReturn($effectType);

        $this->setType(null);
        $this->getType()->shouldReturn(null);
    }

    public function it_has_no_literal_by_default(): void
    {
        $this->getLiteral()->shouldReturn(null);
    }

    public function its_literal_is_mutable(): void
    {
        $this->setLiteral('Foo');
        $this->getLiteral()->shouldReturn('Foo');

        $this->setLiteral(null);
        $this->getLiteral()->shouldReturn(null);
    }

    public function it_has_no_resource_by_default(): void
    {
        $this->getResource()->shouldReturn(null);
    }

    public function its_resource_field_is_mutable(ResourceInterface $market): void
    {
        $this->setResource($market);
        $this->getResource()->shouldReturn($market);

        $this->setResource(null);
        $this->getResource()->shouldReturn(null);
    }
}
