<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\Profession;
use App\Entity\ProfessionInterface;
use PhpSpec\ObjectBehavior;

class ProfessionFactorySpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith(Profession::class);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(ProfessionFactoryInterface::class);
    }

    public function it_implements_a_factory_interface(): void
    {
        $this->shouldImplement(FactoryInterface::class);
    }

    public function it_creates_a_profession(): void
    {
        $this->createNew()->shouldHaveType(ProfessionInterface::class);
    }

    public function it_creates_a_named_profession(): void
    {
        $profession = $this->createNamed('Foo');
        $profession->shouldHaveType(ProfessionInterface::class);
        $profession->getName()->shouldReturn('Foo');
    }
}
