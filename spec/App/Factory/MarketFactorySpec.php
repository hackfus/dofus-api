<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\Market;
use App\Entity\MarketInterface;
use PhpSpec\ObjectBehavior;

class MarketFactorySpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith(Market::class);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(MarketFactoryInterface::class);
    }

    public function it_implements_a_factory_interface(): void
    {
        $this->shouldImplement(FactoryInterface::class);
    }

    public function it_creates_a_market(): void
    {
        $this->createNew()->shouldHaveType(MarketInterface::class);
    }
}
