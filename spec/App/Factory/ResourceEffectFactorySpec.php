<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\ResourceEffect;
use App\Entity\ResourceEffectInterface;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;

class ResourceEffectFactorySpec extends ObjectBehavior
{
    public function let(EntityManagerInterface $em, ResourceEffectTypeFactoryInterface $effectTypeFactory): void
    {
        $this->beConstructedWith(ResourceEffect::class, $em, $effectTypeFactory);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(ResourceEffectFactoryInterface::class);
    }

    public function it_implements_a_factory_interface(): void
    {
        $this->shouldImplement(FactoryInterface::class);
    }

    public function it_creates_a_resource_effect(): void
    {
        $this->createNew()->shouldHaveType(ResourceEffectInterface::class);
    }
}
