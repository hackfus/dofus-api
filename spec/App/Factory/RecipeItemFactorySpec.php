<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\RecipeItem;
use App\Entity\RecipeItemInterface;
use PhpSpec\ObjectBehavior;

class RecipeItemFactorySpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith(RecipeItem::class);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RecipeItemFactoryInterface::class);
    }

    public function it_implements_a_factory_interface(): void
    {
        $this->shouldImplement(FactoryInterface::class);
    }

    public function it_creates_a_recipe_item(): void
    {
        $this->createNew()->shouldHaveType(RecipeItemInterface::class);
    }
}
