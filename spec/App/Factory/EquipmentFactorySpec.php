<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\Equipment;
use App\Entity\EquipmentInterface;
use PhpSpec\ObjectBehavior;

class EquipmentFactorySpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith(Equipment::class);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(EquipmentFactoryInterface::class);
    }

    public function it_implements_a_factory_interface(): void
    {
        $this->shouldImplement(FactoryInterface::class);
    }

    public function it_creates_an_equipment(): void
    {
        $this->createNew()->shouldHaveType(EquipmentInterface::class);
    }
}
