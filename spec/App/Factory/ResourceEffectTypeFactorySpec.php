<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\ResourceEffectType;
use App\Entity\ResourceEffectTypeInterface;
use PhpSpec\ObjectBehavior;

class ResourceEffectTypeFactorySpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith(ResourceEffectType::class);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(ResourceEffectTypeFactoryInterface::class);
    }

    public function it_implements_a_factory_interface(): void
    {
        $this->shouldImplement(FactoryInterface::class);
    }

    public function it_creates_a_resource_effect_type(): void
    {
        $this->createNew()->shouldHaveType(ResourceEffectTypeInterface::class);
    }
}
