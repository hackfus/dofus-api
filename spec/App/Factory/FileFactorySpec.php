<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\File;
use App\Entity\FileInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileFactorySpec extends ObjectBehavior
{
    public function let($uploadedFile): void
    {
        $this->beConstructedWith(File::class);

        $uploadedFile->beADoubleOf(UploadedFile::class);
        $uploadedFile->beConstructedWith(['/path', 'foo', null, null, 1, true]);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(FileFactoryInterface::class);
    }

    public function it_implements_a_factory_interface(): void
    {
        $this->shouldImplement(FactoryInterface::class);
    }

    public function it_creates_a_file(): void
    {
        $this->createNew()->shouldHaveType(FileInterface::class);
    }

    public function it_creates_a_file_with_image_file($uploadedFile): void
    {
        $instance = $this->createWithUploadedFile($uploadedFile);

        $instance->shouldHaveType(FileInterface::class);
        $instance->getImageFile()->shouldBe($uploadedFile);
    }
}
