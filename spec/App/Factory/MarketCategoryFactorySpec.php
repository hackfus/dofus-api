<?php

/*
 * This file is part of hackfus/dofus-api project.
 *
 * (c) Jonathan Huteau
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Factory;

use App\Entity\MarketCategory;
use App\Entity\MarketCategoryInterface;
use PhpSpec\ObjectBehavior;

class MarketCategoryFactorySpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith(MarketCategory::class);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(MarketCategoryFactoryInterface::class);
    }

    public function it_implements_a_factory_interface(): void
    {
        $this->shouldImplement(FactoryInterface::class);
    }

    public function it_creates_a_market_category(): void
    {
        $this->createNew()->shouldHaveType(MarketCategoryInterface::class);
    }
}
