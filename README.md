# Dofus API

## Getting started

### Installation

- `git clone git@gitlab.com:hackfus/dofus-api.git`
- `cd dofus-api`
- `cp .env.dist .env`
- Edit `.env` content to your needs
  > :warning: You must setup a MySQL database, otherwise DoctrineMigrations won't work.
- `composer install`

#### Database setup

- Create database `php bin/console doctrine:database:create`
- Create database schema `doctrine:schema:update --force`

If you already have a database for this project, make sure to execute 
`php bin/console doctrine:migrations:migrate -n` instead of the previous one.

#### [JWT setup](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#getting-started)

- `openssl genrsa -out var/jwt/private.pem -aes256 4096`
- `openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem`

  > `var/jwt` is the path specified in your `.env` file.
  
  > The passphrase asked during generation is the value of your `JWT_PASSPHRASE` in `.env` file.

##### Optional:
To ensure everythings works, you can add some [fake data](#generate-fake-data). This gives you two users :
- Demo user with credentials demo/demo (`ROLE_USER`)
  > ```
  > curl -X POST http://localhost.dofus-api/login -H 'Accept: application/ld+json' -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -d '{"username": "demo", "password": "demo"}'
  > ```
  > Replace `http://localhost.dofus-api` by your own endpoint

- Admin user with credentials api-admin/api-admin (`ROLE_ADMIN`)
  > ```
  > curl -X POST http://localhost.dofus-api/login -H 'Accept: application/ld+json' -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -d '{"username": "api-admin", "password": "api-admin"}'
  > ```
  > Replace `http://localhost.dofus-api` by your own endpoint

These methods give you a token that you can pass to [Swagger UI](https://api-platform.com/docs/core/jwt#adding-a-new-api-key).
You have to set complete token value in the input field : `Bearer MY_JWT_TOKEN_VALUE`

#### [Generate fake data](https://github.com/hautelook/AliceBundle)

If you want some fake data, run `php bin/console hautelook:fixtures:load -n`. You can specified 
an environment with `-e=prod`. The default value is `dev`.

## Developer tools

### [PHP CS Fixer](https://github.com/FriendsOfPhp/PHP-CS-Fixer)

- Run code fixes `vendor/bin/php-cs-fixer --verbose fix`

### [Behat](https://github.com/Behat/Behat)

- Run features `vendor/bin/behat --colors --format progress`

### [PHPStan](https://github.com/phpstan/phpstan)

- Run code analysis `vendor/bin/phpstan analyse src -c phpstan.neon -l 4`

### [PHPSpec](https://github.com/phpspec/phpspec)

- Genarete spec : `vendor/bin/phpspec desc "App\Entity\TestModel"`
- Run spec `vendor/bin/phpspec run`
