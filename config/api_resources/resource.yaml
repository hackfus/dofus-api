resources:
    # > Resource configuraton
    App\Entity\Resource:
        attributes: &resourcesAttributes
            filters:
                - name.search_filter
                - resource.range_filter
                - resource.boolean_filter
                - resource.subentities_filter
                - resource.groups_filter
                - all_fields.order_filter
        properties: &resourcesProperties
            recipeItems:
                subresource:
                    resourceClass: 'App\Entity\RecipeItem'
                    collection: true
            effects:
                subresource:
                    resourceClass: 'App\Entity\ResourceEffect'
                    collection: true
        collectionOperations:
            get: &collection_get
                method: 'GET'
                normalization_context:
                    groups: ['resource_read_collection', 'file_read_item']
                    enable_max_depth: true
                swagger_context:
                    summary: |
                        Retrieves the collection of Resources. Default items per page is 30.
            api_markets_resources_get_subresource: *collection_get
            api_markets_categories_resources_get_subresource: *collection_get
            api_market_categories_resources_get_subresource: *collection_get

            post:
                method: 'POST'
                normalization_context: &resourceItemNormalizationContext
                    groups: ['resource_read_item', 'file_read_item']
                denormalization_context:
                    groups: ['resource_write', 'recipe_item_write', 'resource_effect_write', 'file_write']
                    enable_max_depth: true
                force_eager: false
                swagger_context:
                    summary: |
                        Creates a Resource.
        itemOperations:
            get:
                method: 'GET'
                normalization_context:
                    <<: *resourceItemNormalizationContext
                    enable_max_depth: true
                swagger_context:
                    summary: |
                        Retrieves a Resource by its ID.
            put:
                method: 'PUT'
                normalization_context:
                    <<: *resourceItemNormalizationContext
                    enable_max_depth: true
                denormalization_context:
                    groups: ['resource_write', 'recipe_item_write', 'resource_effect_write']
                    enable_max_depth: true
                force_eager: false
                swagger_context:
                    summary: |
                        Replaces a Resource by giving ID as path parameter and Resource object as body parameter.
            delete:
                method: 'DELETE'
                swagger_context:
                    summary: |
                        Removes a Resource by its ID.
    # < Resource configuraton

    # > Equipment configuraton
    # It's necessary to define path and controller attributes as the default name is "Equipment" and we want "Equipments"
    App\Entity\Equipment:
        attributes: *resourcesAttributes
        properties: *resourcesProperties
        collectionOperations:
            get: &collection_get_equipments
                method: 'GET'
                path: '/equipments.{_format}'
                controller: api_platform.action.get_collection
                normalization_context:
                    groups: ['resource_read_collection', 'equipment_read_collection', 'file_read_item']
                    enable_max_depth: true
                swagger_context:
                    summary: |
                        Retrieves the collection of Equipments. Default items per page is 30.
            api_markets_resources_get_subresource: *collection_get_equipments
            api_markets_categories_resources_get_subresource: *collection_get_equipments
            api_market_categories_resources_get_subresource: *collection_get_equipments

            post:
                method: 'POST'
                path: '/equipments.{_format}'
                controller: api_platform.action.post_collection
                normalization_context: &equipmentItemNormalizationContext
                    groups: ['resource_read_item', 'equipment_read_item', 'file_read_item']
                denormalization_context: &equipmentItemDenormalizationContext
                    groups: ['resource_write', 'equipment_write', 'resource_effect_write', 'file_write']
                    enable_max_depth: true
                force_eager: false
                swagger_context:
                    summary: |
                        Creates an Equipment.
        itemOperations:
            get:
                method: 'GET'
                path: '/equipments/{id}.{_format}'
                controller: api_platform.action.get_item
                normalization_context:
                    <<: *equipmentItemNormalizationContext
                    enable_max_depth: true
                swagger_context:
                    summary: |
                        Retrieves an Equipment by its ID.
            put:
                method: 'PUT'
                path: '/equipments/{id}.{_format}'
                controller: api_platform.action.put_item
                normalization_context: *equipmentItemNormalizationContext
                denormalization_context:
                    <<: *equipmentItemDenormalizationContext
                    enable_max_depth: true
                force_eager: false
                swagger_context:
                    summary: |
                        Replaces an Equipment by giving ID as path parameter and Equipment object as body parameter.
            delete:
                method: 'DELETE'
                path: '/equipments/{id}.{_format}'
                controller: api_platform.action.delete_item
                swagger_context:
                    summary: |
                        Removes an Equipment by its ID.
    # < Equipment configuraton

    # > Weapon configuraton
    App\Entity\Weapon:
        attributes: *resourcesAttributes
        properties: *resourcesProperties
        collectionOperations:
            get: &collection_get_weapons
                method: 'GET'
                normalization_context:
                    groups: ['resource_read_collection', 'weapon_read_collection', 'file_read_item']
                    enable_max_depth: true
                swagger_context:
                    summary: |
                        Retrieves the collection of Weapons. Default items per page is 30.
            api_markets_resources_get_subresource: *collection_get_weapons
            api_markets_categories_resources_get_subresource: *collection_get_weapons
            api_market_categories_resources_get_subresource: *collection_get_weapons

            post:
                method: 'POST'
                normalization_context: &weaponItemNormalizationContext
                    groups: ['resource_read_item', 'weapon_read_item', 'file_read_item']
                denormalization_context: &weaponItemDenormalizationContext
                    groups: ['resource_write', 'weapon_write', 'resource_effect_write', 'file_write']
                    enable_max_depth: true
                force_eager: false
                swagger_context:
                    summary: |
                        Creates a Weapon.
        itemOperations:
            get:
                method: 'GET'
                normalization_context:
                    <<: *weaponItemNormalizationContext
                    enable_max_depth: true
                swagger_context:
                    summary: |
                        Retrieves a Weapon by its ID.
            put:
                method: 'PUT'
                normalization_context: *weaponItemNormalizationContext
                denormalization_context:
                    <<: *weaponItemDenormalizationContext
                    enable_max_depth: true
                force_eager: false
                swagger_context:
                    summary: |
                        Replaces a Weapon by giving ID as path parameter and Weapon object as body parameter.
            delete:
                method: 'DELETE'
                swagger_context:
                    summary: |
                        Removes a Weapon by its ID.
    # < Weapon configuraton
