#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -e

composer install --prefer-dist --no-progress --no-suggest --no-interaction
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:schema:create
